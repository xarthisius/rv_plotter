#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy as np
import math
import sys
import itertools
from multiprocessing import Pool, Value

import lomb

counter = None
tot_iter = None


def init(coun, toti):
    ''' store the counter for later use '''
    global counter, tot_iter
    counter, tot_iter = coun, toti


def eval_period(data):
    global counter, tot_iter
    x, y, ip, p = data
    hifac = 0.5 / (0.5 * len(x) / (np.max(x) - np.min(x)))
    fx, fy, nout, jmax, prob = lomb.fasper(
        np.delete(x, p), np.delete(y, p), 10., hifac)
    counter.value += 1
    sys.stderr.write("Completed %6i/%6i iterations   \r" %
                     (counter.value, tot_iter.value))
    return (1.0 / fx[np.argmax(fy)], np.max(fy), p)


def significance_level(p, N):
    return -math.log(p / (2.0 * N))


def draw_siglev(ax, N):
    ax.axhline(y=significance_level(0.01, N), color='k', ls='--')
    ax.axhline(y=significance_level(0.001, N), color='k', ls='-.')
    xlim = ax.get_xlim()
    ax.text(0.25 * xlim[-1], significance_level(0.01, N) * 0.87,
            "p = 0.01")
    ax.text(0.25 * xlim[-1], significance_level(0.001, N) * 0.87,
            "p = 0.001")

def draw_PN(ax, t0, pn):
    lbl = r"$T_{0} = %6.1f$ days" % t0
    #lbl += "\n"
    #lbl += r"$PN = %6.1f$" % pn
    #ax.text(t0*1.1, 0.45, lbl, transform=ax.transAxes)
    ax.text(t0*1.2, ax.get_ylim()[-1] * 0.4, lbl)


def hist_magic(x, y, ey, np_remove, name):
    N = len(x)
    name.replace('_', ' ')
    chunks = [tup for tup in itertools.combinations(list(range(N)), np_remove)]

    counter = Value('i', 0)
    tot_iter = Value('i', len(chunks))

    po = Pool(initializer=init, initargs=(counter, tot_iter))
    res = po.map_async(eval_period,
                       ((x, y, ip, p) for ip, p in enumerate(chunks)))
    result = res.get()
    po.close()

    periods = []
    powers = []
    pairs = []
    for item in result:
        period, power, pair = item
        periods.append(period)
        powers.append(power)
        pairs.append(pair)

    periods = np.array(periods)
    powers = np.array(powers)
    best = np.argmax(powers)

    indices = list(pairs[best])

    F = plt.figure(1, (10, 9))
    F.subplots_adjust(wspace=0.3)
    hifac = 0.5 / (0.5 * len(x) / (np.max(x) - np.min(x)))

    ax = plt.subplot(4, 1, 1)
    ax.set_title("%s -- with %i points removed" % (name, np_remove))
    fx, fy, nout, jmax, prob = lomb.fasper(x, y, 10., hifac)
    ax.semilogx(1.0 / fx[:len(fy)], fy, 'r')
    draw_siglev(ax, len(x))
    draw_PN(ax, 1.0 / fx[np.argmax(fy)], np.max(fy))

    ax = plt.subplot(4, 1, 2)
    ax.errorbar(x, y, yerr=ey, fmt='ro')
    ax.errorbar(x[indices], y[indices], yerr=ey[indices], fmt='bo')

    ax = plt.subplot(4, 1, 3)
    fx, fy, nout, jmax, prob = lomb.fasper(
        np.delete(x, pairs[best]), np.delete(y, pairs[best]), 10., hifac)
    ax.semilogx(1.0 / fx[:len(fy)], fy)
    draw_siglev(ax, len(x))
    draw_PN(ax, 1.0 / fx[np.argmax(fy)], np.max(fy))

    ax = plt.subplot(4, 1, 4)
    ax.hist(periods, bins=40)

    plt.show()


def draw_LS(ax, x, y, sig=True, t0=None):
    hifac = 0.5 / (0.5 * len(x) / (np.max(x) - np.min(x)))
    fx, fy, nout, jmax, prob = lomb.fasper(x, y, 10., hifac)
    ax.semilogx(1.0 / fx[:len(fy)], fy, 'r')
    ax.set_xlim([2, np.max(1.0 / fx[:len(fy)])])
    if sig:
        draw_siglev(ax, len(x))
    #draw_PN(ax, 1.0 / fx[np.argmax(fy)], np.max(fy))
    if t0 is None:
        t0 = 1.0 / fx[np.argmax(fy)]
        ax.plot([t0, t0],
                np.array([1.0 / 3.0, 2.0 / 3.0]) * np.max(fy), 'k')
        return t0
    else:
        ax.plot([t0, t0],
                np.array([1.0 / 3.0, 2.0 / 3.0]) * np.max(fy), 'k')


def paper_figure(t, rv, bs, res, t_V, V, name):
    import matplotlib
    import matplotlib.ticker as tck
    matplotlib.rc('font', family='serif')
    matplotlib.rc('text', usetex=True)
    matplotlib.rc("axes", linewidth=1.5)
    matplotlib.rc("lines", linewidth=1.5)
    matplotlib.rc("font", size=16)

    fig = plt.figure(1, (10, 9))
    from mpl_toolkits.axes_grid1 import Grid
    grid = Grid(fig, rect=111, nrows_ncols=(4, 1),
                axes_pad=0.15, label_mode='L',
                )
    #.subplots_adjust(wspace=0.3)

    #grid[0].set_title("%s" % (name))
    t0 = draw_LS(grid[0], t, rv)
    draw_PN(grid[0], t0, 0.0)
    draw_LS(grid[1], t, bs, t0=t0)
    draw_LS(grid[2], t_V, V, t0=t0)
    draw_LS(grid[3], t, res, t0=t0)
    grid[3].set_xlabel("Period [days]")
    for ax in grid:
        ax.set_ylabel(r"$\textrm{P}_{\textrm{N}}$")
        ax.yaxis.set_major_locator(tck.MaxNLocator(4))
        ymin, ymax = ax.get_ylim()
        ax.set_ylim([ymin, ymax * 0.99])

    plt.savefig('LS_%s.pdf' % name.replace(' ', '_'), bbox='tight')


if __name__ == "__main__":
    import pickle as pickle
    with open(sys.argv[1], 'rb') as pkl_file:
        fit = pickle.load(pkl_file)

    photo = np.loadtxt(sys.argv[2], usecols=(0, 1, 2))
    t_V = photo[:, 0]
    V = photo[:, 1]
    del photo

    star = sys.argv[1].split('.')[1]
    paper_figure(fit.data['T'], fit.data['RV'], fit.data['BS'],
                 fit.data['res'], t_V, V, star.replace('_', ' '))
