#!/usr/bin/python

import h5py as h5
import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib
from yt.units.yt_array import YTQuantity, YTArray
from yt.utilities.physical_constants import gravitational_constant_cgs as G



def stderr_round(x, digit=0):
    if x == 0.0:
        return 0.0, 0.0
    if np.log10(x) <= 0:
        log = -int(np.log10(x)) + 1
    else:
        log = -int(np.log10(x))
    val = x * 10 ** log
    if (np.ceil(val) - val) > 0.1 * val:
        return np.ceil(val * 10.0) * 10.0 ** -(log + 1), -(log + 1)
    else:
        return np.ceil(val) * 10.0 ** -log, -log


def pretty_pm_format(mean, lower, upper):
    err1, log1 = stderr_round(abs(lower))
    err2, log2 = stderr_round(upper)
    return "$%g^{+%g}_{-%g}$" % (round(mean / 10.0 ** log1) * 10.0 ** log1,
                                 err2, err1)


def asini(x):
    return x[:, 2] * (x[:, 1] * 86400.0 * np.sqrt(1.0 - x[:, 3] ** 2)) / \
        (2.0 * np.pi)

def a2sini(star_mass, P):
    return 1.96e-2 * (star_mass * P ** 2) ** (1. / 3.)

def ea2sini(star_mass, P, errors):
    result = np.abs(1./3. * 1.96e-2 * star_mass ** (-2./3.) * P ** (2./3.)) * errors['ems']
    result += np.abs(2./3. * 1.96e-2 * star_mass ** (1./3.) * P ** (-1./3.)) * errors['eP']
    return YTQuantity(result, "AU")


def m2sini(star_mass, data):
    # P T e w K v0 off jit chi
    ms = YTQuantity(star_mass, 'Msun')
    K = YTArray(data[:, 4], 'm/s')
    P = YTArray(data[:, 0], 'day')
    e = YTArray(data[:, 2], '1')

    return (K * ms ** (2./3.) * np.sqrt(1 - e**2) * (np.pi * 2.0 * G / P)**(-1./3.)).in_units("Mjup")

def em2sini(star_mass, data, error):
    ms = YTQuantity(star_mass, 'Msun')
    K = YTArray(data[:, 4], 'm/s')
    P = YTArray(data[:, 0], 'day')
    e = YTArray(data[:, 2], '1')

    result = np.abs(2./3. * ms ** (-1./3.) * K * (1.0 - e ** 2) ** (1./2.) *
                    (np.pi * 2.0 * G / P) ** (-1./3.)) * YTQuantity(error['ems'], 'Msun')
    result += np.abs(-e / np.sqrt(1.0 - e ** 2) * K * ms**(2./3.) *
                     (np.pi * 2.0 * G / P)**(-1./3.)) * YTQuantity(error['ee'], '1')
    result += np.abs(ms ** (2./3.) * np.sqrt(1 - e**2) *
                     (np.pi * 2.0 * G / P)**(-1./3.)) * YTQuantity(error['eK'], 'm/s')
    result += np.abs(1./3. * P ** (-2./3.) * (np.pi * 2.0 * G) ** (-1./3.) *
                     K * ms ** (2./3.) * np.sqrt(1 - e**2)) * YTQuantity(error['eP'], 'day')
    return result.in_units("Mjup")

matplotlib.rc('font', family='serif')
matplotlib.rc('text', usetex=True)
matplotlib.rc("axes", linewidth=1.5)
matplotlib.rc("lines", linewidth=1.5)
#matplotlib.rc("font", size=16)

with h5.File(sys.argv[1], 'r') as h5f:
    data = h5f['data'][:]
    best_fit = h5f['gold'][:]

if data.shape[1] > best_fit.shape[0]:
    best_fit = np.concatenate((best_fit, [1.0]))

print(best_fit, data.shape[1])

# P T e w K v0 off jit chi
lbl = [r'$P$ [days]', r'$T_0$ [MJD]', r'$e$', r'$\omega$ [deg]', r'$K$ [m/s]']
elbl = ['eP', 'eT', 'ee', 'ew', 'eK']
errors = []

with h5.File(sys.argv[1]) as h5f:
    if not 'data' in h5f:
        h5f['data'] = data

for i in range(len(best_fit) // 5):
    errors.append({})
    errors[i]['ems'] = float(sys.argv[3])
    fig, axs = plt.subplots(nrows=2, ncols=3, figsize=(8, 8))
    plt.subplots_adjust(hspace=0.28)

    for j in range(5):
        ax = axs.ravel()[j]
        ind = j + i * 5
        if j == 3:
            data[:, ind] = (data[:, ind] - best_fit[ind] + 180.0) % 360.0 \
                - 180.0 + best_fit[ind]
        n, bins, patches = ax.hist(data[:, ind], 50, normed=0,
                                   facecolor='green', alpha=0.75)
        ax.axvline(best_fit[ind], lw=2, c='k', ls='--')
        ax.set_xlabel(lbl[j])
        ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(4))
        bounds = np.percentile(data[:, ind], (15.87, 84.13)) - best_fit[ind]
        ax.set_title(pretty_pm_format(best_fit[ind], bounds[0], bounds[1]))
        errors[i][elbl[j]] = np.abs(bounds).max()
        if (j % 3 == 0):
            ax.set_ylabel("Number of fits")
        print(("{}\t\t{}".format(lbl[j], pretty_pm_format(best_fit[ind], bounds[0], bounds[1]))))
    fig.savefig('fap_%s_pln%i.pdf' % (sys.argv[1][:-3], i + 1), bbox='tight')

lbl = [r'$v_0$ [m/s]', r'offset [m/s]', r'jitter [m/s]', r'$\sqrt{\chi_\nu^2}$']
fig, axs = plt.subplots(nrows=1, ncols=4)

for ilbl, ind in enumerate([-4, -3, -2, -1]):
    ax = axs[ilbl]
    n, bins, patches = ax.hist(data[:, ind], 50, normed=0, facecolor='green',
                               alpha=0.75)
    ax.axvline(best_fit[ind], lw=2, c='k', ls='--')
    ax.set_xlabel(lbl[ilbl])
    ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(4))
    bounds = np.percentile(data[:, ind], (15.87, 84.13)) - best_fit[ind]
    if ilbl == 2:
        print((best_fit[ind], bounds))
        continue
    print(("{}\t\t{}".format(lbl[ilbl], pretty_pm_format(best_fit[ind], bounds[0], bounds[1]))))
    #ax.set_title(pretty_pm_format(best_fit[ind], bounds[0], bounds[1]))

fig.savefig('fap_%s_off.pdf' % (sys.argv[1][:-3]), bbox='tight')

for i in range(len(best_fit) // 5):
    asini_best = a2sini(float(sys.argv[2]), best_fit[0+ i * 5])
    asini_data = a2sini(float(sys.argv[2]), data[:, 0+ i * 5])
    amass = a2sini(float(sys.argv[2]), best_fit[0+i*5])
    eamass = ea2sini(float(sys.argv[2]), best_fit[0+i*5], errors[i])
    print(("a sin i = {} +/- {}".format(amass, eamass)))
    bounds = np.percentile(asini_data, (15.87, 84.13)) - asini_best
    print(("A sini - ", np.percentile(asini_data, (15.87, 84.13)) - asini_best,
          asini_best, np.median(asini_data)))

    pmass = m2sini(float(sys.argv[2]), best_fit[0+i*5:5+i*5].reshape((1,5)))
    epmass = em2sini(float(sys.argv[2]), best_fit[0+i*5:5+i*5].reshape((1,5)), errors[i])
    print(("m2 sin i = {} +/- {}".format(pmass[0].d, epmass[0])))
    m2sini_best = m2sini(float(sys.argv[2]), best_fit[0+i*5:5+i*5].reshape((1,5)))
    m2sini_data = m2sini(float(sys.argv[2]), data[:, 0+i*5:5+i*5])
    bounds = YTArray(np.percentile(m2sini_data, (15.87, 84.13)), 'Mjup') - m2sini_best
    print(("M sini - ", np.percentile(m2sini_data.d, (15.87, 84.13)) - m2sini_best.d,
          m2sini_best, np.median(m2sini_data)))
