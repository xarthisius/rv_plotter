#!/usr/bin/env python
'''
comment me
'''





from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5 import QtCore
import ui_dialog
import numpy as np

#import matplotlib
from matplotlib.backends.backend_qt5agg \
   import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.widgets import  RectangleSelector
import matplotlib.tri as tri
import matplotlib.cm as cm
import collections

def remove_duplicates(data):
    initial_np = data.shape[0]
    x = collections.Counter( data[:, 0] )
    for dupe in  [n for n, i in x.items() if i > 1]:
        xind = np.where(np.abs(data[:, 0] - dupe) == 0.0)
        y = collections.Counter( data[xind, 1].ravel() )
        for dupe2 in [n for n, i in y.items() if i > 1]:
            yind = np.where( (data[:, 1] == dupe2) & (data[:, 0] == dupe) )
            if len(yind[0]) > 1:
                data = np.delete(data, yind[0][1:], 0)
    print("Removed %i duplicates" % (initial_np - data.shape[0]))
    return data

def validate_str(string):
    '''Convert string to float'''
    try:
        return float(string)
    except ValueError:
        return 0.0

def unique(array):
    '''Remove duplicate rows from 2D array'''
    order = np.lexsort(array.T)
    array = array[order]
    diff = np.diff(array, axis=0)
    temp = np.ones(len(array), 'bool')
    temp[1:] = (diff != 0).any(axis=1)
    return array[temp]

class InitialFitDlg(QDialog, ui_dialog.Ui_Dialog):
    '''
    Class for generating form that returns parameters for mpfit function from 
    pikaia's results
    '''

    data = None
    fig = None
    canvas = None
    ts = [0, 0, 0]


    def onselect(self, eclick, erelease):
        '''
        Convert rectangle selection made on triangulation maps to actual 
        parameters that are return by this form
        '''
        #'eclick and erelease are matplotlib events at press and release'
        # print(' startposition : (%f, %f)' % (eclick.xdata, eclick.ydata))
        # print(' endposition   : (%f, %f)' % (erelease.xdata, erelease.ydata))
        # eclick.inaxes.rowNum
        x_min = min(eclick.xdata, erelease.xdata)
        x_max = max(eclick.xdata, erelease.xdata)
        y_min = min(eclick.ydata, erelease.ydata)
        y_max = max(eclick.ydata, erelease.ydata)
        if eclick.inaxes.colNum == 0:
            self.set_LB.setCheckState(True)
            self.set_RB.setCheckState(True)
            self.set_LB_2.setCheckState(True)
            self.set_RB_2.setCheckState(True)
            self.LB.setText("%f"%x_min)
            self.RB.setText("%f"%x_max)
            self.value.setText("%f"%(0.5*(x_max+x_min)))
            self.LB_2.setText("%f"%y_min)
            self.RB_2.setText("%f"%y_max)
            self.value_2.setText("%f"%(0.5*(y_max+y_min)))
        elif eclick.inaxes.colNum == 1:
            self.set_LB_3.setCheckState(True)
            self.set_RB_3.setCheckState(True)
            self.set_LB_4.setCheckState(True)
            self.set_RB_4.setCheckState(True)
            self.LB_3.setText("%f"%x_min)
            self.RB_3.setText("%f"%x_max)
            self.value_3.setText("%f"%(0.5*(x_max+x_min)))
            self.LB_4.setText("%f"%y_min)
            self.RB_4.setText("%f"%y_max)
            self.value_4.setText("%f"%(0.5*(y_max+y_min)))
        elif eclick.inaxes.colNum == 2:
            self.set_LB_5.setCheckState(True)
            self.set_RB_5.setCheckState(True)
            self.set_LB_6.setCheckState(True)
            self.set_RB_6.setCheckState(True)
            self.LB_5.setText("%f"%x_min)
            self.RB_5.setText("%f"%x_max)
            self.value_5.setText("%f"%(0.5*(x_max+x_min)))
            self.LB_6.setText("%f"%y_min)
            self.RB_6.setText("%f"%y_max)
            self.value_6.setText("%f"%(0.5*(y_max+y_min)))


    def __init__(self, parent=None, init=None):
        super(InitialFitDlg, self).__init__(parent)
        self.setupUi(self)

        self.tri_ax = []
        self.dpi = 92
        if init != None:
            self.data = init
            initial_data = self.data[np.argmin(self.data[:, -2]), :]
            self.value.setText("%f" % initial_data[0])
            self.value_2.setText("%f" % initial_data[1])
            self.value_3.setText("%f" % initial_data[2])
            self.value_4.setText("%f" % initial_data[3])
            self.value_5.setText("%f" % initial_data[4])
            self.value_6.setText("%f" % initial_data[5])
        else:
            self.data = np.random.randn(100, 8)

        self.data = unique(self.data) # prevent bug in matplotlib
        self.prep_plots()
        self.do_voodoo()

    def prep_plots(self):
        '''
        Create three subplots for triangulated maps from pikaia's data
        '''
        if self.fig == None:
            self.fig = Figure(dpi=self.dpi)
        if self.canvas == None:
            self.canvas = FigureCanvas(self.fig)
            self.canvas.setParent(self)
            self.canvas.setGeometry(QtCore.QRect(375, 1, 3*390, 400))

        if len(self.tri_ax) > 0: 
            return

        for i in range(0, 3):
            self.tri_ax.append(self.fig.add_subplot(131+i))

    def do_voodoo(self):
        '''
        Create triangulated maps from pikaia's data to ease parameter choice
        '''
        xlbl = ['$T_0$ [days]', 'K [m/s]', '$\omega$ [deg]']
        ylbl = ['P [days]', 'e', '$v_0$ [m/s]']
        for i in range(0, 3):
            self.tri_ax[i].clear()
            data = remove_duplicates( np.array([self.data[:,2*i],
               self.data[:,2*i+1], self.data[:,-2]]).T )
            triang = tri.Triangulation(data[:, 0], data[:, 1])
            self.tri_ax[i].tricontour(triang, data[:, 2], linewidths=0.5, colors='k')
            self.tri_ax[i].tricontourf(triang, data[:, 2], 
                                   cmap=cm.get_cmap(name="jet"))
            self.tri_ax[i].plot(data[:, 0], data[:, 1], 'ko', ms=3)
            self.tri_ax[i].set_xlabel(xlbl[i])
            self.tri_ax[i].set_ylabel(ylbl[i])
            self.ts[i] = RectangleSelector(self.tri_ax[i], self.onselect, 
                                           drawtype='box')
        self.fig.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.12)
        self.canvas.draw()

    def result(self):
        '''
        Return parameters consumed by mpfit, i.e. [T0, P, K, e, om, v0]
        '''
        return ([
            {'value': validate_str(self.value.text()), 
              'fixed': self.Fix.isChecked(),
              'limited': [self.set_LB.isChecked(), 
                          self.set_RB.isChecked()],
              'limits': [validate_str(self.LB.text()), 
                         validate_str(self.RB.text())]},
            {'value': validate_str(self.value_2.text()), 
             'fixed': self.Fix_2.isChecked(),
             'limited': [self.set_LB_2.isChecked(), 
                         self.set_RB_2.isChecked()],
             'limits': [validate_str(self.LB_2.text()), 
                        validate_str(self.RB_2.text())]},
            {'value': validate_str(self.value_3.text()),
             'fixed': self.Fix_3.isChecked(),
             'limited': [self.set_LB_3.isChecked(), 
                         self.set_RB_3.isChecked()],
             'limits': [validate_str(self.LB_3.text()), 
                        validate_str(self.RB_3.text())]},
            {'value': validate_str(self.value_4.text()), 
             'fixed': self.Fix_4.isChecked(),
             'limited': [self.set_LB_4.isChecked(), 
                         self.set_RB_4.isChecked()],
             'limits': [validate_str(self.LB_4.text()), 
                        validate_str(self.RB_4.text())]},
            {'value': validate_str(self.value_5.text()), 
             'fixed': self.Fix_5.isChecked(),
             'limited': [self.set_LB_5.isChecked(), 
                         self.set_RB_5.isChecked()],
             'limits': [validate_str(self.LB_5.text()), 
                        validate_str(self.RB_5.text())]},
            {'value': validate_str(self.value_6.text()), 
             'fixed': self.Fix_6.isChecked(),
             'limited': [self.set_LB_6.isChecked(), 
                         self.set_RB_6.isChecked()],
             'limits': [validate_str(self.LB_6.text()), 
                        validate_str(self.RB_6.text())]}
        ])


if __name__ == "__main__":
    import sys
    APP = QApplication(sys.argv)
    FORM = InitialFitDlg()
    FORM.show()
    APP.exec_()
    print(FORM.result())
