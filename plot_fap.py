#!/usr/bin/python

import h5py as h5
import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rc('font', family='serif')
matplotlib.rc('text', usetex=True)
matplotlib.rc("axes", linewidth=1.5)
matplotlib.rc("lines", linewidth=1.5)
matplotlib.rc("font", size=16)

chi2 = []
h5f = h5.File(sys.argv[1], 'r')
for grp in list(h5f.keys()):
    for perm in list(h5f[grp].keys()):
        chi2.append(h5f[grp][perm]['fit'].attrs['chi2'])
h5f.close()

best_star1 = 1.120537
best_star2 = 2.211692
best_star3 = 2.335600
best_star3b = 1.778272
best_TYC_3318_01515 = 0.6684
best_TYC_0017_01084 = 1.0234
best_TYC_3227_00413 = 0.9455
best_TYC_3917_01107 = 0.893

best = eval("best_%s" % sys.argv[1][:-3])

chi2 = np.sort(np.array(chi2))
fap = (chi2 < best).sum() / float(chi2.shape[0]) * 100.0
for i in range(1,chi2.shape[0]):
    tail = (chi2 > chi2[-i]).sum() / float(chi2.shape[0]) * 100.0
    if tail > 0.5:
        cutoff = chi2[-i]
        break
chi2[chi2 > cutoff] = np.ceil(cutoff)

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, patches = ax.hist(chi2, 50, normed=0, facecolor='green', alpha=0.75)
ax.set_xlim([0.5 * best, np.ceil(cutoff)])
ax.axvline(best, lw=2, c='k', ls='--')
ylim = ax.get_ylim()
ax.text(1.5 * best, 0.95 * ylim[-1], "FAP = %5.2f\\%%" % fap)
ax.set_title(sys.argv[1][:-3].replace('_', ' '))
ax.set_xlabel(r'$\sqrt{\chi^2}$ [m/s]')
ax.set_ylabel("Number of fits")
plt.savefig('fap_%s.pdf' % sys.argv[1][:-3], bbox='tight')
