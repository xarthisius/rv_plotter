module rvlin

   implicit none
   real(kind=8), parameter :: pi = 3.141592653589793d0
   real(kind=8), parameter :: dtor = pi/180.d0
   enum, bind(C)
      enumerator :: I_P = 1, I_TP, I_E
   end enum
   
   real(kind=8), dimension(:), allocatable :: K, omega, offset, gam

contains
   ! Returns the inverse of a matrix calculated by finding the LU
   ! decomposition.  Depends on LAPACK.
   function inv(A) result(Ainv)
     real(kind=8), dimension(:,:), intent(in) :: A
     real(kind=8), dimension(size(A,1),size(A,2)) :: Ainv

     real(kind=8), dimension(size(A,1)) :: work  ! work array for LAPACK
     integer, dimension(size(A,1)) :: ipiv   ! pivot indices
     integer :: n, info

     ! External procedures defined in LAPACK
     external DGETRF
     external DGETRI

     ! Store A in Ainv to prevent it from being overwritten by LAPACK
     Ainv = A
     n = size(A, 1)

     ! DGETRF computes an LU factorization of a general M-by-N matrix A
     ! using partial pivoting with row interchanges.
     call DGETRF(n, n, Ainv, n, ipiv, info)

     if (info /= 0) then
        stop 'Matrix is numerically singular!'
     end if

     ! DGETRI computes the inverse of a matrix using the LU factorization
     ! computed by DGETRF.
     call DGETRI(n, Ainv, n, ipiv, work, n, info)

     if (info /= 0) then
        stop 'Matrix inversion failed!'
     end if
   end function inv

   function kepler(M, e)
      implicit none
      real(kind=8), intent(in), dimension(:) :: M
      real(kind=8), intent(in) :: e
      real(kind=8), dimension(size(M)) :: kepler

      real(kind=8), dimension(size(M)) :: eccarr, mphase, fiarr, Earr
      real(kind=8), dimension(size(M)) :: d1, d2, d3, fip, fipp, fippp
      logical, dimension(size(M)) :: convd
      real(kind=8) :: conv, k 
      integer :: cnt

      eccarr = e

      conv = 1e-12
      k = 0.85
      mphase = (0.5 * M / pi)
      where (mphase < 0.0)
         mphase = mphase + 1.0
      endwhere
      ! sign(sin(Marr)), but faster
      ! ssm = int(mphase <= 0.5) - in((mphase >= 0.5) .or. (mphase == 0)))
      Earr = M + sign(1.d0, sin(M)) * k * eccarr
      fiarr = (Earr - eccarr * sin(Earr) - M)
      convd = abs(fiarr) > conv
      cnt = 0
      do while (any(convd) .or. (cnt < 100))
         cnt = cnt + 1
         where (convd)
            fip = 1.0 - eccarr * cos(Earr)
            fipp = eccarr * sin(Earr)
            fippp = 1.0 - fip
         
            d1 = -fiarr / fip
            d2 = -fiarr / (fip + 0.5 * d1 * fipp)
            d3 = -fiarr / (fip + 0.5 * d2 * fipp + d2**2 * fippp / 6.0)
         
            Earr = Earr + d3
            fiarr = (Earr - eccarr * sin(Earr) - M)
         endwhere   
         convd = abs(fiarr) > conv
      enddo
      kepler = Earr
   end function kepler

   function calcnu(t, n, P, tp, e)
      implicit none
      integer, intent(in) :: n
      real(kind=8), dimension(n), intent(in) :: t
      real(kind=8), intent(in) :: P, tp , e
      real(kind=8), dimension(n) :: calcnu

      real(kind=8), dimension(n) :: phase, M, E1

      phase = (t - tp) / P
      M = 2.0 * pi * (phase - floor(phase))
      E1 = kepler(M, e)

      calcnu = 2.0 * atan(sqrt((1.0+e)/(1.0-e)) * tan(0.5 * E1))
   end function calcnu

   function rvlin_main(t, v, e, tel, pars) result(vout)
      implicit none
      real(kind=8), dimension(:), intent(in) :: t, v, e
      integer, dimension(:), intent(in) :: tel
      real(kind=8), dimension(:,:), intent(in) :: pars
      real(kind=8), dimension(size(t)) :: vout

      integer :: npln, ntels, ntrans, nt, j, i, nf, v0_index, ind
      real(kind=8), dimension(:,:), allocatable :: F, W, WFT, eps, FT
      real(kind=8), dimension(:), allocatable :: vW, nu, beta, u

      ntels = maxval(tel)+1
      npln = size(pars, 2)
      nt = size(t)
      ntrans = 0

      if (.not.allocated(K)) allocate(K(npln))
      if (.not.allocated(omega)) allocate(omega(npln))
      if (.not.allocated(gam)) allocate(gam(npln))
      if (.not.allocated(offset)) allocate(offset(ntels))

      nf = 2*npln + ntels - 1 + 1
      allocate(F(nf, nt))
      do j = 1, npln
         nu = calcnu(t, size(t), pars(I_P, j), pars(I_TP, j), pars(I_E, j))
         F(1 + (j-1)*2, :) = cos(nu)
         F(2 + (j-1)*2, :) = sin(nu)
      enddo
      F(2*npln + 1, :) = 1.0
      do i = 2, ntels
         where(tel == (i-1))
            F(2*npln + i, :) = 1.0
         elsewhere
            F(2*npln + i, :) = 0.0
         endwhere
      enddo

      allocate(W(nt, nt))
      W = 0.0
      do i = 1, nt
         W(i, i) = 1.d0/e(i)**2
      enddo
      vW = v / e ** 2
      WFT = matmul(W, transpose(F))
      deallocate(W)

      ! This is the "curvature matrix" or "variance covariance matrix"
      eps = inv(matmul(F, WFT))
      deallocate(WFT)
      ! solution to linear least-squares problem, a in Bevington
      beta = matmul(matmul(vW, transpose(F)), eps)
      deallocate(vW, eps)
      vout = matmul(beta, F)  ! definition of the model and result

      deallocate(F)
  
      ! This is irrelevant for pikaia
      !K = 0.0
      !omega = 0.0
      !offset = 0.0
      !v0_index = npln * 2 - ntrans + 1
      !ind = 0

      !do i = 1, npln
      !   K(i) = sqrt(beta(1 + ind)**2 + beta(2 + ind)**2)
      !   omega(i) = mod(atan2(-beta(2+ind), beta(1+ind))/dtor + 360.0, 360.0)
      !   ind = ind+2
      !enddo

      !if (ntels > 1) offset(2:) = beta(v0_index + 1:)
      !! This looks fishy from python's POV 
      !gam = beta(v0_index) - sum(K * pars(I_E,:) * cos(omega*dtor))
      deallocate(beta)
   end function rvlin_main

   function rv_drive(t, orbel) result(rv)
      implicit none
      real(kind=8), dimension(:), intent(in) :: t
      real(kind=8), dimension(:,:), intent(in) :: orbel
      real(kind=8), dimension(size(t)) :: nu, rv

      real(kind=8) :: p, tp, e, om, k, gam, curv, dvdt
      integer :: i
         
      rv = 0.0
      do i = lbound(orbel, 2), ubound(orbel, 2)
         p = orbel(1, i)
         tp = orbel(2, i)
         e = orbel(3, i)
         om = orbel(4, i) * dtor
         k = orbel(5, i)
         gam = orbel(6, i)
         dvdt = orbel(7, i)
         curv = orbel(8, i)
!        if (i == 0) and len(orbel)/7*7 == len(orbel) - 1:
!            curv = orbel[-1]
 
         p = max(0.01, p)
         k = max(0.01, k)
         e = min(max(0.0, e), 0.99)
         nu = calcnu(t, size(t), p, tp, e)
         rv = rv + k * (cos(nu + om) + e * cos(om)) ! + gam + dvdt * (t) + curv * (t) ** 2
      enddo
      rv = rv + gam + dvdt * (t) + curv * (t) ** 2

   end function rv_drive

end module rvlin 

program test
   use rvlin, only: rvlin_main, K, omega, gam, offset, rv_drive
   implicit none
   real(kind=8), dimension(:), allocatable :: t, v, e, nu
   integer, dimension(:), allocatable :: tel, dev
   real(kind=8) :: ti, vi, ei, chi2, chi, rms
   integer :: teli
   integer :: i, ierr
   real(kind=8), dimension(3, 2) :: pars
   real(kind=8), dimension(7, 2) :: pars2

   pars(:, 1) = [199.13598285, 53216.46821614, 0.12533725]
   pars(:, 2) = [557.82026295, 53842.26183377, 0.08500666]

   pars2(:, 1) = [199.13598285, 53216.46821614, 0.12533725, 30.424843583897200, &
                  81.669123610004419, -18.970375202957893, 0.0]
   pars2(:, 2) = [557.82026295, 53842.26183377, 0.08500666, 183.19800654116364, &
                  230.07701370023628, -18.970375202957893, 0.0]

   allocate(t(0), v(0), e(0), tel(0))

   open(1,file="2p.dat", status='old')
   ierr = 0
   do
      read(1, *, iostat=ierr) ti, vi, ei, teli
      if (ierr /= 0) exit
      t = [t, ti]
      v = [v, vi]
      e = [e, ei]
      tel = [tel, teli]
   enddo
   close(1)

   allocate(nu(size(t)))
   v(::3) = v(::3) + 10.0
   tel(::3) = 1
   nu = rvlin_main(t, v, e, tel, pars)
   print *, 'K', K
   print *, 'omega', omega
   print *, 'offset', offset
   print *, 'gam', gam
   

   dev = v - rv_drive(t, pars2)
   print *, shape(dev)
   chi2 = sum((dev / e) ** 2)
   chi  = sqrt(chi2 / real(size(t) - 7) )
   rms = sqrt( sum(dev**2) / real( size(t) - 7))
   
   print *, "Reduced chi^2 = ", chi**2
   print *, "RMS = ", rms
   
   deallocate(t, nu)

end program test
