module func
   
   implicit none
   integer, save :: fmin = 100, fmax = 1

contains
!-----------------------------------------------------------------------
!     Routine to compute RV component of a star in a multiple system.
!     Alex Wolszczan, June 1988, mods: Feb 2005, March 2008.
!-----------------------------------------------------------------------
      function yfun_orbits_new(obsepoch,a) result(rv)
!-----------------------------------------------------------------------
!     The I/O parameters (first orbit) are:
!     * obsepoch    -  MJD of observation (days)             (I)
!  a1 * T0          -  Epoch of periastron (MJD)             (I)
!  a2 * Pb          -  Orbital period (days)                 (I)
!  a3 * K           -  Semi-amplitude (m/s)                  (I)
!  a4 * e           -  Eccentricity of the orbit             (I)
!  a5 * omega       -  Longitude of periastron (deg)         (I)
!     * yfun_orbits -  Radial velocity (m/s)                 (O)
!     All parameters are declared real*8.
!-----------------------------------------------------------------------
      implicit none
      real(kind=8), intent(in) :: obsepoch
      real(kind=8), intent(in), dimension(:) :: a
      real(kind=8) :: rv
      integer, parameter :: maxorb = 1
      integer(kind=4), parameter :: norbits = 1
      real(kind=8), parameter :: pi=3.1415926535897932d0
      real(kind=8), parameter :: fact2=pi/180.d0
      real(kind=8), parameter :: twopi=2.d0*pi
      real(kind=8), parameter :: fact1=86400.d0
      real(kind=8), parameter :: c=2.99792456d5
      real(kind=8), parameter :: eps=1.d-12

      real(kind=8) :: T0,Pb,a1sini,e,omega,omegadot,voffset,phase,denom
      real(kind=8) :: omega0,timediff,M,mu,E0,E1,tantheta,&
               theta,c1,c2,yf

      real(kind=8) :: pharray(maxorb),trarray(maxorb)
      real(kind=8) :: mrvorbv(maxorb),mrvorbtrdv(maxorb)
!-----------------------------------------------------------------------
!     Local varaibles.
!-----------------------------------------------------------------------
      integer :: i,j,ii,indx
!-----------------------------------------------------------------------

      T0 = a(1)
      Pb = a(2)
      a1sini = a(3)*(a(2)*86400.d0*sqrt(1.d0-a(4)**2))/6.283185307d0
      e = a(4)
      omega = a(5)

!-----Compute time elapsed since the periastron passage (days)
      timediff = abs(obsepoch)-T0

!-----Convert longitude of periastron to radians
      omega0 = mod(omega*fact2,twopi)

!-----Now, calculate mean anomaly (rad) at the given epoch
      mu = twopi/(Pb*fact1)
      M = mod(mu*timediff*fact1,twopi)
      phase = mod(M/twopi,1.0)
      if (phase < 0.0) phase = phase + 1.0  !can get it out, if necessary
      phase =twopi*phase

!-----Compute eccentric anomaly by means of Wallace's method
      E0=phase+e*sin(phase)*(1.0+e*cos(phase))
      denom=1.0-e*cos(E0)
      E1 = 2.0*eps
      ii = 1
      do while( abs(E1) > eps )
         E1=(phase-(E0-e*sin(E0)))/denom
         E0=E0+E1
         ii = ii + 1
         if (ii > 20) then
            ! write(*,*) "Wallace went bananas :/" 
            exit
         endif
      enddo

      fmax = max(fmax,ii)
      fmin = min(fmin,ii)

!-----Calculate true anomaly
      tantheta = sqrt((1.0+e)/(1.0-e))*tan(E0*0.5)
      theta = 2.0*atan(tantheta)
      if (obsepoch < 0.0 .and. theta < 0.0) theta=theta+twopi

!-----Compute radial velocity 
      c1 = mu*a1sini/sqrt(1.0-e*e)
      c2 = e*cos(omega0)+cos(theta+omega0)
      rv = c1*c2
      return
   end function yfun_orbits_new
!-----------------------------------------------------------------------
end module func
