module tabs
   implicit none

   real(kind=8), dimension(:), pointer :: fx, fy, fe, minaga, maxaga
   integer, dimension(:), pointer :: ftel
   integer, save :: norbits = 1
   integer, save :: off_ord = 0
   interface
      function eval_func(n,x) result(fn_val)
         implicit none
         integer, intent(in)  :: n
         real, intent(in), dimension(:) :: x
         real(kind=4) :: fn_val
      end function eval_func
   end interface

   procedure(eval_func), pointer :: func

contains

   function eval_rvlin_jit(n, x) result(fn_val)
      use rvlin, only: rvlin_main, rv_drive, offset

      implicit none
      integer, intent(in)  :: n
      real, intent(in), dimension(:) :: x

      real(kind=4) :: fn_val

      !     Local
      real(kind=8), parameter :: dpi         = 2.0*3.141592653589793238  !< Pi (Archimedes' constant)
      real(kind=8), dimension(n) :: a
      real(kind=8) :: RedChi2
      real(kind=8), dimension(3, norbits) :: pars
      real(kind=8), dimension(size(fx)) :: rv
      integer :: i, j, k

      a = real(x(1:n), kind=8) * (maxaga - minaga) + minaga

      ! [P, T0, e, ..., jit]
      ! ['T0', 'P', 'K', 'e', 'w']  !!! FIXME
      do j = 0, norbits-1
         pars(:, j+1) = a(j * 3 + 1:j * 3 + 3)
      enddo
      rv = rvlin_main(fx, fy, fe, ftel, pars)

      RedChi2 = sum(log(sqrt(dpi * (fe + a(n))**2)) + 0.5*((fy - rv)/(fe + a(n)))**2) &
                 / (dble(size(fy) - 4))
      fn_val = 1.0 / real(RedChi2)

   end function eval_rvlin_jit

   function eval_rvlin(n, x) result(fn_val)
      use rvlin, only: rvlin_main, rv_drive, offset

      implicit none
      integer, intent(in)  :: n
      real, intent(in), dimension(:) :: x

      real(kind=4) :: fn_val

      !     Local
      real(kind=8), dimension(n) :: a
      real(kind=8) :: RedChi2
      real(kind=8), dimension(3, norbits) :: pars
      real(kind=8), dimension(size(fx)) :: rv
      integer :: i, j, k

      a = real(x(1:n),kind=8)*(maxaga-minaga)+minaga

      ! ['T0', 'P', 'K', 'e', 'w']  !!! FIXME
      ! -> [ P, T_0, e]
      do j = 0, norbits-1
         pars(:, j+1) = [a(j*5+2), a(j*5+1), a(j*5+4)]
      enddo
      rv = rvlin_main(fx, fy, fe, ftel, pars)
      RedChi2 = sum( ((fy - rv)/fe)**2 ) / (dble(size(fy) - 3))
      fn_val = 1.0 / real(RedChi2)
   
   end function eval_rvlin

   function twod(n,x) result(fn_val)
      use func, only: yfun_orbits_new

      implicit none
      integer, intent(in)  :: n
      real, intent(in), dimension(:) :: x

      real(kind=4) :: fn_val

      !     Local
      real(kind=8), dimension(n) :: a
      real(kind=8) :: Chi2, RedChi2, vr
      integer :: i, j, k

      a = real(x(1:n),kind=8)*(maxaga-minaga)+minaga

      Chi2 = 0.0
      do i=lbound(fy,1),ubound(fy,1)
         vr = 0.0
         do j = 0, norbits - 1
            vr = vr + yfun_orbits_new(fx(i),a((j*5)+1:(j*5)+5 ))
         enddo
         j = norbits * 5
         do k = 1, off_ord
            vr = vr + a(j+k) * fx(i)**(k-1)
         enddo
         Chi2 = Chi2 + ((fy(i)-vr)/fe(i))**2
      enddo
      RedChi2=Chi2/(dble(size(fy)-n))
      fn_val = 1.0 / real(RedChi2)
   
   end function twod
end module tabs

subroutine pypikaia(T, RV, eRV, tel, nn, gseed, ctrl, mina, maxa, iters, method, x, f, stat)
 
   use Genetic_Algorithm, only: rninit, pikaia
   use tabs
   use omp_lib

   implicit none
   integer  :: seed

   integer, intent(in) :: nn, iters, method
   real(kind=8), dimension(0:nn-1), target, intent(in) :: T, RV, eRV
   integer, dimension(0:nn-1), target, intent(in) :: tel
   integer, intent(in) :: gseed
   real(kind=8), dimension(0:11), intent(in) :: ctrl
   real(kind=8), dimension(:), target, intent(in) :: mina, maxa

   real(kind=8), dimension(iters, size(mina)), intent(out) :: x
   real(kind=8), dimension(iters), intent(out)    :: f
   integer, dimension(iters), intent(out) :: stat

   real :: f_
   real(kind=4), dimension(12) :: ctrl_
   real, dimension(size(mina)) :: x_
   integer :: stat_, iter, thread_id, nloops

   fx => T
   fy => RV
   fe => eRV
   ftel => tel
   minaga => mina
   maxaga => maxa
 
   select case (method)
      case (2)
         norbits = (size(mina) - 1) / 3
         off_ord = 0
         func => eval_rvlin_jit
      case (1)
         norbits = size(mina) / 3
         off_ord = 0
         func => eval_rvlin
      case (0)
         norbits = size(mina) / 5
         off_ord = mod(size(mina), 5)
         func => twod
   end select

   seed = gseed
   call rninit(seed)
   ctrl_ = real(ctrl,kind=4)

   nloops = 0
   !$OMP PARALLEL SHARED(nloops)
   thread_id = omp_get_thread_num()
   !$OMP DO
   do iter = 1, iters
      call pikaia(func, size(mina), ctrl_, x_, f_, stat_)
      x(iter, :) = real(x_,kind=8)*(maxa-mina) + mina
      f(iter) = 1./f_
      stat(iter) = stat_
      nloops = nloops + 1
      write(*,'("Completed ",i6,"/",i6," of Pikaia iterations",a1)', advance="no") nloops, iters, char(13)
   enddo
   !$OMP END PARALLEL
   return
end subroutine pypikaia
