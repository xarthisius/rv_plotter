#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Pikaia's dialog
"""

DataLabels = {'Planet': ['T0', 'P', 'K', 'e', 'w'],
              'Offset': ['v0', 'v1', 'v2']}

import sys
from PyQt5 import QtCore
from PyQt5.QtWidgets import \
    QWidget, QDialog, QDoubleSpinBox, QLabel, \
    QGridLayout, QComboBox, QPushButton, \
    QHBoxLayout, QVBoxLayout, QApplication

class MyField(QWidget):

    def __init__(self, Dataset, Param, parent=None):
        super(MyField, self).__init__()
        self.parent = parent
        self.Param = Param
        self.Dataset = Dataset

        self.widgets = [QLabel(self), QDoubleSpinBox(self), QDoubleSpinBox(self)]
        self.widgets[0].setText(self.Param)
        for i in range(1, 3):
            self.widgets[i].setValue(0.0)
            self.widgets[i].setMaximum(999999.0)
            self.widgets[i].setMinimum(-999999.0)
            self.widgets[i].editingFinished.connect(self.textChnged)

    def textChnged(self):
        self.parent.MainWidget.ValueChanged(self.Dataset, self.Param,
                [self.widgets[1].value(), self.widgets[2].value()])


class ParamGrid(QWidget):

    ParamItems = {}

    def __init__(self, Dataset, parent=None):
        super(ParamGrid, self).__init__()
        self.MainWidget = parent

        params_grid = QGridLayout()
        for n, Param in enumerate(DataLabels[Dataset]):
            self.ParamItems[Param] = (MyField(Dataset, Param, self))
            for itm, widg in enumerate(self.ParamItems[Param].widgets):
                params_grid.addWidget(widg, n+1, itm+1)
        self.setLayout(params_grid)


class pik_dial(QDialog):

    DataTable = {}

    def __init__(self, nplanets, parent=None, initvals=None):
        super(pik_dial, self).__init__(parent)

        self.nplanets = nplanets
        self.planetsIDs = ["Planet %i" % (i+1) for i in range(self.nplanets)]
        self.Current = {'Planet': self.planetsIDs[0] if len(self.planetsIDs) else None,
                        'Offset': 'Offset'}

        self.planets = QComboBox()
        self.PlanetFitForm = ParamGrid('Planet', self)
        self.offsets = QComboBox()
        self.OffsetsForm = ParamGrid('Offset', self)
        self.okButton = QPushButton("OK")
        self.cancelButton = QPushButton("Cancel")

        self.initUI()
        self.InitVals(initvals)
        self.UpdateVals()
        self.UpdateOffs()
        self.show()

    def initUI(self):

        for planetID in self.planetsIDs:
            self.planets.addItem(planetID)
        self.planets.setCurrentIndex(self.planetsIDs.index(self.Current['Planet']))

        for offset in ['None', 'Constant', 'Linear', 'Quadratic']:
            self.offsets.addItem(offset)
        self.offsets.setCurrentIndex(0)

        hbox = QHBoxLayout()
        hbox.addWidget(self.okButton)
        hbox.addWidget(self.cancelButton)

        vbox = QVBoxLayout()
        vbox.addWidget(self.planets)
        vbox.addWidget(self.PlanetFitForm)
        vbox.addWidget(self.offsets)
        vbox.addWidget(self.OffsetsForm)

        vbox.addLayout(hbox)

        self.setLayout(vbox)

        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('Buttons')

        self.planets.currentIndexChanged.connect(self.UpdateVals)
        self.offsets.currentIndexChanged.connect(self.UpdateOffs)
        self.cancelButton.clicked.connect(self.button_action)
        self.okButton.clicked.connect(self.button_action)

    def button_action(self):
        if self.sender().text() == 'Cancel':
            self.DataTable = None
            self.reject()
        self.accept()

    def InitVals(self, initvals):
        sane_init = { 'T0': [50000.0, 55000.0], 'P': [10.0, 1000.0],
                      'K': [5.0, 500.0], 'e': [0.0, 0.9], 'w': [0.0, 360.0] }
        for planetID in self.planetsIDs:
            self.DataTable[planetID] = {param: sane_init[param] for param in DataLabels['Planet']}
            if isinstance(initvals, dict):
                if planetID in list(initvals.keys()):
                    self.DataTable[planetID].update(initvals[planetID])
        self.DataTable['Offset'] = {param:[0.0, 0.0] for param in DataLabels['Offset']}
        if isinstance(initvals, dict):
            if 'Offset' in list(initvals.keys()):
                self.DataTable['Offset'].update(initvals['Offset'])
                self.offsets.setCurrentIndex(len(list(initvals['Offset'].keys())))

    def UpdateVals(self):
        self.Current['Planet'] = str(self.planets.currentText())

        for Param in DataLabels['Planet']:
            self.PlanetFitForm.ParamItems[Param].widgets[1].setValue(
                    self.DataTable[self.Current['Planet']][Param][0])
            self.PlanetFitForm.ParamItems[Param].widgets[2].setValue(
                    self.DataTable[self.Current['Planet']][Param][1])

        for ei, Param in enumerate(DataLabels['Offset']):
            if Param in self.DataTable['Offset']:
                self.OffsetsForm.ParamItems[Param].widgets[1].setValue(
                    self.DataTable['Offset'][Param][0]
                )
                self.OffsetsForm.ParamItems[Param].widgets[2].setValue(
                    self.DataTable['Offset'][Param][1]
                )

    def UpdateOffs(self):
        for ei, Param in enumerate(DataLabels['Offset']):
            enable = ei < self.offsets.currentIndex()
            self.OffsetsForm.ParamItems[Param].widgets[1].setEnabled(enable)
            self.OffsetsForm.ParamItems[Param].widgets[2].setEnabled(enable)
            if not enable and Param in self.DataTable['Offset']:
                del self.DataTable['Offset'][Param]
            if enable and Param not in self.DataTable['Offset']:
                self.DataTable['Offset'][Param] = [
                    self.OffsetsForm.ParamItems[Param].widgets[1].value(),
                    self.OffsetsForm.ParamItems[Param].widgets[2].value(),
                ]

    def ValueChanged(self, Dataset, Param, newValue):
        self.DataTable[self.Current[Dataset]][Param] = newValue


    def result(self):
        return (self.DataTable)


def parse_pik_dial(foo):
    from re import match
    from numpy import array, float64
    par_order = DataLabels['Planet']
    mina = []
    maxa = []

    planets = [item for item in list(foo.keys()) if match("Planet",item)]
    for p in planets:
        mina += [foo[p][par][0] for par in par_order]
        maxa += [foo[p][par][1] for par in par_order]

    for par in DataLabels['Offset']:
        if par in foo['Offset']:
            mina.append(foo['Offset'][par][0])
            maxa.append(foo['Offset'][par][1])
    return array(mina, dtype=float64), array(maxa, dtype=float64)


def main():

    ddd = {'Offset': {'v2':[1,2]} }
    app = QApplication(sys.argv)
    form = pik_dial(nplanets=3, initvals=ddd)
    form.show()
    app.exec_()
    a = form.result()
    print(a)
    a, b = parse_pik_dial(a)
    print(a)
    print(b)

if __name__ == '__main__':
    main()
