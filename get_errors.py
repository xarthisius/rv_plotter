#!/usr/bin/python

import os
import numpy as np
import random
import tempfile
import argparse
from mpi4py import MPI
from mpfit import mpfit
from my_fit import keplerian_fit
from fit import Fit
import h5py as h5
import shutil
import glob
import copy
import pickle as pickle


def random_permutations(l, n):
    """http://stackoverflow.com/questions/10232338/"""
    while n:
        random.shuffle(l)
        yield list(l)
        n -= 1


def flatten(lst):
    for x in lst:
        if isinstance(x, list):
            for x in flatten(x):
                yield x
        else:
            yield x


def old_mpfit(data, p0, parinfo):
    from my_fit import myfunc

    DataLabels = {'Planet': ['T0', 'P', 'K', 'e', 'w'],
                  'Offset': ['v0', 'v1', 'v2']}

    m = mpfit(myfunc, p0, parinfo=parinfo, quiet=True,
              functkw={'x': data['T'], 'y': data['RV'], 'err': data['eRV']})

    nplanets = len(m.params) // 5
    noffsets = len(m.params) % 5
    fit = {}
    err = {}
    ind = 0
    for planet in range(nplanets):
        planetID = "Planet %i" % (planet + 1)
        fit[planetID] = \
            {param: 0.0 for param in DataLabels['Planet']}.copy()
        err[planetID] = \
            {param: 0.0 for param in DataLabels['Planet']}.copy()
        for param in DataLabels['Planet']:
            fit[planetID][param] = m.params[ind]
            try:
                err[planetID][param] = m.perror[ind]
            except TypeError:
                err[planetID][param] = 0.0
            ind += 1
    fit['Offset'] = \
        {param: 0.0 for param in DataLabels['Offset']}.copy()
    err['Offset'] = \
        {param: 0.0 for param in DataLabels['Offset']}.copy()
    for offset in range(noffsets):
        fit['Offset'][DataLabels['Offset'][offset]] = m.params[ind]
        try:
            err['Offset'][DataLabels['Offset'][offset]] = m.perror[ind]
        except TypeError:
            err['Offset'][DataLabels['Offset'][offset]] = 0.0
        ind += 1
    return fit, err, nplanets, np.zeros(nplanets).tolist()


def new_mpfit(data, p0, parinfo):
    from rvlin import myfunc, rvlin
    from fit_dialog import DataLabels
    nplt = len(p0) // 5
    my_p0 = []
    my_info = []
    for iplt in range(nplt):
        ind = iplt * 5
        my_p0 += [p0[1 + iplt * 5], p0[0 + iplt * 5],
                  p0[3 + iplt * 5]]
        my_info += [parinfo[1 + iplt * 5], parinfo[0 + iplt * 5],
                    parinfo[3 + iplt * 5]]

    m = mpfit(myfunc, my_p0, parinfo=my_info, quiet=True,
              functkw={'x': data['T'], 'y': data['RV'],
                       'err': data['eRV'], 'tel': data['tel']})
    fp = rvlin(data, m.params, nompfit=True)
    a = fp['pars']
    b = np.reshape(a, (7, nplt))
    npars = (len(a) % 5) // nplt
    new_params = []
    for iplt in range(nplt):
        new_params += [b[1][iplt], b[0][iplt], b[4][iplt], b[2][iplt],
                       b[3][iplt]]
    for ipar in range(npars):
        new_params.append(b[-npars + ipar][0])
    nplanets = len(new_params) // 5
    noffsets = len(new_params) % 5
    fit = {}
    err = {}
    ind = 0
    for planet in range(nplanets):
        planetID = "Planet %i" % (planet + 1)
        fit[planetID] = \
            {param: 0.0 for param in DataLabels['Planet']}.copy()
        err[planetID] = \
            {param: 0.0 for param in DataLabels['Planet']}.copy()
        for param in DataLabels['Planet']:
            fit[planetID][param] = new_params[ind]
            err[planetID][param] = 0.01  # m.perror[ind]
            ind += 1
    fit['Offset'] = \
        {param: 0.0 for param in DataLabels['Offset']}.copy()
    err['Offset'] = \
        {param: 0.0 for param in DataLabels['Offset']}.copy()
    for offset in range(noffsets):
        fit['Offset'][DataLabels['Offset'][offset]] = new_params[ind]
        err['Offset'][DataLabels['Offset'][offset]] = 0.01  # m.perror[ind]
        ind += 1

    tel_off = [0.0] + list(fp['offset'])

    return fit, err, nplanets, tel_off


parser = argparse.ArgumentParser()
parser.add_argument("--nperms", type=int, default=100)
parser.add_argument('fname', metavar='fname', type=str, nargs=1,
                    help='Plik z danymi')
args = parser.parse_args()

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    if os.path.isfile(args.fname[0]):
        with open(args.fname[0], 'rb') as pklfile:
            gold = pickle.load(pklfile)

        starname = args.fname[0].split('.')[1]
        fileName, fileExtension = os.path.splitext(str(args.fname[0]))
        data = copy.deepcopy(gold.data)
        Npoints = len(data['T'])
        #data['eRV'] = np.sqrt(data['eRV'] ** 2 + data['jitter'] ** 2)

    if args.nperms > 1:
        perms = [item for item in
                 random_permutations(list(range(Npoints)), args.nperms)]
    else:
        perms = [list(range(Npoints))]
    # dividing data into chunks
    chunks = [[] for _ in range(size)]
    for i, chunk in enumerate(perms):
        chunks[i % size].append(chunk)

    p0 = gold.fit_to_list()
    # [T, P, K, e, w]
    parinfo = [{'value': val, 'fixed': 0, 'limited': [0, 0],
                'limits': [0.0, 0.0]} for val in p0]
    for i in range(len(p0) // 5):
        parinfo[0 + i * 5]['limited'] = [1, 1]  # T
        parinfo[0 + i * 5]['limits'] = [p0[0 + i * 5] - 0.75 * p0[1 + i * 5],
                                        p0[0 + i * 5] + 0.75 * p0[1 + i * 5]]
        parinfo[3 + i * 5]['limited'] = [1, 1]
        parinfo[3 + i * 5]['limits'] = [0.0001, 0.6]  # e
        parinfo[4 + i * 5]['limited'] = [1, 1]
        parinfo[4 + i * 5]['limits'] = [0.0, 360.0]  # w
else:
    permid = None
    perms = None
    data = None
    chunks = None
    p0 = None
    parinfo = None

perms = comm.scatter(chunks, root=0)
data = comm.bcast(data, root=0)
p0 = comm.bcast(p0, root=0)
parinfo = comm.bcast(parinfo, root=0)

fits = []
tempdir = tempfile.mkdtemp()
tname = os.path.join(tempdir, 'proc%3.3i.h5' % (rank))
h5f = h5.File(tname, 'w')
for iperm, perm in enumerate(perms):
    scramble = copy.deepcopy(data)
    scramble['RV'] = keplerian_fit(data['T'], p0) \
        - data['res'][perm] + data['off']

    if any(data['tel'][perm] > 0):
        fit, err, nplanets, tel_off = new_mpfit(scramble, p0, parinfo)
    else:
        fit, err, nplanets, tel_off = old_mpfit(scramble, p0, parinfo)

    for planet in range(nplanets):
        planetID = "Planet %i" % (planet + 1)
        fit[planetID]['w'] = fit[planetID]['w'] % (360.0)
        # empirycznie :)
        #fit[planetID]['T0'] = (fit[planetID]['T0'] % fit[planetID]['P']) \
        #    + fit[planetID]['P'] * int(data['T'][0] / fit[planetID]['P'] + 1)

    current_fit = Fit(fit, err, scramble, keplerian_fit, offset=tel_off)
    dset = '/%3.3i/perm%5.5i' % (rank, iperm)
    h5f[dset + '/fit'] = np.array(current_fit.fit_to_list())
    h5f[dset + '/fit'][-1] = current_fit.offset[-1]
    h5f[dset + '/fit'].attrs['chi2'] = current_fit.chi2
    h5f[dset + '/perm'] = perm

    fits.append(current_fit.chi2)
h5f.close()
shutil.move(tname, os.getcwd())
fits = comm.gather(fits, root=0)

if rank == 0:
    h5f = h5.File('%s.h5' % starname, 'w')
    h5f['/gold'] = np.array(gold.fit_to_list())
    h5f['/gold'][-1] = gold.offset[-1]
    h5f['/gold'].attrs['chi2'] = gold.chi2
    for fname in glob.glob('proc*.h5'):
        old = h5.File(fname, 'r')
        for group in list(old.keys()):
            old.copy(group, h5f)
        old.close()
    h5f.close()
