#!/usr/bin/python

import numpy as np
from mpfit import *
import scipy.constants as sconst
from rvlin import rvlin, rv_drive

orig = False
au = sconst.astronomical_unit
days = np.float64(24.0*3600.0)
dpi = 2.0*sconst.pi

# p = [T0 P K e omega v0]
def one_planet(t,p):
    # No fscking clue what's that... Using normal asini for now...
    a1sini = p[2]*(p[1]*86400.0*np.sqrt(1.0-p[3]**2)) / dpi
    omega = np.radians(p[4] % 360.0)

    # Compute time elapsed since the periastron passage (days->s)
    timediff = (np.abs(t)-p[0])*days
    # Calculate mean anomaly (rad) at the given epoch
    mu =  dpi/(p[1]*days)
    M = (mu * timediff) % dpi
    phase = (M/dpi) % 1.0
    phase[np.where(phase < 1.0)] += 1.0
    phase *= dpi

    # Compute eccentric anomaly by means of Wallace's method
    En = phase + p[3]*np.sin(phase)*(1.0+p[3]*np.cos(phase))
    denom = 1.0 - p[3]*np.cos(En)
    ind = [[0]]
    foo = 0
    go = True
    while (len(ind[0]) > 0) & go:
        E1 = (phase - (En -  p[3]*np.sin(En)))/denom
        ind = np.where(np.abs(E1) > 1.e-12)
        En[ind] += E1[ind]
        foo += 1
        if (foo > 30): go = False

    # Calculate true anomaly
    tantheta = np.sqrt( (1.0+p[3])/(1.0-p[3]) )* np.tan(En*0.5)
    theta = 2.0*np.arctan(tantheta)
    ind = np.where((t < 0.0) & (theta < 0))
    theta[ind] += dpi

    # Compute radial velocity
    c1 = mu*a1sini/np.sqrt(1.0 - p[3]**2)
    c2 = p[3]*np.cos(omega) + np.cos(theta + omega)

    # Return with additional long-term RV trend
    return c1*c2


def keplerian_fit(t, p):
    norbits = len(p) // 5
    off_ord = len(p) % 5

    vr = np.zeros(t.shape)
    for n in range(0, norbits):
        vr += one_planet(t, p[n*5:(n+1)*5])
    for n in range(off_ord):
        vr += p[norbits*5 + n] * t**n
    # if (off_ord == 1): vr += p[-1]
    # if (off_ord == 2): vr += p[-1]*t + p[-2]
    # if (off_ord == 3): vr += p[-1]*t*t + p[-2]*t + p[-3]
    return vr


def myfunc(p, fjac=None, x=None, y=None, err=None):
    # Parameter values are passed in "p"
    # If fjac==None then partial derivatives should not be
    # computed.  It will always be None if MPFIT is called with default
    # flag.
    model = keplerian_fit(x, p[:-1])
    oc = np.log(np.sqrt(2.0 * np.pi * (err + p[-1]) ** 2))
    oc += 0.5 * ((y - model) / (err + p[-1]))**2
    oc = np.sqrt(oc)

    # oc = (y - model) / err
    # Non-negative status value means MPFIT should continue, negative means
    # stop the calculation.
    status = 0
    return [status, oc]


def blah(p, fjac=None, x=None, y=None, err=None, tel=None):
    data = {'T': x, 'RV': y, 'eRV': err, 'tel': tel}
    model, pderiv = rvlin(data, p[:-1])
    foo = np.log(np.sqrt(2*np.pi*(err + p[-1])**2)) +\
           0.5 * ((y - model)/(err + p[-1]))**2
    foo = np.sqrt(foo)
    old_foo = (y - model) / err
    status = 0
    if (fjac is not None):
        return [status, foo, pderiv]
    else:
        return [status, foo]


def get_long_pars(data, shortp, shuffle=False, trend=False):
    fp = rvlin(data, shortp, nompfit=True, trend=trend)
    nplt = len(shortp) // 3
    a = fp['pars']
    b = np.reshape(a, (7, nplt))
    
    # npars = (len(a) % 5) // nplt
    p1 = []
    for iplt in range(nplt):
        if shuffle:
            p1 += [b[1][iplt], b[0][iplt], b[4][iplt], b[2][iplt],
                   b[3][iplt]]
        else:
            p1 += [b[0][iplt], b[1][iplt], b[2][iplt], b[3][iplt],
                   b[4][iplt]]
    p1.append(b[-2][0])
    if trend:
        p1.append(b[-1][0])
    #for ipar in range(npars):
    #    p1.append(b[-npars + ipar][0])
    return p1


if __name__ == "__main__":
    import matplotlib.pylab as plt

    # p = [T0 P K e omega v0]
    tyc_3227_00413 = np.array([53590.880480060565, 148.34872471161077, 50.198896519239369,
                               0.39337734417719628, 276.52812600963546, -5.5701616381261996,
                               23.0])
    tyc_3227_00413 = np.array(
        [53586.72006540584, 148.56823130540261, 51.401559606475772, 0.39698629802992763,
         273.75335311052544, -5.632389894752043, 16.515794018617854]
    )
    fdata = '/home/xarth/docs/an_2013/data/rvc-bis.TYC_3227_00413.47.dst159221'

    tyc_3011_00791 = np.array([53190.533260772499, 483.10357202205455, 113.76873971506124,
                              0.10547958008536319, 36.345683422791687, -9.2325129339121226,
                              51.0])
    # fdata = '/home/xarth/docs/an_2013/data/rvc-bis.TYC_3011_00791.41.dst149604'
    tyc_3318_01515 = np.array([55479.706286347915, 2587.7481085969625, 19.763589732266205,
                              0.37470203409416625, 172.8537282240784, 1.349153202944384,
                              11.6])
    # fdata = '/home/xarth/docs/an_2013/data/rvc-bis.TYC_3318_01515.42.dst530577'

    hd131496 = np.array([16026.2181819, 881.536461149, 35.014940162,
                         0.155439453394, 29.0805889191, 11.0517877134,
                         5.56970264093])
    p0 = [880.65031199090538, 16020.918611635727, 0.15499541861457081, 5.572772]

    ptps1195 = np.array([908.0, 4885.0, 0.28, 0.0])
    fdata = '/home/xarth/aniedzi/PTPS_1195/good.PTPS_1195.18.dat'

    p0 = ptps1195

    rvlin_dt = np.dtype({'names': ['t', 'v', 'e', 'tel'],
                         'formats': [np.float, np.float, np.float, 'S1']})
    data = np.loadtxt(str(fdata), dtype=rvlin_dt)
    print(data)
    telarr = np.zeros(data['tel'].shape, dtype=np.int)
    for itel, stel in enumerate(np.unique(data['tel'])):
        telarr[np.where(data['tel'] == stel)] = itel



    pars = {
        'P1': np.array([p0[0]]), 'tp1': np.array([p0[1]]),
        'e1': np.array([p0[2]]),'sigma': np.array([p0[3]])
    }
         #  27.071157768066428, 34.675232187591838, 10.746894928897772, 0.0]
    isort = np.argsort(data['t'][:])
    epoch = 0.0
    ddata = {
        'T': data['t'][isort] - epoch, 'RV': data['v'][isort],
        'eRV': data['e'][isort], 'tel': telarr[isort]
    }
    foo = np.zeros((data['t'].shape[0], 3))
    foo[:, 0] = np.array(data['t'][isort])
    foo[:, 1] = np.array(data['v'][isort])
    foo[:, 2] = np.array(data['e'][isort])
    data = foo

    parinfo=[]
    if orig:
        for i in range(len(p0)):
            parinfo.append({'value':0., 'fixed':0, 'limited':[0,0], 'limits':[0.,0.]})
        for i in range(len(p0)):
            parinfo[i]['value']=p0[i]
    else:
        for ikey, key in enumerate(pars.keys()):
            parinfo.append(
                {'value': 0., 'fixed': 0, 'limited': [0, 0], 'limits': [0., 0.]})
            parinfo[ikey]['value'] = pars[key]

    if orig:
        # T0 P K e omega v0 jitter
        # 0  1 2 3   4    5    6
        parinfo[3]['limited'] = [1, 1]
        parinfo[3]['limits'] = [0.0, 0.7]
        fa = {'x':data[:, 0], 'y':data[:, 1], 'err':data[:, 2]}
        m = mpfit(myfunc, p0, parinfo=parinfo, functkw=fa, quiet=True)
        pars1 = m.params
        p1 = np.array(pars1)
    else:
        # P T e jitter
        # 0 1 2  3
        # P t e o K
        parinfo[2]['limited'] = [1, 1]
        parinfo[2]['limits'] = [0.0, 0.7]
        fa = {'x': ddata['T'], 'y': ddata['RV'], 'err': ddata['eRV'],
              'tel': ddata['tel']}
        m = mpfit(blah, p0, parinfo=parinfo, quiet=True, functkw=fa)

        p1 = get_long_pars(ddata, m.params[:-1])
        p1.append(m.params[-1])
        print(("jitter = %f" % p1[-1]))
        p1 = np.array(p1)


    x = np.linspace(data[:, 0].min(), data[:, 0].max(), 1000)

    p00 = get_long_pars(ddata, p0[:-1])
    p00.append(p0[-1])
    p00 = np.array(p00)

    for i in range(p00.shape[0]):
        print((p00[i], p1[i]))

    eRV_0 = np.sqrt(data[:, 2] ** 2 + p00[-1] ** 2)
    eRV_1 = np.sqrt(data[:, 2] ** 2 + p1[-1] ** 2)

    if orig:
        chi2_p0 = np.sum(
            (data[:, 1] - keplerian_fit(data[:, 0], p0[:-1])) ** 2 /
            eRV_0 ** 2) / (data.shape[0] - p1.shape[0])

        chi2_p1 = np.sum(
            (data[:, 1] - keplerian_fit(data[:, 0], p1[:-1])) ** 2 /
            eRV_1 ** 2) / (data.shape[0] - p1.shape[0])

    else:
        nu = len(p0)
        chi2_p0 = np.sum(
            (data[:, 1] - rv_drive(data[:, 0], p00[:-1])) ** 2 /
            eRV_0 ** 2) / (data.shape[0] - nu)
        chi2_p1 = np.sum(
            (data[:, 1] - rv_drive(data[:, 0], p1[:-1])) ** 2 /
            eRV_1 ** 2) / (data.shape[0] - nu)

    print((np.sqrt(chi2_p0), np.sqrt(chi2_p1)))
    print(p1)
    exit()

    plt.figure()
    plt.errorbar(data[:, 0], data[:, 1],
                 yerr=np.sqrt(data[:, 2] ** 2 + p0[-1] ** 2), fmt='ro')
    if orig:
        plt.plot(x, keplerian_fit(x, p0[:-1]), label="best")
        plt.plot(x, keplerian_fit(x, p1), label="jitter")
    else:
        plt.plot(x, rv_drive(x, p00[:-1]), label="best")
        plt.plot(x, rv_drive(x, p1[:-1]), label="jitter")
    plt.legend()
    plt.show()
