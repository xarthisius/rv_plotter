#!/usr/bin/python

import matplotlib.pylab as plt
import numpy as np
from mpfit import *
import scipy.constants as sconst

au = sconst.astronomical_unit
days = np.float64(24.0*3600.0)
dpi = 2.0*sconst.pi

# p = [T0 P K e omega v0]
def one_planet(t,p):
    # No fscking clue what's that... Using normal asini for now...
    a1sini = p[2]*(p[1]*86400.0*np.sqrt(1.0-p[3]**2)) / dpi
    omega = np.radians(p[4] % 360.0)

    # Compute time elapsed since the periastron passage (days->s)
    timediff = (np.abs(t)-p[0])*days
    # Calculate mean anomaly (rad) at the given epoch
    mu =  dpi/(p[1]*days)
    M = (mu * timediff) % dpi
    phase = (M/dpi) % 1.0
    phase[np.where(phase < 1.0)] += 1.0
    phase *= dpi

    # Compute eccentric anomaly by means of Wallace's method
    En = phase + p[3]*np.sin(phase)*(1.0+p[3]*np.cos(phase))
    denom = 1.0 - p[3]*np.cos(En)
    ind = [[0]]
    foo = 0
    go = True
    while (len(ind[0]) > 0) & go:
        E1 = (phase - (En -  p[3]*np.sin(En)))/denom
        ind = np.where(np.abs(E1) > 1.e-12)
        En[ind] += E1[ind]
        foo += 1
        if (foo > 30): go = False

    # Calculate true anomaly
    tantheta = np.sqrt( (1.0+p[3])/(1.0-p[3]) )* np.tan(En*0.5)
    theta = 2.0*np.arctan(tantheta)
    ind = np.where((t < 0.0) & (theta < 0))
    theta[ind] += dpi

    # Compute radial velocity
    c1 = mu*a1sini/np.sqrt(1.0 - p[3]**2)
    c2 = p[3]*np.cos(omega) + np.cos(theta + omega)

    # Return with additional long-term RV trend
    return c1*c2

def keplerian_fit(t, p):
    norbits = len(p) // 5
    off_ord = len(p) % 5

    vr = np.zeros(t.shape)
    for n in range(0, norbits):
        vr += one_planet(t, p[n*5:(n+1)*5])
    for n in range(off_ord):
        vr += p[norbits*5 + n] * t**n
#    if (off_ord == 1): vr += p[-1]
#    if (off_ord == 2): vr += p[-1]*t + p[-2]
#    if (off_ord == 3): vr += p[-1]*t*t + p[-2]*t + p[-3]
    return vr


def myfunc(p, fjac=None, x=None, y=None, err=None):
    # Parameter values are passed in "p"
    # If fjac==None then partial derivatives should not be
    # computed.  It will always be None if MPFIT is called with default
    # flag.
    model = keplerian_fit(x, p)

    # Non-negative status value means MPFIT should continue, negative means
    # stop the calculation.
    status = 0
    return [status, (y-model)/err]


if __name__ == "__main__":
   gold = np.array([ 53316.79648024, 604.54973695, 40.0, 0.32528757, 61.27030755, 0.674])
#p = [T0 P e omega K v0]

   p0 = [5.40862000e+04, 4.06457001e+02, 3.39370301e+01, 1.91999995e-04, 2.53259996e+01,
         5.23413999e+04, 5.72193992e+03, 1.99541251e+02, 1.56183994e-01, 1.31011201e+02,
         -2.98539996e+01]

   fdata = 'rvc-bis.TYC_3304_00323.58.dst530308'
   data = {'T': [], 'RV': [], 'eRV': []}

   fh = open(fdata,'r')
   for line in fh.readlines():
       f = line.split()
       data['T'].append(float(f[0]))
       data['RV'].append(float(f[1]))
       data['eRV'].append(float(f[2]))
   for key in list(data.keys()):
       data[key] = np.array(data[key])
   fh.close()

   parinfo=[]
   for i in range(len(p0)):
      parinfo.append({'value':0., 'fixed':0, 'limited':[0,0], 'limits':[0.,0.]})
   for i in range(len(p0)):
      parinfo[i]['value']=p0[i]
# T0 P K e omega v0
   parinfo[1]['limited']= [1,1]
   parinfo[1]['limits']=  [380.,410.]
   parinfo[2]['limited']= [1,1]
   parinfo[2]['limits']=  [20.,40.]
   parinfo[3]['limited']= [1,1]
   parinfo[3]['limits']=  [0.0,0.3]
   parinfo[4]['limited']= [1,1]
   parinfo[4]['limits']=  [0.0,2.*np.pi]

   fa = {'x':data['T'], 'y':data['RV'], 'err':data['eRV']}

   m = mpfit(myfunc, p0, parinfo=parinfo, functkw=fa)

   pars1 = m.params
   pars1[4] = pars1[4] % dpi
#   pars1[9] = pars1[9] % dpi
   pars1[0] = (pars1[0] % pars1[1])  + pars1[1] * int(data['T'][0] / pars1[1] + 1)   # empiriczne :)
   pars1[5] = (pars1[5] % pars1[6])  + pars1[6] * int(data['T'][0] / pars1[6] + 1)   # empiriczne :)


   x = np.linspace(data['T'].min(),data['T'].max(),1000)
#
   print(pars1)
   print(p0)
   plt.figure()
   plt.errorbar(data['T'], data['RV'], yerr=3.0*data['eRV'], fmt='ro')
   plt.plot(x,keplerian_fit(x,p0))
   plt.plot(x,keplerian_fit(x,pars1))
   plt.show()
