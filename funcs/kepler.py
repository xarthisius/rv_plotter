#!/usr/bin/python
import math
import numpy as np

def kepler(M, e):

    if len(M) == 1 and len(e) > 1:
        Marr = np.repeat(M, len(e))
    else:
        Marr = M
    if len(e) == 1 and len(M) > 1:
        eccarr = np.repeat(e, len(M))
    else:
        eccarr = e

    conv = 1e-12
    k = 0.85
    mphase = (0.5 * Marr / np.pi)
    mphase[np.where(mphase < 0.0)] += 1.0
    # sign(sin(Marr)), but faster
    ssm = (mphase <= 0.5) - ((mphase >= 0.5) | (mphase == 0))*1
    Earr = Marr + ssm * k * eccarr
    fiarr = (Earr - eccarr * np.sin(Earr) - Marr)
    convd = np.where(abs(fiarr) > conv)
    count = 0
    while np.any(convd) | (count == 100):
        count += 1
        M = Marr[convd]
        ecc = eccarr[convd]
        E = Earr[convd]

        fi = fiarr[convd]
        fip = 1.0 - ecc * np.cos(E)
        fipp = ecc * np.sin(E)
        fippp = 1.0 - fip

        d1 = -fi / fip
        d2 = -fi / (fip + 0.5 * d1 * fipp)
        d3 = -fi / (fip + 0.5 * d2 * fipp + d2**2 * fippp / 6.0)

        E += d3
        Earr[convd] = E
        fiarr = (Earr - eccarr * np.sin(Earr) - Marr)
        convd = np.where(abs(fiarr) > conv)

    if len(Marr) == 1:
        return Earr[0]
    else:
        return Earr


"""Calculates the true anomaly from the time and orbital elements.

Args:
    P (float):      period
    tp (float):     time of periastron passage
    e (float):      eccentricity
    t (np.array):   time

Returns:
    float.  True anomaly

In Wright & Howard, nu is denoted by little 'f'.
Here we must distinguish from big F, a matrix, so we use nu
"""
def calcnu(P, tp, e, t):
    phase = (t - tp) / P
    M = 2.0 * np.pi * (phase - np.floor(phase))
    E1 = kepler(np.array(M), np.array([e]))

    n1 = 1.0 + e
    n2 = 1.0 - e
    return 2.0 * np.arctan(math.sqrt(n1 / n2) * np.tan(0.5 * E1))
