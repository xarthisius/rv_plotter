#!/usr/bin/python
import numpy as np

def parse_file(fname):
    rvlin_dt = np.dtype({'names': ['t', 'v', 'e', 'tel'],
                        'formats': [np.float, np.float, np.float, 'S1']})
    data = np.loadtxt(fname, dtype=rvlin_dt)

    telarr = np.zeros(data['tel'].shape, dtype=np.int)
    for itel, stel in enumerate(np.unique(data['tel'])):
        telarr[np.where(data['tel'] == stel)] = itel

    return data['t'], data['v'], data['e'], telarr

    t, v, e, tel = parse_file(fname)
    print(tel)
