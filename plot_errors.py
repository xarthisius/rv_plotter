#!/usr/bin/python

import h5py as h5
import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib


def stderr_round(x, digit=0):
    if x == 0.0:
        return 0.0, 0.0
    if np.log10(x) <= 0:
        log = -int(np.log10(x)) + 1
    else:
        log = -int(np.log10(x))
    val = x * 10 ** log
    if (np.ceil(val) - val) > 0.1 * val:
        return np.ceil(val * 10.0) * 10.0 ** -(log + 1), -(log + 1)
    else:
        return np.ceil(val) * 10.0 ** -log, -log


def pretty_pm_format(mean, lower, upper):
    err1, log1 = stderr_round(abs(lower))
    err2, log2 = stderr_round(upper)
    return "$%g^{+%g}_{-%g}$" % (round(mean / 10.0 ** log1) * 10.0 ** log1,
                                  err2, err1)


matplotlib.rc('font', family='serif')
matplotlib.rc('text', usetex=True)
matplotlib.rc("axes", linewidth=1.5)
matplotlib.rc("lines", linewidth=1.5)
#matplotlib.rc("font", size=16)

data = []
h5f = h5.File(sys.argv[1], 'r')
for grp in list(h5f.keys()):
    if grp == 'gold':
        continue
    for perm in list(h5f[grp].keys()):
        data.append(h5f[grp][perm]['fit'][:])
best_fit = h5f['gold'][:]
temp = best_fit[:].copy()
best_fit[0:5] = best_fit[5:10]
best_fit[5:10] = temp[0:5]
del temp
best_chi2 = h5f['gold'].attrs['chi2']
h5f.close()

lbl = [r'$T_0$ [MJD]', r'$P$ [days]', r'$K$ [m/s]', r'$e$', r'$\omega$ [deg]']

data = np.array(data)
for i in range(data.shape[-1]):
    print(data[:, i].min(), data[:, i].max())

for i in range(len(best_fit) // 5):
    fig, axs = plt.subplots(nrows=2, ncols=3, figsize=(8,8))
    plt.subplots_adjust(hspace=0.28)

    for j in range(5):
        ax = axs.ravel()[j]
        ind = j + i * 5
        if j == 4:
            data[:, ind] = (data[:, ind] - best_fit[ind] + 180.0) % 360.0 \
                - 180.0 + best_fit[ind]
        n, bins, patches = ax.hist(data[:, ind], 50, normed=0,
                                   facecolor='green', alpha=0.75)
        ax.axvline(best_fit[ind], lw=2, c='k', ls='--')
        ax.set_xlabel(lbl[j])
        ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(4))
        bounds = np.percentile(data[:, ind], (15.87, 84.13)) - best_fit[ind]
        ax.set_title(pretty_pm_format(best_fit[ind], bounds[0], bounds[1]))
        if (j % 3 == 0):
            ax.set_ylabel("Number of fits")
        print((np.percentile(data[:, ind], (15.87, 84.13)) - best_fit[ind],
              best_fit[ind], np.median(data[:, ind])))
    fig.savefig('fap_%s_pln%i.pdf' % (sys.argv[1][:-3], i + 1), bbox='tight')

lbl = [r'$v_0$ [m/s]', r'offset [m/s]']
fig, axs = plt.subplots(nrows=1, ncols=2)
for i, ind in enumerate([-3, -1]):
    ax = axs[i]
    n, bins, patches = ax.hist(data[:, ind], 50, normed=0, facecolor='green',
                               alpha=0.75)
    ax.axvline(best_fit[ind], lw=2, c='k', ls='--')
    ax.set_xlabel(lbl[i])
    ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(4))
    bounds = np.percentile(data[:, ind], (15.87, 84.13)) - best_fit[ind]
    ax.set_title(pretty_pm_format(best_fit[ind], bounds[0], bounds[1]))

fig.savefig('fap_%s_off.pdf' % (sys.argv[1][:-3]), bbox='tight')
