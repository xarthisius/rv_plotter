#!/usr/bin/python
import numpy as np
import random
from pypikaia import pypikaia
import matplotlib.pylab as plt
from my_fit import keplerian_fit

days = np.float64(24.0*3600.0)
dpi = 2.0*np.pi

def load_file(fdata):
   data = {'T': [], 'RV': [], 'eRV': []}

   fh = open(fdata,'r')
   for line in fh.readlines():
       f = line.split()
       data['T'].append(float(f[0]))
       data['RV'].append(float(f[1]))
       data['eRV'].append(float(f[2]))
   for key in data.keys():
       data[key] = np.array(data[key])
   fh.close()
   return data

ctrl = -np.ones((12))
mina = np.array([ 50000.0, 10.0,      1.0,  0.0,   0.0]*2 + [-100.0, 0.0, 0.0])
maxa = np.array([ 55000.0, 10000.0, 1000.0, 0.8, 360.0]*2 + [100.0, 0.0, 0.0])
data = load_file('rvc-bis.TYC_3304_00323.58.dst530308')

mina[1] = 350.
maxa[1] = 450.
mina[6] = 1000.

seed = random.randint(1, 999999)

data['eRV'] = np.sqrt(data['eRV'] + 10.**2)

x, f, stat = pypikaia(data['T'], data['RV'], data['eRV'],
                      np.zeros_like(data['T']),
                      seed, ctrl, mina, maxa, 10, 0)

print x, f, stat

xx = np.linspace(min(data['T']), max(data['T']),100)
yy = keplerian_fit(xx, x)

plt.errorbar(data['T'], data['RV'], yerr=data['eRV'], fmt='o')
plt.plot(xx,yy)
plt.show()
