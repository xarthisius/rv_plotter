#!/usr/bin/python

import numpy as np
from mpfit import *
from funcs.kepler import \
    calcnu, kepler
from funcs.fileio import \
    parse_file


def rvlingrad(t, pars, beta, F, vW, WFT, epsilon, requested):
    # BEWARE not tested
    npln = len(pars) // 3
    nt = t.shape[0]

    dp = np.zeros((npln * 3, nt))

    Ftepsilon = np.dot(F.T, epsilon)
    ntrans = 0
    index = 0
    t0 = 0.0

    for j in range(npln):
        if (requested[j * 3:j * 3 + 2]).sum() > 0:

            P = pars[j * 3]
            tp = pars[j * 3 + 1] + t0
            e = pars[j * 3 + 2]

            phase = (t - tp) / P
            M = 2.0 * np.pi * (phase - np.floor(phase))
            E1 = kepler(np.array(M), np.array([e]))
            sE1 = np.sin(E1)
            cE1 = np.cos(E1)
            onemecosE1 = 1.0 - e * cE1
            s1e1e = np.sqrt((1.0 + e) / (1.0 - e))

            nu = 2.0 * np.arctan(s1e1e * np.tan(0.5 * E1))
            sn = np.sin(nu)
            cn = np.cos(nu)

            dnudE1 = s1e1e * (cn + 1.0) / (cE1 + 1.0)

        for x in range(3):
            if requested[j * 3 + x]:

                dE1dpar = -2.0 * np.pi / P / onemecosE1
                if x == 0:
                    dE1dpar *= phase
                if x == 2:
                    dE1dpar = sE1 / onemecosE1
                    dnudpar = dnudE1 * (dE1dpar + sE1 / (1.0 - e ** 2))
                else:
                    dnudpar = dnudE1 * dE1dpar

                dFdpar = np.zeros_like(F)

                dFdpar[index, :] = -sn * dnudpar
                dFdpar[index + 1, :] = cn * dnudpar

                N = np.dot(dFdpar, WFT)
                dadpar = np.dot(np.dot(
                    vW, (dFdpar.T - np.dot(Ftepsilon, N + N.T))), epsilon)

                dvdpar = np.dot(beta, dFdpar) + np.dot(dadpar, F)
                if x == 2:
                    dvdpar /= 1000.0  # ????
                dp[j * 3 + x, :] = dvdpar
        index += 1

    return dp


def rvlin(data, pars, dp=None, t0=None, trend=False, nompfit=False):
    npln = len(pars) // 3
    ntrans = 0  # For now, we don't do transits
    if t0 is None:
        t0 = 0.0 #14000.0

    P = pars[0::3]
    tp = pars[1::3]
    e = pars[2::3]

    #data['T'] += t0
    #pars['e'] *= 0.001
    ntels = data['tel'].max() + 1

    for j in range(npln):
        nu = calcnu(P[j], tp[j], e[j], data['T'])
        if j == 0:
            F = np.vstack((np.cos(nu), np.sin(nu)))
        else:
            F = np.vstack((F, np.cos(nu), np.sin(nu)))

    F = np.vstack((F, np.ones(data['T'].shape)))
    if trend:
        F = np.vstack((F, data['T'] - t0))
    for i in range(1, ntels):
        offsetarr = np.zeros_like(data['tel'])
        offsetarr[np.where(data['tel'] == i)] = 1.0
        F = np.vstack((F, offsetarr))
        del offsetarr
    nt = F.shape[1]
    # Definition of the model u = np.dot(beta, F)
    # calculate diagonal matrix of weights
    W = np.diagflat(1.0 / data['eRV'] ** 2)
    # precompute for clarity and speed below
    vW = data['RV'] / data['eRV'] ** 2
    WFT = np.dot(W, F.T)

    # This is the "curvature matrix" or "variance covariance matrix"
    epsilon = np.linalg.inv(np.dot(F, WFT))
    # solution to linear least-squares problem, a in Bevington
    beta = np.dot(np.dot(vW, F.T), epsilon)
    u = np.dot(beta, F)  # definition of the model

    K = np.zeros((npln))
    omega = np.zeros((npln))
    v0_index = npln * 2 - ntrans
    index = 0
    for i in range(npln):
        K[i] = np.sqrt(beta[0 + index] ** 2 + beta[1 + index] ** 2)
        omega[i] = (np.rad2deg(
            np.arctan2(-beta[1 + index], beta[0 + index])) + 360.0) % 360.0
        index += 2

    if ntels > 1:
        offset = beta[v0_index + (trend) + 1:]
    else:
        offset = 0.0
    # This looks fishy from python's POV
    gamma = beta[v0_index] - (K * e * np.cos(np.deg2rad(omega))).sum()

    parset = np.hstack(
        (P, tp, e, omega, K, gamma * np.ones((npln)), np.zeros((npln)))
    )
    if trend:
        parset[-1] = beta[v0_index + 1]

    requested = np.ones((3 * npln))  # It's initialized by f***ing magic in IDL
    if requested.sum() > 0:
        dp = rvlingrad(data['T'], pars, beta, F, vW, WFT, epsilon, requested).T
#        dp /= np.tile(data['eRV'], (3, 1)).T
    if nompfit:
        return {'pars': parset, 'offset': offset}
    else:
        return u, dp
#   return (v-u) / data['eRV']
def myfunc(p, fjac=None, x=None, y=None, err=None, tel=None):
    data = {'T': x, 'RV': y, 'eRV': err, 'tel': tel}
    model, pderiv = rvlin(data, p[:-1], trend=False)
    foo = np.log(np.sqrt(2*np.pi*(err + p[-1])**2)) +\
           0.5 * ((y - model)/(err + p[-1]))**2  # chi^2
    foo = np.sqrt(foo)  # chi

    old_foo = (y - model) / err  # chi
    status = 0
    if (fjac is not None):
        return [status, foo, pderiv]
    else:
        return [status, foo]


def myfunc_old(p, fjac=None, x=None, y=None, err=None, tel=None):
    data = {'T': x, 'RV': y, 'eRV': err, 'tel': tel}
    model, pderiv = rvlin(data, p)
    status = 0
    if (fjac is not None):
        return [status, (y - model) / err, pderiv]
    else:
        return [status, (y - model) / err]

def rv_drive(t, orbel):
    rv = np.zeros_like(t)
    n = len(orbel) // 5

    gamma = orbel[-2]
    dvdt = orbel[-1]
    for i in range(n):
        p = orbel[0 + i * 5]
        tp = orbel[1 + i * 5]
        e = orbel[2 + i * 5]
        om = np.deg2rad(orbel[3 + i * 5])
        k = orbel[4 + i * 5]
        curv = 0
#        if i == 0 and len(orbel) // 7 * 7 == len(orbel) - 1:
#            curv = orbel[-1]

        p = max(0.01, p)
        k = max(0.01, k)
        e = min(max(0, e), 0.99)

        nu = calcnu(p, tp, e, t)
        rv += k * (np.cos(nu + om) + e * np.cos(om))
    rv += gamma + dvdt * (t) + curv * (t) ** 2
    return rv

if __name__ == "__main__":
    import matplotlib.pylab as plt
    pars = {'P': np.array([200.72745]), 'tp': np.array([2452219.0]),
            'e': np.array([0.3])}
    p0 = [259.72745, 2452219.0, 0.3]
    t, v, e, tel = parse_file('funcs/12661.vel.txt')
    isort = np.argsort(t)
    epoch = 0.0
    data = {'T': t[isort] - epoch, 'RV': v[isort], 'eRV': e[isort],
            'tel': tel[isort]}

    for i in range(109):
        print((data['T'][i], data['RV'][i], data['eRV'][i], data['tel'][i]))
    #rvlin(pars, data, t0=epoch)

    parinfo = []
    for ikey, key in enumerate(pars.keys()):
        parinfo.append(
            {'value': 0., 'fixed': 0, 'limited': [0, 0], 'limits': [0., 0.]})
        parinfo[ikey]['value'] = pars[key]
    parinfo[2]['limited'] = [1, 1]
    parinfo[2]['limits'] = [0.0, 0.9]
    fa = {'x': data['T'], 'y': data['RV'], 'eRV': data['eRV'], 'tel':
          data['tel']}

    #m = mpfit(myfunc, p0, parinfo=parinfo, functkw=fa, autoderivative=1)

    #gold = [261.0410, 2452222, 0.0, 160.9320, 71.43109, 7.317909, 0.0]
    #off_gold = 3.9099181

    #vall = data['RV']
    #final_pars = rvlin(data, m.params, nompfit=True)
    #for itel in range(1, data['tel'].max() + 1):
    #    vall[np.where(data['tel'] == itel)] -= final_pars['offset'][itel - 1]

#   for itel in range(1, data['tel'].max() + 1):
#       vall[np.where(data['tel'] == itel)] -= off_gold

    #dev = vall - rv_drive(data['T'], final_pars['pars'])
    #chi2 = ((dev / data['eRV']) ** 2).sum()
    #chi = np.sqrt(chi2 / (len(data['T']) - 7))
    #rms = np.sqrt((dev ** 2).sum() / (len(data['T']) - 7))
#
#    print "Reduced chi^2 = %f" % (chi ** 2)
#    print "RMS = %f" % rms
    x = np.linspace(data['T'].min(), data['T'].max(), 1000)
#
    plt.figure()
#    offset = [0, final_pars['offset']]
#    colors = ['ro', 'go', 'bo', 'co']
#    for itel in range(data['tel'].max() + 1):
#        ind = np.where(data['tel'] == itel)
#        plt.errorbar(data['T'][ind], data['RV'][ind] - offset[itel],
#                     yerr=3.0 * data['eRV'][ind], fmt='o')
    f = [198.48598579620685, 53329.216958483026, 0.11352122328956968, 220.71614006610824, 88.97759037479122, \
         560.61446019033508, 53749.464798832712, 0.051887450150071956, 128.71624794883303, 232.24717199058824,
         -9.4620816536862513, 0.0]
    #plt.plot(x, rv_drive(x, final_pars['pars']))
    plt.plot(x, rv_drive(x, f))
    plt.show()
