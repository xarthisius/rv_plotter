module func
   
   implicit none

contains

   function userff(n,x,flag) result(fn_val)
      use commons, only: maxaga, minaga, sigma, t, rv

      implicit none
      integer, intent(in)  :: n
      real(kind=8), intent(in), dimension(:) :: x
      integer, intent(in) :: flag

      real(kind=4) :: fn_val

      !     Local
      real(kind=8), dimension(n) :: a
      real(kind=8) :: Chi2, RedChi2
      integer :: i

      a(:) = real(x(:)*(maxaga(:)-minaga(:))+minaga(:) , kind=8)
      
      Chi2 = 0.0
      do i=lbound(rv,1),ubound(rv,1)
         Chi2 = Chi2 + ( (rv(i)-yfun_orbits_new(t(i),a))/sigma(i) )**2
      enddo
      RedChi2=Chi2/(dble(size(rv)-n))
      fn_val = 1.0 / real(RedChi2)
!      fn_val = real( real(ndata-na,kind=8) / Chi2 , kind=4)
   end function userff
!-----------------------------------------------------------------------
!     Routine to compute RV component of a star in a multiple system.
!     Alex Wolszczan, June 1988, mods: Feb 2005, March 2008.
!-----------------------------------------------------------------------
      function yfun_orbits_new(obsepoch,a) result(rv)
!-----------------------------------------------------------------------
!     The I/O parameters (first orbit) are:
!     * obsepoch    -  MJD of observation (days)             (I)
!  a1 * T0          -  Epoch of periastron (MJD)             (I)
!  a2 * Pb          -  Orbital period (days)                 (I)
!  a3 * a1sini      -  Projected semi-major axis (m)         (I)
!  a4 * e           -  Eccentricity of the orbit             (I)
!  a5 * omega       -  Longitude of periastron (deg)         (I)
!  a6 * omegadot    -  Periastron advance (deg/yr)           (I)
!  a7 * voffset     -  Constant velocity offset              (I)
!     * yfun_orbits -  Radial velocity (m/s)                 (O)
!     All parameters are declared real*8.
!-----------------------------------------------------------------------
      implicit none
      real(kind=8), intent(in) :: obsepoch
      real(kind=8), intent(in), dimension(:) :: a
      real(kind=8) :: rv
      integer, parameter ::maxp = 6, maxorb = 1
      integer(kind=4), parameter :: norbits = 1
      real(kind=8), parameter :: pi=3.1415926535897932d0
      real(kind=8), parameter :: fact2=pi/180.d0
      real(kind=8), parameter :: twopi=2.d0*pi
      real(kind=8), parameter :: fact1=86400.d0
      real(kind=8), parameter :: c=2.99792456d5
      real(kind=8), parameter :: eps=1.d-12

      real(kind=8) :: T0,Pb,a1sini,e,omega,omegadot,voffset,phase,denom
      real(kind=8) :: omega0,timediff,M,mu,E0,E1,tantheta,&
               theta,c1,c2,yf

      real(kind=8) :: h(maxp), pharray(maxorb),trarray(maxorb)
      real(kind=8) :: mrvorbv(maxorb),mrvorbtrdv(maxorb)
!-----------------------------------------------------------------------
!     Local varaibles.
!-----------------------------------------------------------------------
      integer :: i,j,ii,indx
!-----------------------------------------------------------------------

      T0 = a(1)
      Pb = a(2)
      a1sini=a(3)*(a(2)*86400.d0*dsqrt(1.d0-a(4)**2))/6.283185307d0
      e = a(4)
      omega = a(5)

!-----Compute time elapsed since the periastron passage (days)
      timediff = abs(obsepoch)-T0

!-----Convert longitude of periastron to radians
      omega0 = mod(omega*fact2,twopi)

!-----Now, calculate mean anomaly (rad) at the given epoch
      mu=twopi/(Pb*fact1)
      M=dmod(mu*timediff*fact1,twopi)
      phase=dmod(M/twopi,1.d0)
      if(phase.lt.0.d0) phase=phase+1.d0  !can get it out, if necessary
      phase=twopi*phase

!-----Compute eccentric anomaly by means of Wallace's method
      E0=phase+e*dsin(phase)*(1.d0+e*dcos(phase))
      denom=1.d0-e*dcos(E0)
      E1 = 2.0*eps
      do while( abs(E1) > eps )
         E1=(phase-(E0-e*dsin(E0)))/denom
         E0=E0+E1
      enddo

!-----Calculate true anomaly
      tantheta=dsqrt((1.d0+e)/(1.d0-e))*dtan(E0*0.5)
      theta=2.d0*datan(tantheta)
      if(obsepoch.lt.0.d0) then       !store orbital phase and true anomaly at first
         if(theta.lt.0.d0) theta=theta+twopi
      endif

!-----Compute radial velocity 
      c1 = mu*a1sini/sqrt(1.d0-e*e)
      c2 = e*cos(omega0)+cos(theta+omega0)
!-----Comupute long-term RV trend, including higher-order terms, if non-zero
      rv = c1*c2 + a(6)
      return
   end function yfun_orbits_new
!-----------------------------------------------------------------------
      function yfun_orbits(obsepoch,a) result(rv)
!-----------------------------------------------------------------------
!     The I/O parameters (first orbit) are:
!     * obsepoch    -  MJD of observation (days)             (I)
!  a1 * T0          -  Epoch of periastron (MJD)             (I)
!  a2 * Pb          -  Orbital period (days)                 (I)
!  a3 * a1sini      -  Projected semi-major axis (m)         (I)
!  a4 * e           -  Eccentricity of the orbit             (I)
!  a5 * omega       -  Longitude of periastron (deg)         (I)
!  a6 * omegadot    -  Periastron advance (deg/yr)           (I)
!  a7 * voffset     -  Constant velocity offset              (I)
!     * yfun_orbits -  Radial velocity (m/s)                 (O)
!     All parameters are declared real*8.
!-----------------------------------------------------------------------
      implicit none
      real(kind=8), intent(in) :: obsepoch
      real(kind=8), intent(in), dimension(:) :: a
      real(kind=8) :: rv
      integer, parameter ::maxp = 6, maxorb = 1
      integer(kind=4), parameter :: norbits = 1
      real(kind=8), parameter :: pi=3.1415926535897932d0
      real(kind=8), parameter :: fact2=pi/180.d0
      real(kind=8), parameter :: twopi=2.d0*pi
      real(kind=8), parameter :: fact1=86400.d0
      real(kind=8), parameter :: c=2.99792456d5
      real(kind=8), parameter :: eps=1.d-12

      real(kind=8) :: T0,Pb,a1sini,e,omega,omegadot,voffset,phase,denom
      real(kind=8) :: omega0,timediff,M,mu,E0,E1,En,tantheta,&
               theta,c1,c2,yf

      real(kind=8) :: h(maxp), pharray(maxorb),trarray(maxorb)
      real(kind=8) :: mrvorbv(maxorb),mrvorbtrdv(maxorb)
!-----------------------------------------------------------------------
!     Local varaibles.
!-----------------------------------------------------------------------
      integer :: i,j,ii,indx
!-----------------------------------------------------------------------
      yf=0.d0                            !reset rv accumulator
      indx=5*norbits+1
      voffset=a(indx)                    

      do i=1,norbits
         mrvorbv(i)=0.d0

!omdot        ii=(i-1)*6                       !copy params to local vars
        ii=(i-1)*5                       !copy params to local vars
        T0=a(1+ii)
        Pb=a(2+ii)
!k        a1sini=a(3+ii)
        a1sini=a(3+ii)*(a(2+ii)*86400.d0*dsqrt(1.d0-a(4+ii)*a(4+ii)))/6.283185307d0
        e=a(4+ii)
        omega=a(5+ii)
!omdot        omegadot=a(6+ii)

!-----Compute time elapsed since the periastron passage (days)
        timediff=dabs(obsepoch)-T0

!-----Convert longitude of periastron to radians
!omdot        omega0=(omega+omegadot*timediff/365.d0)*fact2
        omega0=omega*fact2
        omega0=dmod(omega0,twopi)

!-----Now, calculate mean anomaly (rad) at the given epoch
        mu=twopi/(Pb*fact1)
        M=dmod(mu*timediff*fact1,twopi)
        phase=dmod(M/twopi,1.d0)
        if(phase.lt.0.d0) phase=phase+1.d0  !can get it out, if necessary
        phase=twopi*phase
!      write(*,*) mu, M, phase
!      stop

!-----Compute eccentric anomaly by means of Wallace's method
        E0=phase+e*dsin(phase)*(1.d0+e*dcos(phase))
        denom=1.d0-e*dcos(E0)
 10     E1=(phase-(E0-e*dsin(E0)))/denom
        E0=E0+E1
        if(dabs(E1).gt.eps) goto 10
        En=E0

!-----Calculate true anomaly
        tantheta=dsqrt((1.d0+e)/(1.d0-e))*dtan(En/2.d0)
        theta=2.d0*datan(tantheta)
!        write(*,*) En, tantheta, theta
!        stop
        if(obsepoch.lt.0.d0) then       !store orbital phase and true anomaly at first
           pharray(i)=phase/twopi !epoch for each orbit
           if(theta.lt.0.d0) theta=theta+twopi
           trarray(i)=theta/fact2
        endif

!-----Compute radial velocity 
        c1=mu*a1sini/dsqrt(1.d0-e*e)
        c2=e*dcos(omega0)+dcos(theta+omega0)
        yf=yf+c1*c2
        mrvorbv(i)=mrvorbv(i)+c1*c2

      enddo
      
!-----Comupute long-term RV trend, including higher-order terms, if non-zero
      
      rv=yf+voffset
      do i=1,norbits
         mrvorbtrdv(i)=mrvorbv(i)+voffset
      enddo
      return
   end function yfun_orbits
end module func
