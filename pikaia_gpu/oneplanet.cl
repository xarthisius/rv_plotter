
float one_planet(float t, float a[6]) {
   const float fact2 = 0.017453292520;
   const float twopi = 6.283185307179586; 
   const float eps = 1e-12;

   float a1sini = a[2]*(a[1]*sqrt(1.0-a[3]*a[3]))*13750.987083139758;
   float timediff = fabs(t)-a[0];
   float omega0 = fmod(a[4]*fact2, twopi);
   float mu = 7.27220521664e-5/a[1];
   float M = fmod(mu*timediff*86400.0, twopi);
   float phase = fmod(M/twopi,1.0);
   if (phase < 0) phase += 1.0;
   phase *= twopi;

   float E0 = phase + a[3]*sin(phase)*(1.0+a[3]*cos(phase));
   float denom = 1.0 - a[3]*cos(E0);
   float E1 = 2.0*eps;
   int ind=0;
   while (E1 > eps && ind < 31) {
      E1 = (phase-(E0-a[3]*sin(E0)))/denom;
      E0 += E1;
      ind++;
   }
   float theta = 2.0*atan(sqrt((1.0+a[3])/(1.0-a[3]))*tan(E0*0.5));
   if (t < 0.0 && theta < 0.0) theta += twopi;

   return (mu*a1sini / sqrt(1.0-a[3]*a[3]))*(a[3]*cos(omega0)+cos(theta+omega0)) + a[5];

}

__kernel void sum(const int size, const int nvec, 
      const __global float * time, 
      const __global float * rv, 
      const __global float * sigma, 
      const __global float * a1,
      const __global float * a2,
      const __global float * a3,
      const __global float * a4,
      const __global float * a5,
      const __global float * a6,
      __global float * vec2) {

   int ii = get_global_id(0);
   int i;
   float b[6];

   if(ii < size) {
      b[0] = a1[ii];
      b[1] = a2[ii];
      b[2] = a3[ii];
      b[3] = a4[ii];
      b[4] = a5[ii];
      b[5] = a6[ii];
      float temp = 0.0, chi;
      for ( i=0; i < nvec; i++) {
          chi = (rv[i] - one_planet(time[i],b))/sigma[i];
          temp += chi*chi;
      }
      if (temp == 0.0) temp += 1e-7;
      vec2[ii] = (nvec-6.0)/temp;
   }
}
