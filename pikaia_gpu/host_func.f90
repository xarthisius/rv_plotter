module gpik
  use cl

  implicit none

  type(cl_platform_id), dimension(:), allocatable   :: platform
  type(cl_device_id), dimension(:), allocatable     :: device
  type(cl_context), save       :: context
  type(cl_command_queue), save :: command_queue
  type(cl_program), save :: prog
  type(cl_kernel), save  :: kernel

   integer, parameter :: np = 6
  
  integer    :: num, ierr, irec, numd
  integer, save :: asize = 0
  integer(8) :: size_in_bytes, globalsize, localsize
  character(len = 100)  :: info
  integer, parameter :: iplat = 2
  integer, parameter :: idev  = 2
  integer, parameter :: iunit = 10
  integer, parameter :: source_length = 10000
  integer :: i
  character(len = source_length) :: source
  real, allocatable  :: v1(:), v2(:), v3(:), vec2(:)
  real, allocatable, dimension(:) :: a1, a2, a3, a4, a5, a6
  type(cl_mem)       :: cl_vec1, cl_vec2, cl_v1, cl_v2, cl_v3
  type(cl_mem)       :: cl_a1, cl_a2, cl_a3, cl_a4, cl_a5, cl_a6
  real :: start, finish, ts_, te_
  logical, parameter :: perf = .false.

  real, dimension(np) :: minaga, maxaga

  !=====================
  ! INITIALIZATION
  !=====================

contains

   subroutine init(nmax, mina, maxa, T, rv, sigma, jit)
 
      implicit none
      real, dimension(np), intent(in) :: mina, maxa
      real, dimension(:), intent(in) :: T, rv, sigma
      real, intent(in) :: jit
      integer, intent(in) :: nmax

      minaga = mina
      maxaga = maxa

      asize = size(T)
      allocate(vec2(nmax))
      allocate(a1(nmax), a2(nmax), a3(nmax), a4(nmax), a5(nmax), a6(nmax))
      allocate(v1(asize), v2(asize), v3(asize))
      v1(:) = T(:)
      v2(:) = rv(:)
      v3(:) = sqrt(sigma(:)**2 + jit**2)

      ! get the platform ID
      call clGetPlatformIDs(num, ierr)
      allocate(platform(num))
      call clGetPlatformIDs(platform, num, ierr)
      if(ierr /= CL_SUCCESS) stop "Cannot get CL platform."
 
      ! get the device ID
      call clGetDeviceIDs(platform(iplat), CL_DEVICE_TYPE_ALL, numd, ierr)
      allocate(device(numd))
      call clGetDeviceIDs(platform(iplat), CL_DEVICE_TYPE_ALL, device, numd, ierr)
      if(ierr /= CL_SUCCESS) stop "Cannot get CL device."

      ! get the device name and print it
      call clGetDeviceInfo(device(idev), CL_DEVICE_NAME, info, ierr)
      print*, "CL device: ", info

      ! create the context and the command queue
      context = clCreateContext(platform(iplat), device(idev), ierr)
      command_queue = clCreateCommandQueue(context, device(idev), CL_QUEUE_PROFILING_ENABLE, ierr)

      !=====================
      ! BUILD THE KERNEL
      !=====================

      ! read the source file
      open(unit = iunit, file = 'oneplanet.cl', access='direct', status = 'old', action = 'read', iostat = ierr, recl = 1)
      if (ierr /= 0) stop 'Cannot open file oneplanet.cl'
   
      source = ''
      irec = 1
      do
         read(unit = iunit, rec = irec, iostat = ierr) source(irec:irec)
         if (ierr /= 0) exit
         if(irec == source_length) stop 'Error: CL source file is too big'
         irec = irec + 1
      end do
      close(unit = iunit)

      ! create the program
      prog = clCreateProgramWithSource(context, source, ierr)
      if(ierr /= CL_SUCCESS) stop 'Error: cannot create program from source.'

      ! build
      call clBuildProgram(prog, '-cl-mad-enable', ierr)

      ! get the compilation log
      call clGetProgramBuildInfo(prog, device(idev), CL_PROGRAM_BUILD_LOG, source, irec)
      if(len(trim(source)) > 0) print*, trim(source)

      if(ierr /= CL_SUCCESS) stop 'Error: program build failed.'

      ! finally get the kernel and release the program
      kernel = clCreateKernel(prog, 'sum', ierr)
      call clReleaseProgram(prog, ierr)

      size_in_bytes = int(asize, 8)*4_8
      cl_v1   = clCreateBuffer(context, CL_MEM_READ_ONLY, size_in_bytes, ierr)
      cl_v2   = clCreateBuffer(context, CL_MEM_READ_ONLY, size_in_bytes, ierr)
      cl_v3   = clCreateBuffer(context, CL_MEM_READ_ONLY, size_in_bytes, ierr)
      cl_a1   = clCreateBuffer(context, CL_MEM_READ_ONLY, int(nmax,8)*4_8, ierr)
      cl_a2   = clCreateBuffer(context, CL_MEM_READ_ONLY, int(nmax,8)*4_8, ierr)
      cl_a3   = clCreateBuffer(context, CL_MEM_READ_ONLY, int(nmax,8)*4_8, ierr)
      cl_a4   = clCreateBuffer(context, CL_MEM_READ_ONLY, int(nmax,8)*4_8, ierr)
      cl_a5   = clCreateBuffer(context, CL_MEM_READ_ONLY, int(nmax,8)*4_8, ierr)
      cl_a6   = clCreateBuffer(context, CL_MEM_READ_ONLY, int(nmax,8)*4_8, ierr)
      cl_vec2 = clCreateBuffer(context, CL_MEM_READ_WRITE, int(nmax,8)*4_8, ierr)

      call clEnqueueWriteBuffer(command_queue, cl_v1, cl_bool(.true.), 0_8, size_in_bytes, v1(1), ierr)
      call clEnqueueWriteBuffer(command_queue, cl_v2, cl_bool(.true.), 0_8, size_in_bytes, v2(1), ierr)
      call clEnqueueWriteBuffer(command_queue, cl_v3, cl_bool(.true.), 0_8, size_in_bytes, v3(1), ierr)

  end subroutine init

  subroutine gpu_fitness(nmax, npar, oldph, fitness)
    implicit none
    integer, intent(in) :: nmax, npar
    real, intent(in)    :: oldph(:,:)
    real, intent(out)   :: fitness(:)

    vec2 = 0.0; fitness = 1.0
    a1(:) = oldph(1,1:nmax) * (maxaga(1) - minaga(1)) + minaga(1)
    a2(:) = oldph(2,1:nmax) * (maxaga(2) - minaga(2)) + minaga(2)
    a3(:) = oldph(3,1:nmax) * (maxaga(3) - minaga(3)) + minaga(3)
    a4(:) = oldph(4,1:nmax) * (maxaga(4) - minaga(4)) + minaga(4)
    a5(:) = oldph(5,1:nmax) * (maxaga(5) - minaga(5)) + minaga(5)
    a6(:) = oldph(6,1:nmax) * (maxaga(6) - minaga(6)) + minaga(6)

  ! copy data to device memory

    call clEnqueueWriteBuffer(command_queue, cl_a1,    cl_bool(.true.), 0_8, int(nmax,8)*4_8, a1(1), ierr)
    call clEnqueueWriteBuffer(command_queue, cl_a2,    cl_bool(.true.), 0_8, int(nmax,8)*4_8, a2(1), ierr)
    call clEnqueueWriteBuffer(command_queue, cl_a3,    cl_bool(.true.), 0_8, int(nmax,8)*4_8, a3(1), ierr)
    call clEnqueueWriteBuffer(command_queue, cl_a4,    cl_bool(.true.), 0_8, int(nmax,8)*4_8, a4(1), ierr)
    call clEnqueueWriteBuffer(command_queue, cl_a5,    cl_bool(.true.), 0_8, int(nmax,8)*4_8, a5(1), ierr)
    call clEnqueueWriteBuffer(command_queue, cl_a6,    cl_bool(.true.), 0_8, int(nmax,8)*4_8, a6(1), ierr)

    call clSetKernelArg(kernel, 0, 200, ierr)
    call clSetKernelArg(kernel, 1, asize, ierr)
    call clSetKernelArg(kernel, 2, cl_v1, ierr)
    call clSetKernelArg(kernel, 3, cl_v2, ierr)
    call clSetKernelArg(kernel, 4, cl_v3, ierr)
    call clSetKernelArg(kernel, 5, cl_a1, ierr)
    call clSetKernelArg(kernel, 6, cl_a2, ierr)
    call clSetKernelArg(kernel, 7, cl_a3, ierr)
    call clSetKernelArg(kernel, 8, cl_a4, ierr)
    call clSetKernelArg(kernel, 9, cl_a5, ierr)
    call clSetKernelArg(kernel, 10, cl_a6, ierr)
    call clSetKernelArg(kernel, 11, cl_vec2, ierr)

    ! get the localsize for the kernel (note that the sizes are integer(8) variable)
    call clGetKernelWorkGroupInfo(kernel, device(idev), CL_KERNEL_WORK_GROUP_SIZE, localsize, ierr)
    globalsize = int(nmax, 8)
    if(mod(nmax, localsize) /= 0) globalsize = globalsize + localsize - mod(nmax, localsize) 

    ! execute the kernel
    call clEnqueueNDRangeKernel(command_queue, kernel, [globalsize], [localsize], ierr)
    call clFinish(command_queue, ierr)

    ! read the resulting vector from device memory
    call clEnqueueReadBuffer(command_queue, cl_vec2, cl_bool(.true.), 0_8, int(nmax,8)*4_8, vec2(1), ierr)
    fitness(1:nmax) = vec2
    return
   end subroutine gpu_fitness

   subroutine cl_final()
      implicit none
      call clReleaseKernel(kernel, ierr)
      call clReleaseCommandQueue(command_queue, ierr)
      call clReleaseContext(context, ierr)
      deallocate(device,platform)
      deallocate(v1,v2,v3,vec2)
      deallocate(a1,a2,a3,a4,a5,a6)
   end subroutine cl_final
end module gpik
