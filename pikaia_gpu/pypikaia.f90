subroutine gpupikaia(steps, T_,rv_,sig_,nn,foo)
   use gpik, only: init, cl_final
   use ga_pikaia, only: pikaia, rninit
   use commons, only: T, rv, sigma
   implicit none
   integer, intent(in) :: nn, steps
   real (kind=8), intent(in), dimension(0:nn-1) :: T_, rv_, sig_
   real (kind=8), intent(out), dimension(0:7,0:steps-1) :: foo
   
   integer, parameter :: na = 6
   ! [ T0, P, K, e, om ,v0]
   ! [  1  2, 3, 4, 5, 6 ]
   real(kind=4), dimension(na) :: minaga = [ 50000.0, 10.0,     1.0, 0.0,   0.0, -100.0]
   real(kind=4), dimension(na) :: maxaga = [ 55000.0, 1000.0, 500.0, 0.8, 360.0, 100.0]

   integer seed, i, status, Time(3)
   integer, parameter :: n = 6
   real :: ctrl(12), x(n), f, te, ts

   ctrl(:) = [ 198.0, 700., 5., 0.85, 2., 0.005, 0.0005, 0.25, 1.0, 1.0, 1., 0.]

   print *, nn
   allocate(T(nn),rv(nn),sigma(nn))

   T(:) = T_(:)
   rv(:) = rv_(:)
   sigma(:) = sqrt(sig_(:)**2 + 50.0)

   call init(int(ctrl(1)),minaga, maxaga, real(T), real(rv), real(sig_), 50.0)
   call ITime(Time)
   call srand(Time(3)**3)
   seed=abs(Time(1)*Time(2)+Time(2)*Time(3)+sum(Time)-nint(10000.d0*rand(Time(3)**3)))
   call rninit(seed)

   call cpu_time(ts)
   do i = 1, steps
      call pikaia(n,ctrl,x,f,status)
      ! [ chi, T0, P, e, om, K, v0, status]
      x = x*(maxaga-minaga)+minaga
      foo(:,i-1) = real([1./f, x(1),x(2),x(4),x(5),x(3),x(6) , real(status)],kind=8)
      write(*,'("Done step ",I4,"/",I4)') i, steps
   enddo
   call cpu_time(te)

   write(*,*) 'xcute took ',te - ts,'s'
   deallocate(T,rv,sigma)
   call cl_final()
   return
end subroutine gpupikaia
