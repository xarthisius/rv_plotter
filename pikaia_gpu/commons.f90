module commons
   integer, parameter :: na = 6
   ! [ T0, P, K, e, om ,v0]
   real(kind=8), dimension(na) :: minaga = [ 50000.0, 10.0,     1.0, 0.0,   0.0, -100.0]
   real(kind=8), dimension(na) :: maxaga = [ 55000.0, 1000.0, 500.0, 0.8, 360.0, 100.0]
                        
   real(kind=8), dimension(:), allocatable :: T, rv, sigma

end module commons
