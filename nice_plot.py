#!/usr/bin/python
from cycler import cycler
from matplotlib import rcParams
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import numpy as np
from my_fit import keplerian_fit
from rvlin import rv_drive

MRK_FMT = ['o', 'D', '8']
LABELS = ['TNG/HARPS-N', 'HET/HRS', 'CAFE'] 
#LABELS = ['CAFE', 'HET/HRS', 'TNG/HARPS-N']
plt.style.use('tableau-colorblind10')

def to_km(y, position):
    return "%.2f" % (y / 1000.)


def single_nice_plot(pp, dplt):
    params = {'legend.fontsize': 'medium',
         'axes.labelsize': 'medium',
         'axes.titlesize':'medium',
         'axes.titlesize':'medium',
         'xtick.labelsize':'medium',
         'font.family': 'serif',
         'font.weight': 'normal',
         'mathtext.fontset': 'stix',
         'text.usetex': False,
         'ytick.labelsize':'medium'}
    rcParams.update(params)
    F = plt.figure(figsize=(11.5, 8.625), dpi=100, )
    x = np.linspace(dplt['xmin'], dplt['xmax'], 1000)

    ax = plt.subplot2grid((10, 1), (0, 0), rowspan=7)
    #ax.set_title(dplt['title'])

    for ntel in range(dplt['tel'].max() + 1):
        ind = np.where(dplt['tel'] == ntel)
        ax.errorbar(dplt['x_data'][ind], dplt['y_data'][ind],
                    yerr=dplt['ey_data'][ind], fmt=MRK_FMT[ntel],
                    label=LABELS[ntel])
                    # markeredgecolor=MRK_FMT[ntel][0])
    ax.plot(x, dplt['fit'], linewidth=2.0)
    ax.axis([dplt['xmin'], dplt['xmax'], dplt['ymin'], dplt['ymax']])
    ax.xaxis.set_major_locator(tck.MaxNLocator(5))
    ax.xaxis.set_minor_locator(tck.MaxNLocator(25))
    ax.yaxis.set_major_locator(tck.MaxNLocator(5))
    ax.yaxis.set_minor_locator(tck.MaxNLocator(25))
    ax.tick_params(axis="x", labelbottom=False)
    ax.legend(loc=0)
    span = dplt['ymax'] - dplt['ymin']
    if span >= 100.0:
        units = 'km'
        formatter = tck.FuncFormatter(to_km)
        ax.yaxis.set_major_formatter(formatter)
    else:
        units = 'm'
    ax.set_ylabel("Radial Velocity (%s s$^{-1}$)" % units)

    ax = plt.subplot2grid((10, 1), (7, 0), rowspan=3)
    ax.plot(x, np.zeros(len(x)), color='k', linewidth=2.0)
    for ntel in range(dplt['tel'].max() + 1):
        ind = np.where(dplt['tel'] == ntel)
        ax.errorbar(dplt['x_data'][ind], dplt['y2_data'][ind],
                    yerr=dplt['ey_data'][ind], fmt=MRK_FMT[ntel])
                    #markeredgecolor=MRK_FMT[ntel][0])
    ax.axis([dplt['xmin'], dplt['xmax'], dplt['eymin'], dplt['eymax']])
    span = dplt['eymax'] - dplt['eymin']
    if span >= 200.0:
        units = 'km'
        formatter = tck.FuncFormatter(to_km)
        ax.yaxis.set_major_formatter(formatter)
    else:
        units = 'm'
    ax.set_ylabel("Residual (%s s$^{-1}$)     " % units)
    ax.set_xlabel(dplt['xlabel'])
    ax.xaxis.set_major_locator(tck.MaxNLocator(5))
    ax.xaxis.set_minor_locator(tck.MaxNLocator(25))
    ax.yaxis.set_major_locator(tck.MaxNLocator(5))
    ax.yaxis.set_minor_locator(tck.MaxNLocator(25))
    plt.draw()
    pp.savefig(F, facecolor="white", bbox_inches='tight')


def nice_plot(data, gold, name, method):
    alw = rcParams["axes.linewidth"]
    lme = rcParams["lines.markeredgewidth"]
    lms = rcParams["lines.markersize"]
    fs = rcParams["font.size"]
    xmp = rcParams['xtick.major.pad']
    ymp = rcParams['ytick.major.pad']
    bck = rcParams['backend']

    rcParams["axes.linewidth"] = 2.0
    rcParams["lines.markeredgewidth"] = 1.5
    rcParams["lines.markersize"] = 8
    rcParams["axes.labelsize"] = 48
    #rcParams["font.size"] = 36
    #rcParams["xtick.labelsize"] = 36
    #rcParams["ytick.labelsize"] = 36
    #rcParams["legend.fontsize"] = 36

    rcParams['xtick.major.pad'] = 8
    rcParams['ytick.major.pad'] = 10
    rcParams['backend'] = 'PDF'

    fname = "%s.pdf" % name

    dplt = {}
    rv = data['RV']
    if 'off' in data:
        rv -= data['off']

    xmin, xmax = np.min(data['T']), np.max(data['T'])
    dplt['xmin'] = np.floor(xmin * 0.1) * 10.0 - 100.0
    dplt['xmax'] = np.ceil(xmax * 0.1) * 10.0 + 100.0

    ymin, ymax = np.min(rv) - np.max(data['eRV']), \
        np.max(rv) + np.max(data['eRV'])
    dy = (ymax - ymin) * 0.07
    dplt['ymin'] = np.floor(ymin) - dy
    dplt['ymax'] = np.ceil(ymax) + dy

    if method == 1:
        omc = rv - rv_drive(data['T'], gold)
    else:
        omc = rv - keplerian_fit(data['T'], gold)
    eymin, eymax = np.min(omc) - np.max(data['eRV']), \
        np.max(omc) + np.max(data['eRV'])
    dy = (eymax - eymin) * 0.07
    dplt['eymin'] = np.floor(eymin) - dy
    dplt['eymax'] = np.ceil(eymax) + dy

    pp = PdfPages("fit_%s" % fname)
    name.replace('_', ' ')
    dplt['title'] = name

    dplt['x_data'] = data['T']
    dplt['y_data'] = rv
    dplt['y2_data'] = omc
    dplt['ey_data'] = data['eRV']
    dplt['tel'] = data['tel']
    dplt['xlabel'] = "Epoch (MJD)"
    x = np.linspace(dplt['xmin'], dplt['xmax'], 1000)
    if method == 1:
        dplt['fit'] = rv_drive(x, gold)
    else:
        dplt['fit'] = keplerian_fit(x, gold)

    single_nice_plot(pp, dplt)
    pp.close()

    pp = PdfPages("fitp_%s" % fname)

    tmp = ((data['T'] - gold[0]) % gold[1]) / gold[1]   # FIXME
    dplt['x_data'] = np.concatenate((tmp, tmp + 1.0), axis=0)
    dplt['y_data'] = np.concatenate((rv, rv), axis=0)
    dplt['y2_data'] = np.concatenate((omc, omc), axis=0)
    dplt['ey_data'] = np.concatenate((data['eRV'], data['eRV']), axis=0)
    dplt['tel'] = np.concatenate((data['tel'], data['tel']), axis=0)
    dplt['xlabel'] = "Phase"
    if method == 1:
        dplt['fit'] = rv_drive(
            np.linspace(gold[1], gold[1] + 2.0 * gold[0], 1000), gold  # FIXME
        )
    else:
        dplt['fit'] = keplerian_fit(
            np.linspace(gold[0], gold[0] + 2.0 * gold[1], 1000), gold
        )
    dplt['xmin'] = 0.0
    dplt['xmax'] = 2.0
    single_nice_plot(pp, dplt)

    pp.close()
    print(("%s written to disk" % fname))

    rcParams["axes.linewidth"] = alw
    rcParams["lines.markeredgewidth"] = lme
    rcParams["lines.markersize"] = lms
    rcParams["font.size"] = fs
    rcParams['xtick.major.pad'] = xmp
    rcParams['ytick.major.pad'] = ymp
    rcParams['backend'] = bck


if __name__ == "__main__":
    import pickle as pickle
    import sys
    import os.path

    fname = os.path.abspath(sys.argv[1])
    with open(fname, 'rb') as pkl_file:
        fit = pickle.load(pkl_file)

    curfit = fit.fit['Planet 1']
    gold = np.array([curfit['T0'], curfit['P'], curfit['K'],
                     curfit['e'], curfit['w'], curfit['v0']])
    with plt.style.context('tableau-colorblind10'):
        nice_plot(fit.data, gold, os.path.basename(fname).split('.')[1], 0)
