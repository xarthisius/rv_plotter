import numpy as np
import random
import argparse
from mpi4py import MPI
import h5py as h5
from rvlin_jit import blah, get_long_pars
from mpfit import mpfit
from rvlin import rv_drive, rvlin
import pickle as pickle
from rv_plot import STARNAME_RE


def random_permutations(l, n):
    """http://stackoverflow.com/questions/10232338/"""
    while n:
        random.shuffle(l)
        yield list(l)
        n -= 1


def flatten(lst):
    for x in lst:
        if isinstance(x, list):
            for x in flatten(x):
                yield x
        else:
            yield x


parser = argparse.ArgumentParser()
parser.add_argument("--nperms", type=int, default=1e5)
parser.add_argument('fname', metavar='fname', type=str, nargs=1,
                    help='Plik z danymi')
args = parser.parse_args()

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    fdata = args.fname[0]
    with open(fdata, 'rb') as pkl_file:
        data = pickle.load(pkl_file)
    star = STARNAME_RE.search(fdata).group()
    p0_raw = data.fit_to_list()
    nplanets = len(p0_raw) // 5
    p0 = []
    for i in range(nplanets):
        p0 += [p0_raw[1 + i * 5], p0_raw[0 + i * 5], p0_raw[3 + i * 5]]
    p0.append(data.data['jitter'])
    p0 = np.array(p0)

    isort = np.argsort(data.data['T'])
    epoch = 0.0
    ddata = {
        'T': data.data['T'][isort] - epoch, 'RV': data.data['RV'][isort],
        'eRV': data.data['eRV'][isort],
        'jitter': data.data['jitter'],
        'tel': data.data['tel'][isort],
        'off': data.data['off'][isort]
    }
    Npoints = data.data['T'].shape[0]
    if args.nperms > 1:
        perms = [item for item in
                 random_permutations(list(range(Npoints)), args.nperms)]
    else:
        perms = [list(range(Npoints))]
    # dividing data into chunks
    chunks = [[] for _ in range(size)]
    for i, chunk in enumerate(perms):
        chunks[i % size].append(chunk)

    # [P, T, e, jit]
    parinfo = [{'value': val, 'fixed': 0, 'limited': [0, 0],
                'limits': [0.0, 0.0]} for val in p0]
    for i in range(len(p0) // 3):
        parinfo[2 + i * 3]['limited'] = [1, 1]
        parinfo[2 + i * 3]['limits'] = [0.0, 0.8]  # e
    parinfo[-1]['limited'] = [1, 0]
    parinfo[-1]['limits'] = [0.0, 1.0e4]  # jitter
else:
    perms = None
    ddata = None
    chunks = None
    p0 = None
    parinfo = None
    nplanets = None

nplanets = comm.bcast(nplanets, root=0)
perms = comm.scatter(chunks, root=0)
ddata = comm.bcast(ddata, root=0)
p0 = comm.bcast(p0, root=0)
parinfo = comm.bcast(parinfo, root=0)


final_pars = rvlin(ddata, p0[:-1], nompfit=True, trend=True)
p00 = get_long_pars(ddata, p0[:-1])
p00.append(p0[-1])
p00 = np.array(p00)
computed = rv_drive(ddata['T'][:], p00[:-1])
residua = (ddata['RV'][:] - ddata['off'][:]) - computed

nu = 4
if rank == 0:
    print(("RMS_0 = {}".format(residua.std(ddof=0))))
    print(("RMS = {}".format(residua.std(ddof=nu))))


chi2_p0 = np.sum(residua ** 2 / ddata['eRV'][:] ** 2) / (ddata['T'].shape[0] - nu)
p00[-2] = final_pars['offset']
if rank == 0:
    print(("chi = {}".format(np.sqrt(chi2_p0))))


results = np.zeros((len(perms), nplanets * 5 + 4))
fa = {'x': ddata['T'], 'y': ddata["RV"],
      'err': np.sqrt(ddata['eRV']**2 - ddata['jitter']**2), 'tel': ddata['tel']}
print(fa['err'])
for iperm, perm in enumerate(perms):
    scramble = (computed + ddata['off'][:]) + residua[perm]
    fa["y"] = scramble
    m = mpfit(blah, p0, parinfo=parinfo, quiet=True, autoderivative=1,
              functkw=fa)
    foo = {}
    foo['T'] = fa["x"]
    foo['RV'] = fa["y"]
    foo['eRV'] = np.sqrt(fa["err"] ** 2 + m.params[-1] ** 2)
    foo['tel'] = fa['tel']
    final_pars = rvlin(foo, m.params[:-1], nompfit=True, trend=True)
    p1 = get_long_pars(ddata, m.params[:-1])
    p1.append(m.params[-1])
    p1 = np.array(p1)

    eRV_1 = foo['eRV']
    chi2_p1 = np.sum(
        (ddata['RV'][:] - ddata['off'] - rv_drive(ddata['T'][:], p1[:-1])) ** 2 /
        eRV_1 ** 2) / (ddata['T'].shape[0] - nu)
    p1[-2] = final_pars['offset']

    results[iperm, :] = np.concatenate((p1, [np.sqrt(chi2_p1)]))
    if rank == 0 and (iperm % 10 == 0):
        print(("%i of %i permutations" % (iperm, len(perms))))
        if len(perms) == 1:
            print(p00)
            print(p1)

results = comm.gather(results, root=0)
if rank == 0:
    nperms = args.nperms
    with h5.File('%s_jit_1e%1i.h5' % (star, np.log10(nperms)), 'w') as h5f:
        h5f['/gold'] = np.concatenate((p00, [np.sqrt(chi2_p0)]))
        h5f['/data'] = np.concatenate(results)
