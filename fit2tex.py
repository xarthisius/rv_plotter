#!/usr/bin/python
import os.path
import pickle as pickle
import numpy as np
import argparse


def stderr_round(x, digit=0):
    if x == 0.0:
        return 0.0, 0.0
    if np.log10(x) <= 0:
        log = -int(np.log10(x)) + 1
    else:
        log = -int(np.log10(x))
    val = x * 10 ** log
    if (np.ceil(val) - val) > 0.1 * val:
        return np.ceil(val * 10.0) * 10.0 ** -(log + 1), -(log + 1)
    else:
        return np.ceil(val) * 10.0 ** -log, -log


def pretty_pm_format(mean, stddev):
    err, log = stderr_round(stddev)
    return "$%g \\pm %g$" % (round(mean / 10.0 ** log) * 10.0 ** log, err)


HEAD_TAB = r'''\begin{deluxetable}{lrrrr}
%\tabletypesize{\scriptsize}
%\rotate
\tablecaption{Relative radial velocities and bisector span of $\ldots$ \label{Tab:Table1}}
\tablewidth{0pt}
\tablehead{ \colhead{Epoch (MJD)} & RV (m s$^{-1}$)  & $\sigma_{\textrm{RV}}$ (m s$^{-1}$) &
             BS (m s$^{-1}$)  & $\sigma_{\textrm{BS}}$ (m s$^{-1}$)}
\startdata
'''
FOOT_TAB = r'''\enddata
\end{deluxetable}'''

HEAD_FIT = r"""%\tabletypesize{\scriptsize}
%\rotate
\tablecaption{Orbital parameters of $\ldots$ \label{Tab:Table2}}
\tablewidth{0pt}
\tablehead{ \colhead{Parameter} & $\ldots$ }
\startdata
"""


def tab_orbit_line(fit, head, key):
    line = head + " & "
    for pkey in list(fit.fit.keys()):
        if pkey.startswith("Planet"):
            line += "%s &" % \
                pretty_pm_format(fit.fit[pkey][key], fit.err[pkey][key])
    return line.rstrip("&") + "\\\\\n"


def tab_print_fit(fit, star_mass, e_star_mass):
    npln = len(list(fit.fit.keys())) - 1
    output = tab_orbit_line(fit, "$P$ (days)", 'P')
    output += tab_orbit_line(fit, "$T_0$ (MJD)", 'T0')
    output += tab_orbit_line(fit, "$K$ ($\\textrm{m s}^{-1}$)", 'K')
    output += tab_orbit_line(fit, "$e$", 'e')
    output += tab_orbit_line(fit, "$\\omega$ (deg)", 'w')

    line1 = "$m_2\\sin i$ ($\\textrm{M}_{\\textrm{J}}$) & "
    line2 = "$a$ (AU) & "
    for pkey in list(fit.fit.keys()):
        if pkey.startswith("Planet"):
            pno = int(pkey.split(" ")[-1])
            a2, ea2 = fit.get_planet_sma(star_mass, e_star_mass, pno)
            m2, em2 = fit.get_planet_mass(star_mass, e_star_mass, pno)
            line1 += "%s &" % pretty_pm_format(m2, em2)
            line2 += "%s &" % pretty_pm_format(a2, ea2)
    output += line1.rstrip("&") + "\\\\\n"
    output += line2.rstrip("&") + "\\\\\n"
    output += "offset ($\\textrm{m s}^{-1}$) & " 
    output += "\\multicolumn{%i}{c}{ %s } \\\\\n" % \
        (npln, pretty_pm_format(fit.offset[-1], 9999))
    output += "$V_0$ ($\\textrm{m s}^{-1}$) & "
    output += "\\multicolumn{%i}{c}{ %s } \\\\\n" % \
        (npln, pretty_pm_format(fit.fit['Offset']['v0'],
                                fit.err['Offset']['v0']))
    output += r"$\sqrt{\chi_\nu^2}$ & \multicolumn{%i}{c}{ $%.2f$ } \\" \
        % (npln, np.around(fit.chi2, 2))
    output += "\n"
    output += r"$\sigma_{\textrm{RV}}$ & \multicolumn{%i}{c}{ $%.2f$ } \\" \
        % (npln, np.around(fit.rms, 2))
    output += "\n$N_{\\textrm{obs}}$ & \\multicolumn{%i}{c}{ $%i$ }\n" \
        % (npln, len(fit.data['T']))
    return output


parser = argparse.ArgumentParser()
parser.add_argument("fit_file",
                    help="file that will be converted to tex")
parser.add_argument("--mass", "-m", help="star mass in Msun",
                    type=float, default=1.0)
parser.add_argument("--emass", "-e", help="stddev for star mass in Msun",
                    type=float, default=0.1)
args = parser.parse_args()

with open(args.fit_file, 'rb') as pkl_file:
    fit = pickle.load(pkl_file)

fname = os.path.basename(os.path.abspath(args.fit_file)).split('.')[1]

with open("%s_tab.tex" % fname, 'wb') as ftab:
    ftab.write(HEAD_TAB)
    for ind in range(len(fit.data['T'])):
        ftab.write("%f & %f & %f & %f & %f" %
                   (fit.data['T'][ind], fit.data['RV'][ind],
                    fit.data['eRV'][ind], fit.data['BS'][ind],
                    fit.data['eBS'][ind]))
        if ind < len(fit.data['T']) - 1:
            ftab.write(" \\\\\n")
        else:
            ftab.write("\n")
    ftab.write(FOOT_TAB)

with open("%s_fit.tex" % fname, 'wb') as ftab:
    npln = len(list(fit.fit.keys())) - 1
    ftab.write(r"\begin{deluxetable}")
    ftab.write("{l%s}\n" % (npln * 'c'))
    ftab.write(HEAD_FIT)
    ftab.write(tab_print_fit(fit, args.mass, args.emass))
    ftab.write(FOOT_TAB)
