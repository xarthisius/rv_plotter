#!/usr/bin/python
import operator
import sys
import PyQt5.QtWidgets as QtGui
import PyQt5.QtCore as QtCore
from PyQt5.QtGui import QFont
import numpy as np


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())


class MyWindow(QtGui.QWidget):

    send_point = QtCore.pyqtSignal(list, name="send_point")
    send_data = QtCore.pyqtSignal(dict, name="send_data")

    def __init__(self, *args):
        QtGui.QWidget.__init__(self, *args)

        # create table
        self.get_table_data()
        self.table = self.createTable()

        # create button
        self.restore_button = QtGui.QPushButton("&Restore")

        # layout
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.table)
        layout.addWidget(self.restore_button)
        self.setLayout(layout)

        self.table.clicked.connect(self.on_click)
        self.table.doubleClicked.connect(self.on_dclick)
        self.restore_button.clicked.connect(self.on_restore)

    def on_restore(self):
        for hrow in range(self.tabledata.shape[0]):
            self.table.showRow(hrow)
        self.get_data()

    def get_data(self):
        data = {'T': [], 'RV': [], 'eRV': [], 'BS': [], 'eBS': []}
        for hrow in range(self.tabledata.shape[0]):
            if not self.table.isRowHidden(hrow):
                foo = self.get_row_data(hrow)
                data['T'].append(foo[0])
                data['RV'].append(foo[1])
                data['eRV'].append(foo[2])
                data['BS'].append(foo[3])
                data['eBS'].append(foo[4])
        for key in list(data.keys()):
            data[key] = np.array(data[key])
        self.send_data.emit(data)

    def get_row_data(self, hrow):
        temp = []
        for col in range(self.tabledata.shape[-1]):
            temp.append(
                self.table.model().index(hrow, col).data()
            )
        return temp

    def on_click(self):
        data = self.get_row_data(self.table.currentIndex().row())
        data.append(self.table.currentIndex().row())
        self.send_point.emit(data)

    def on_dclick(self):
        hrow = self.table.currentIndex().row()
        self.table.hideRow(hrow)
        self.get_data()

    def get_table_data(self, tab=None):
        if tab is None:
            self.tabledata = np.array([[12345.67, -123.4567, 123.4567,
                                        -123.4567, 123.4567]])
        else:
            self.tabledata = tab
            self.table.model().update(tab)

    def createTable(self):
        # create the view
        tv = QtGui.QTableView()

        # set the table model
        header = ['T', 'RV', 'eRV', 'BS', 'eBS']
        tm = MyTableModel(self.tabledata, header, self)
        tv.setModel(tm)

        # set the minimum size
        tv.setMinimumSize(400, 300)

        # show grid
        tv.setShowGrid(True)

        # set the font
        font = QFont("Arial", 10)
        tv.setFont(font)

        # hide vertical header
        vh = tv.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = tv.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        tv.resizeColumnsToContents()

        # set row height
        nrows = len(self.tabledata)
        for row in range(nrows):
            tv.setRowHeight(row, 18)

        # enable sorting
        tv.setSortingEnabled(True)

        return tv


class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, datain, headerdata, parent=None, *args):
        """ datain: a list of lists
            headerdata: a list of strings
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.arraydata = datain.tolist()
        self.headerdata = headerdata

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        return len(self.arraydata[0])

    def data(self, index, role):
        if not index.isValid():
            return QtCore.QVariant()
        elif role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()
        return QtCore.QVariant(self.arraydata[index.row()][index.column()])

    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Horizontal and \
           role == QtCore.Qt.DisplayRole:
            return QtCore.QVariant(self.headerdata[col])
        return QtCore.QVariant()

    def update(self, tab):
        self.layoutAboutToBeChanged.emit()
        self.arraydata = tab.tolist()
        self.layoutChanged.emit()

    def sort(self, Ncol, order):
        """Sort table by given column number.
        """
        self.layoutAboutToBeChanged.emit()
        self.arraydata = sorted(self.arraydata, key=operator.itemgetter(Ncol))
        if order == QtCore.Qt.DescendingOrder:
            self.arraydata.reverse()
        self.layoutChanged.emit()

if __name__ == "__main__":
    main()
