#!/usr/bin/python

import itertools
import numpy as np

def sane_pik_defaults(nplanets):
    p_0 = {'T0': [50000.0, 55000.0], 'P': [10.0, 1000.0],
           'K': [5.0, 500.0], 'e': [0.0, 0.9], 'w': [0.0, 360.0]}

    keys = ['T0', 'P', 'K', 'e', 'w']

    foo = []
    for perm in itertools.product(list(range(2)), repeat=len(keys)):
        foo.append(
            nplanets*[p_0[key][perm[j]] for j, key in enumerate(keys)] + [0.0]
        )

    return np.array(foo)

def sane_pik_mean(nplanets):
    return np.array(nplanets*[52500.0, 50.0, 10.0, 0.0, 0.0])
