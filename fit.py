#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
RV plotter Fit Class

author: Kacper Kowalik
website: https://bitbucket.org/xarthisius/rv_plotter/
last edited: April 2013
"""

import numpy as np
import re
import pickle as pickle


class Fit:

    chi2 = 0.0
    rms = 0.0

    def __init__(self, fit, err, data, func, offset=None):
        self.no = -1
        self.fit = fit
        self.err = err
        # {'T': [], 'RV': [], 'eRV': [], 'res': [], 'tel': [],
        #  'BS': [], 'eBS': []}
        self.data = data
        self.offset = offset
        self.data['off'] = self.get_offset()
        self.func = func
        self.chi2 = self.calculate_chi2()
        self.rms = self.calculate_rms()

    def export(self, fname):
        output = open(fname, 'wb')
        pickle.dump(self, output)
        output.close()

    def get_offset(self):
        tab = np.zeros_like(self.data['RV'])
        for ntel in range(self.data['tel'].max() + 1):
            ind = np.where(self.data['tel'] == ntel)
            tab[ind] = self.offset[ntel]
        return tab

    def update_no(self, no):
        self.no = no

    def fit_to_list(self):
        if isinstance(self.fit, list):
            return np.array(self.fit)
        elif isinstance(self.fit, np.ndarray):
            return self.fit
        output = []
        labels = list(filter(re.compile("Planet").match, list(self.fit.keys())))
        for pID in labels:
            output += [self.fit[pID]['T0'], self.fit[pID]['P'],
                       self.fit[pID]['K'], self.fit[pID]['e'],
                       self.fit[pID]['w']]
        for pID in list(self.fit['Offset'].keys()):
            output += [self.fit['Offset'][pID]]
        return np.array(output)

    def calculate_chi2(self):
        return np.sum(
            (self.data['RV'] - self.data['off'] -
             self.func(self.data['T'], self.fit_to_list())) ** 2 /
            self.data['eRV'] ** 2) / (len(self.data['T']) - len(self.fit))

    def calculate_rms(self):

        return np.sqrt(
            np.sum((self.data['RV'] - self.data['off'] -
                    self.func(self.data['T'], self.fit_to_list())) ** 2)
            / (len(self.data['T']) - 1))

    def get_sane_range(self, factor=3.0):
        sane_range = {}
        for mkey in ['Offset']:
            sane_range[mkey] = {}
            for key in list(self.fit[mkey].keys()):
                if self.err[mkey][key] > 0:
                    sane_range[mkey][key] = [
                        self.fit[mkey][key] - factor * self.err[mkey][key],
                        self.fit[mkey][key] + factor * self.err[mkey][key]
                    ]
        return sane_range

    def get_sma(self, star_mass, plno):
        plt = self.fit["Planet %i" % plno]
        e_plt = self.err["Planet %i" % plno]
        a1sini = plt['K'] * \
            (plt['P'] * 86400.0 * np.sqrt(
                1.0 - plt['e'] ** 2)) / (2 * np.pi)
        siga1sini = \
            a1sini * np.sqrt(e_plt['K'] / plt['K'] ** 2 +
                             e_plt['P'] / plt['P'] ** 2 +
                             e_plt['e'] * plt['e'] ** 2)
        return a1sini, siga1sini

    def get_planet_sma(self, star_mass, e_star_mass, plno):
        plt = self.fit["Planet %i" % plno]
        e_plt = self.err["Planet %i" % plno]
        a2 = 1.96e-2 * (star_mass * plt['P'] ** 2) ** (1. / 3.)
        ea2 = abs(1. / 3. * e_star_mass / star_mass) + \
            abs(2. / 3. * e_plt['P'] / plt['P'])
        return a2, ea2 * a2

    def get_planet_mass(self, star_mass, e_star_mass, plno):
        plt = self.fit["Planet %i" % plno]
        e_plt = self.err["Planet %i" % plno]
        a1sini, siga1sini = self.get_sma(star_mass, plno)

        m2 = 3.4e-7 * a1sini * 1e-3 * (star_mass / plt['P']) ** (2. / 3.)
        em2 = abs(siga1sini / a1sini) + \
            abs(2. / 3. * e_star_mass / star_mass) + \
            abs(2. / 3. * e_plt['P'] / plt['P'])
        m2j = m2 / 0.000954502
        return m2j, em2 * m2 / 0.000954502

    def get_orbital_pars(self, star_mass):
        if isinstance(self.fit, list) or isinstance(self.fit, np.ndarray):
            return
        au_si = 149598000e3
        nplt = (len(list(self.fit.keys())) - 1)
        output = ""

        for nplt in range((len(list(self.fit.keys())) - 1)):
            plt_lbl = 'Planet %i' % (nplt + 1)
            plt = self.fit[plt_lbl]  # HACK
            plt.update(self.fit['Offset'])
            e_plt = self.err[plt_lbl]
            e_plt.update(self.err['Offset'])
            a1sini = plt['K'] * \
                (plt['P'] * 86400.0 * np.sqrt(
                    1.0 - plt['e'] ** 2)) / (2 * np.pi)
            siga1sini = \
                a1sini * np.sqrt(e_plt['K'] / plt['K'] ** 2 +
                                 e_plt['P'] / plt['P'] ** 2 +
                                 e_plt['e'] * plt['e'] ** 2)
            m2 = 3.4e-7 * a1sini * 1e-3 * (star_mass / plt['P']) ** (2. / 3.)
            m2j = m2 / 0.000954502
            m2e = m2j * 317.83
            a2 = 1.96e-2 * (star_mass * plt['P'] ** 2) ** (1. / 3.)
            fm = (86400.0 / (2 * np.pi * 6.673e-11 * 1.98892e30)) * \
                (1.0 - plt['e'] ** 2) ** 1.5 * plt['K'] ** 3 * plt['P']
            output += "### %s\n" % plt_lbl
            output += "a1sini\t= %10.4f +/- %8.4f AU\n" % \
                (a1sini / au_si, siga1sini / au_si)
            output += "Pb\t= %10.4f +/- %8.4f days\n" % \
                (plt['P'], e_plt['P'])
            output += "T0\t= %10.4f +/- %8.4f MJD\n" % \
                (plt['T0'], e_plt['T0'])
            output += "e\t= %10.4f +/- %8.4f\n" % \
                (plt['e'], e_plt['e'])
            output += "omega\t= %10.4f +/- %8.4f deg\n" % \
                (plt['w'], e_plt['w'])
            output += "K1\t= %10.4f +/- %8.4f m/s\n\n" % \
                (plt['K'], e_plt['K'])
            output += "f(m)\t= %12.4e Msun\n" % (fm)
            output += "a2\t= %9.3f AU\n" % (a2)
            output += "m2 sini\t= %8.6f Msun \n" % m2
            output += "\t= %6.3f Mj\n" % m2j
            output += "\t= %8.3f Me\n\n" % m2e

        for vel in filter(re.compile('v').match, list(plt.keys())):
            output += "%s    = %9.3f +/- %8.3f m/s\n" % \
                (vel, plt[vel], e_plt[vel])

        computed = self.func(self.data['T'], self.fit_to_list())
        residua = (self.data['RV'][:] - self.data['off'][:]) - computed
        output += "RMS_0         = %8.6f m/s\n" % residua.std(ddof=0)
        for ntel in range(self.data['tel'].max() + 1):
            ind = np.where(self.data['tel'] == ntel)
            output += "RMS_0_tel%i = %8.6f m/s\n" % (ntel, residua[ind].std(ddof=0))

        output += "RMS   = %8.6f\n" % (self.rms)
        output += "CHI^2 = %8.6f\n" % (self.chi2)

        for off in np.unique(self.data['off']):
            if abs(off) > 0.001:
                output += "offset = %17.8f m/s\n" % (off)
        return output
