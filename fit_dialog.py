#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Mpfit's dialog
"""

DataLabels = {'Planet': ['T0', 'P', 'K', 'e', 'w'],
              'Offset': ['v0', 'v1', 'v2']}
def_dict = {'fixed': False, 'fix_lb': False, 'fix_rb': False,
              'value': 0.0, 'val_lb': 0.0, 'val_rb': 0.0}
def_chk = ['fixed', 'fix_lb', 'fix_rb']
def_val = ['value', 'val_lb', 'val_rb']

import sys
from PyQt5.QtWidgets import \
    QWidget, QLabel, QDialog, QCheckBox, \
    QDoubleSpinBox, QGridLayout, QComboBox, \
    QPushButton, QHBoxLayout, QVBoxLayout, \
    QApplication
from PyQt5 import QtCore
import numpy as np
from matplotlib.backends.backend_qt5agg \
   import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.widgets import  RectangleSelector
import matplotlib.tri as tri
import matplotlib.cm as cm


def remove_duplicates(data):
    import collections
    initial_np = data.shape[0]
    x = collections.Counter(data[:, 0])
    for dupe in  [n for n, i in x.items() if i > 1]:
        xind = np.where(np.abs(data[:, 0] - dupe) == 0.0)
        y = collections.Counter(data[xind, 1].ravel())
        for dupe2 in [n for n, i in y.items() if i > 1]:
            yind = np.where((data[:, 1] == dupe2) & (data[:, 0] == dupe))
            if len(yind[0]) > 1:
                data = np.delete(data, yind[0][1:], 0)
    print(("Removed %i duplicates" % (initial_np - data.shape[0])))
    return data


def unique(array):
    '''Remove duplicate rows from 2D array'''
    order = np.lexsort(array.T)
    array = array[order]
    diff = np.diff(array, axis=0)
    temp = np.ones(len(array), 'bool')
    temp[1:] = (diff != 0).any(axis=1)
    return array[temp]


class MyField(QWidget):

    def __init__(self, Dataset, Param, parent=None):
        super(MyField, self).__init__()
        self.parent = parent
        self.Param = Param
        self.Dataset = Dataset

        self.widgets = {
            'name': QLabel(self),
            'fixed': QCheckBox('Fix', self),
            'fix_lb': QCheckBox('LBound', self),
            'fix_rb': QCheckBox('RBound', self),
            'value': QDoubleSpinBox(self),
            'val_lb': QDoubleSpinBox(self),
            'val_rb': QDoubleSpinBox(self)
        }

        self.init_UI()
        self.set_defaults()
        self.signal_handling()

    def init_UI(self):
        params_grid = QGridLayout()
        params_grid.addWidget(self.widgets['name'], 1, 1, 2, 1)
        params_grid.addWidget(self.widgets['value'], 1, 2)
        params_grid.addWidget(self.widgets['fix_lb'], 1, 3)
        params_grid.addWidget(self.widgets['val_lb'], 1, 4)
        params_grid.addWidget(self.widgets['fixed'], 2, 2)
        params_grid.addWidget(self.widgets['fix_rb'], 2, 3)
        params_grid.addWidget(self.widgets['val_rb'], 2, 4)

        self.setLayout(params_grid)

    def set_defaults(self):
        self.widgets['name'].setText(self.Param)

        for i in ['value', 'val_lb', 'val_rb']:
            self.widgets[i].setValue(0.0)
            self.widgets[i].setMaximum(999999.0)
            self.widgets[i].setMinimum(-999999.0)

        for i in ['val_lb', 'val_rb']:
            self.widgets[i].setEnabled(False)

    def update_vals(self, newvals):
        for key in def_chk:
            self.widgets[key].setChecked(newvals[key])
        for key in def_val:
            self.widgets[key].setValue(newvals[key])
        self.stateChnged()

    def signal_handling(self):
        self.widgets['fix_lb'].toggled.connect(self.wrapper_lb)
        self.widgets['fix_rb'].toggled.connect(self.wrapper_rb)
        self.widgets['fixed'].toggled.connect(self.disable_ranges)
        for i in ['value', 'val_lb', 'val_rb']:
            self.widgets[i].editingFinished.connect(self.stateChnged)

    def wrapper_lb(self, tn):
        self.widgets['val_lb'].setEnabled(tn)
        self.stateChnged()

    def wrapper_rb(self, tn):
        self.widgets['val_rb'].setEnabled(tn)
        self.stateChnged()

    def disable_ranges(self, tn):
        if tn:
            for i in ['fix_lb', 'fix_rb', 'val_lb', 'val_rb']:
                self.widgets[i].setEnabled(False)
        else:
            for i in ['fix_lb', 'fix_rb']:
                self.widgets[i].setEnabled(True)
            if self.widgets['fix_lb'].isChecked():
                self.widgets['val_lb'].setEnabled(True)
            if self.widgets['fix_rb'].isChecked():
                self.widgets['val_rb'].setEnabled(True)
        self.stateChnged()

    def stateChnged(self):
        data = {}
        for key in def_val:
            data[key] = self.widgets[key].value()
        for key in def_chk:
            data[key] = self.widgets[key].isChecked()
        self.parent.MainWidget.ValueChanged(self.Dataset, self.Param, data)


class ParamGrid(QWidget):

    ParamItems = {}

    def __init__(self, Dataset, parent=None):
        super(ParamGrid, self).__init__()
        self.MainWidget = parent

        params_grid = QGridLayout()
        for n, Param in enumerate(DataLabels[Dataset]):
            self.ParamItems[Param] = (MyField(Dataset, Param, self))
            params_grid.addWidget(self.ParamItems[Param], n + 1, 1)
        self.setLayout(params_grid)


class fit_dialog(QDialog):

    DataTable = {}
    data = None
    fig = None
    canvas = None
    pikaia_data = None
    ts = [0]

    def __init__(self, nplanets, data=None, best=None, parent=None):
        super(fit_dialog, self).__init__(parent)

        self.nplanets = nplanets
        self.planetsIDs = ["Planet %i" % (i + 1) for i in range(self.nplanets)]
        self.Current = {
                'Planet': self.planetsIDs[0] if len(self.planetsIDs) else None,
                'Offset': 'Offset'}

        if data is not None:
            self.pikaia_data = data
        if best is not None:
            self.best = best

        self.planets = QComboBox()
        self.PlanetFitForm = ParamGrid('Planet', self)
        self.offsets = QComboBox()
        self.OffsetsForm = ParamGrid('Offset', self)
        self.okButton = QPushButton("OK")
        self.cancelButton = QPushButton("Cancel")

        self.xaxis = QComboBox()
        self.lbl = QLabel('vs')
        self.lbl.setAlignment(QtCore.Qt.AlignHCenter)
        self.yaxis = QComboBox()

        self.tri_ax = []
        self.dpi = 92
        self.data = np.random.randn(100, 3)
        self.data = unique(self.data)  # prevent bug in matplotlib
        self.prep_plots()

        self.initUI()
        self.InitVals()
        self.UpdateOffs()
        self.UpdateVals()
        self.UpdatePlot()

        self.show()

    def prep_plots(self):
        '''
        Create three subplots for triangulated maps from pikaia's data
        '''
        if self.fig is None:
            self.fig = Figure(dpi=self.dpi)
        if self.canvas is None:
            self.canvas = FigureCanvas(self.fig)

        if len(self.tri_ax) > 0:
            return

        self.tri_ax.append(self.fig.add_subplot(111))

    def do_voodoo(self):
        '''
        Create triangulated maps from pikaia's data to ease parameter choice
        '''
        xlbl = ['$T_0$ [days]', 'K [m/s]', '$\omega$ [deg]']
        ylbl = ['P [days]', 'e', '$v_0$ [m/s]']
        self.tri_ax[0].clear()
        data = remove_duplicates(np.array([self.data[:, 0],
               self.data[:, 1], self.data[:, 2]]).T)
        triang = tri.Triangulation(data[:, 0], data[:, 1])
        self.tri_ax[0].tricontour(triang, data[:, 2], linewidths=0.5,
                    colors='k')
        self.tri_ax[0].tricontourf(triang, data[:, 2],
                    cmap=cm.get_cmap(name="jet"))
        self.tri_ax[0].plot(data[:, 0], data[:, 1], 'ko', ms=3)
        self.ts[0] = RectangleSelector(self.tri_ax[0], self.onselect,
                                       drawtype='box')
        self.fig.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.12)
        self.canvas.draw()

    def initUI(self):

        for planetID in self.planetsIDs:
            self.planets.addItem(planetID)
            for param in DataLabels['Planet']:
                key = "%s - %s" % (planetID, param)
                self.xaxis.addItem(key)
                self.yaxis.addItem(key)
        self.planets.setCurrentIndex(
                self.planetsIDs.index(self.Current['Planet']))

        for offset in ['None', 'Constant', 'Linear', 'Quadratic']:
            self.offsets.addItem(offset)
        self.offsets.setCurrentIndex(0)

        self.xaxis.setCurrentIndex(0)
        self.yaxis.setCurrentIndex(1)

        hbox = QHBoxLayout()
        hbox.addWidget(self.okButton)
        hbox.addWidget(self.cancelButton)

        vbox = QVBoxLayout()
        vbox.addWidget(self.planets)
        vbox.addWidget(self.PlanetFitForm)
        vbox.addWidget(self.offsets)
        vbox.addWidget(self.OffsetsForm)
        vbox.addLayout(hbox)

        hbox2 = QHBoxLayout()
        hbox2.addWidget(self.xaxis)
        hbox2.addWidget(self.lbl)
        hbox2.addWidget(self.yaxis)

        vbox2 = QVBoxLayout()
        vbox2.addWidget(self.canvas)
        vbox2.addLayout(hbox2)

        hbox_main = QGridLayout()
        hbox_main.addLayout(vbox, 1, 1)
        hbox_main.addLayout(vbox2, 1, 2, 1, 4)

        self.setLayout(hbox_main)

        self.setWindowTitle('Mpfit feeder')

        self.planets.currentIndexChanged.connect(self.UpdateVals)
        self.offsets.currentIndexChanged.connect(self.UpdateOffs)
        for widget in [self.xaxis, self.yaxis]:
            widget.currentIndexChanged.connect(self.UpdatePlot)

        self.cancelButton.clicked.connect(self.button_action)
        self.okButton.clicked.connect(self.button_action)

    def InitVals(self):
        sane_init = {'fixed': False, 'fix_lb': False, 'fix_rb': False,
                      'value': 0.0, 'val_lb': 0.0, 'val_rb': 0.0}
        for planetID in self.planetsIDs:
            self.DataTable[planetID] = \
                {param: sane_init.copy() for param in DataLabels['Planet']}.copy()
        self.DataTable['Offset'] = \
            {param: sane_init.copy() for param in DataLabels['Offset']}.copy()

        if self.best is not None:
            for planetID in self.planetsIDs:
                for param in DataLabels['Planet']:
                    self.DataTable[planetID][param]['value'] = \
                            self.best[planetID][param][0]
            for off in DataLabels['Offset']:
                if off in list(self.best['Offset'].keys()):
                    self.DataTable['Offset'][off]['value'] = \
                            self.best['Offset'][off][0]
                    self.offsets.setCurrentIndex(len(list(self.best['Offset'].keys())))

    def UpdateXAxis(self, text):
        planetId, param = tuple(str(text).split(' - '))

    def UpdatePlot(self):
        XPlanetID, XParam = tuple(str(self.xaxis.currentText()).split(' - '))
        YPlanetID, YParam = tuple(str(self.yaxis.currentText()).split(' - '))

        nelem = len(self.pikaia_data['chi2'][:])

        self.data = np.zeros( [nelem, 3] )
        self.data[:, 0] = self.pikaia_data[XPlanetID][XParam][:]
        self.data[:, 1] = self.pikaia_data[YPlanetID][YParam][:]
        self.data[:, 2] = self.pikaia_data['chi2'][:]

        self.do_voodoo()

    def UpdateVals(self):
        self.Current['Planet'] = str(self.planets.currentText())

        for item in ['Planet', 'Offset']:
            for Param in DataLabels[item]:
                self.PlanetFitForm.ParamItems[Param].update_vals(
                    self.DataTable[self.Current[item]][Param]
                )

    def UpdateOffs(self):
        for ei, Param in enumerate(DataLabels['Offset']):
            tn = ei < self.offsets.currentIndex()
            for key, item in list(self.PlanetFitForm.ParamItems[Param].widgets.items()):
                item.setEnabled(tn)

    def ValueChanged(self, Dataset, Param, newValue):
        self.DataTable[self.Current[Dataset]][Param] = newValue

    def button_action(self):
        if self.sender().text() == 'Cancel':
            self.DataTable = None
            self.reject()
        self.accept()

    def onselect(self, eclick, erelease):
        '''
        Convert rectangle selection made on triangulation maps to actual
        parameters that are return by this form
        '''
        #'eclick and erelease are matplotlib events at press and release'
        # print(' startposition : (%f, %f)' % (eclick.xdata, eclick.ydata))
        # print(' endposition   : (%f, %f)' % (erelease.xdata, erelease.ydata))
        # eclick.inaxes.rowNum

        XPlanetID, XParam = tuple(str(self.xaxis.currentText()).split(' - '))
        YPlanetID, YParam = tuple(str(self.yaxis.currentText()).split(' - '))

        x_min = min(eclick.xdata, erelease.xdata)
        x_max = max(eclick.xdata, erelease.xdata)
        y_min = min(eclick.ydata, erelease.ydata)
        y_max = max(eclick.ydata, erelease.ydata)

        self.DataTable[XPlanetID][XParam]['val_lb'] = x_min
        self.DataTable[XPlanetID][XParam]['fix_lb'] = True
        self.DataTable[XPlanetID][XParam]['val_rb'] = x_max
        self.DataTable[XPlanetID][XParam]['fix_rb'] = True
        self.DataTable[YPlanetID][YParam]['val_lb'] = y_min
        self.DataTable[YPlanetID][YParam]['fix_lb'] = True
        self.DataTable[YPlanetID][YParam]['val_rb'] = y_max
        self.DataTable[YPlanetID][YParam]['fix_rb'] = True

        for planet, param in [(XPlanetID, XParam), (YPlanetID, YParam)]:
            if planet == self.Current['Planet']:
                self.PlanetFitForm.ParamItems[param].update_vals(
                    self.DataTable[planet][param]
                )


    def result(self):
        if self.DataTable is  None:
            return None
        output = []
        for planetID in self.planetsIDs:
            for param in DataLabels['Planet']:
                temp = {
                    'value': self.DataTable[planetID][param]['value'],
                    'fixed': self.DataTable[planetID][param]['fixed'],
                    'limited': [ self.DataTable[planetID][param]['fix_lb'], \
                                 self.DataTable[planetID][param]['fix_rb'] ],
                    'limits': [ self.DataTable[planetID][param]['val_lb'], \
                                self.DataTable[planetID][param]['val_rb'] ]
                }
                if param == 'e':
                    if not temp['limited'][0]:
                        temp['limited'][0] = 1
                        temp['limits'][0] = 0.0
                    if not temp['limited'][1]:
                        temp['limited'][1] = 1
                        temp['limits'][1] = 0.99
                if param == 'w':
                    if not temp['limited'][0]:
                        temp['limited'][0] = 1
                        temp['limits'][0] = 0.0
                    if not temp['limited'][1]:
                        temp['limited'][1] = 1
                        temp['limits'][1] = 360.0
                output.append(temp)
        for ei, off in enumerate(DataLabels['Offset']):
            if off in list(self.DataTable['Offset'].keys()):
                if ei < self.offsets.currentIndex():
                    temp = {
                        'value': self.DataTable['Offset'][off]['value'],
                        'fixed': self.DataTable['Offset'][off]['fixed'],
                        'limited': [ self.DataTable['Offset'][off]['fix_lb'], \
                                    self.DataTable['Offset'][off]['fix_rb'] ],
                        'limits': [ self.DataTable['Offset'][off]['val_lb'], \
                                    self.DataTable['Offset'][off]['val_rb'] ]
                    }
                    output.append(temp)
        return output


def decode_pikaia(pikaia_data):
    result = {'Offset': {}}

    offs = [('v%i' % i) for i in range(0, 5)]
    nplanets = pikaia_data[:,:-1].shape[-1] // 5
    noffsets = pikaia_data[:,:-1].shape[-1] % 5

    for planet in range(0, nplanets):
        key = "Planet %i" % (planet + 1)
        result[key] = {}
        for indx, item in enumerate(['T0', 'P', 'K', 'e', 'w']):
            i = indx + 5 * planet
            result[key][item] = pikaia_data[:, i]
    for i in range(0, noffsets):
        result['Offset'][offs[i]] = pikaia_data[:, nplanets * 5 + i]

    result['chi2'] = pikaia_data[:,-1]
    return result


def main():
    app = QApplication(sys.argv)
    pik_arr = np.genfromtxt('TYC_3304_00323.58.dst530308.pikaia',
            unpack=True).T
    pik_full = decode_pikaia(pik_arr[:,:-1])
    pik_best = decode_pikaia(np.array([
        pik_arr[np.argmin(pik_arr[:, -2]), :-1]
        ]))
    form = fit_dialog(nplanets=2, data=pik_full, best=pik_best)
    form.show()
    app.exec_()
    res = form.result()
    print(res)
    print(len(res) // 5)
    print(len(res) % 5)

if __name__ == '__main__':
    main()
