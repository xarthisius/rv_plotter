#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
RV plotter

author: Kacper Kowalik
website: https://bitbucket.org/xarthisius/rv_plotter/
last edited: October 2012
"""

import sys
import os
import random
import re
import numpy as np
import pickle

from PyQt5 import QtWidgets as QtGui
from PyQt5 import QtCore

from matplotlib.backends.backend_qt5agg \
    import FigureCanvasQTAgg as FigureCanvas
try:
    from matplotlib.backends.backend_qt5agg \
        import NavigationToolbar2QTAgg as NavigationToolbar
except ImportError:
    from matplotlib.backends.backend_qt5agg \
        import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from mpfit import mpfit
from my_fit import keplerian_fit
from pypikaia import pypikaia
from pikaia_dialog import pik_dial, parse_pik_dial
from tabelka import MyWindow
from fit import Fit
from rvlin_jit import get_long_pars

debug = False
MRK_FMT = ['ro', 'go', 'bo']
STARNAME_RE = re.compile('''(PTPS_\d{4}|TYC_\d{4}_\d{5})''', re.IGNORECASE)

def add_actions(target, actions):
    for action in actions:
        if action is None:
            target.addSeparator()
        else:
            target.addAction(action)


class Example(QtGui.QMainWindow):

    cur_fit_no = -1
    fits = []
    point = None
    pik = None
    pik_full = None
    nplanets = 1
    star_TYC = "unknown"
    star_rvs = 0

    send_point = QtCore.pyqtSignal(list, name='send_point')
    send_data = QtCore.pyqtSignal(dict, name='send_data')

    def __init__(self):
        self.data = {'T': [], 'RV': [], 'eRV': [], 'res': [], 'tel': [],
                     'off': [], 'BS': [], 'eBS': []}
        super(Example, self).__init__()
        self.setWindowTitle('RV Plotter')

        self.main_frame = QtGui.QWidget()

        # Create the mpl Figure and FigCanvas objects.
        # 5x4 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = Figure((10.0, 8.0), dpi=self.dpi)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.main_frame)
        # Since we have only one plot, we can use add_axes
        # instead of add_subplot, but then the subplot
        # configuration tool in the navigation toolbar wouldn't
        # work.
        #
        from mpl_toolkits.axes_grid1 import ImageGrid
        self.grid = ImageGrid(
            self.fig, 111,  # similar to subplot(111)
            nrows_ncols=(2, 1),
            axes_pad=0.1,
            label_mode="L",
            aspect=False,
        )
        self.axes = self.grid[0]
        self.axes.tick_params(axis="x", labelbottom=False)
        #
        # Bind the 'pick' event for clicking on one of the bars
        #
        self.canvas.mpl_connect('pick_event', self.on_pick)
        # Create the navigation toolbar, tied to the canvas
        #
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.main_frame)
        #
        self.file_menu = self.menuBar().addMenu("&File")
        #
        self.draw_button = QtGui.QPushButton("&Draw")
        self.hist_button = QtGui.QPushButton("&Histogram")
        self.fit_button = QtGui.QPushButton("&Fit")
        self.pik_button = QtGui.QPushButton("&Pikaia")
        #
        self.jitter_label = QtGui.QLabel(self)
        self.jitter_label.setText('Jitter:')
        self.jitter_field = QtGui.QDoubleSpinBox()
        self.jitter_field.valueChanged[float].connect(self.jitterChanged)
        self.jitter_field.setEnabled(False)
        self.jitter_field.setMaximum(200.0)
        self.jitter_field.setMinimum(0.0)

        self.mass_label = QtGui.QLabel(self)
        self.mass_label.setText('Mass of the host star:')
        self.mass_field = QtGui.QDoubleSpinBox()
        self.mass_field.setEnabled(False)
        self.mass_field.setValue(1.0)
        self.mass_field.valueChanged[float].connect(self.massChanged)
        #
        self.star_name_label = QtGui.QLabel(self)
        self.star_name_label.setText('Star Name:')
        self.star_name_field = QtGui.QLineEdit()
        self.star_name_field.setEnabled(False)
        #
        self.textedit = QtGui.QTextEdit()
        self.textedit.setMinimumWidth(300)
        #
        self.tabelka = MyWindow()
        self.tabelka.send_point.connect(self.point_selected)
        self.tabelka.send_data.connect(self.point_removed)
        self.tabelka.show()
        self.initUI()

    def jitterChanged(self, newval):
        self.data['eRV'] = np.sqrt(
            self.data['eRV'] ** 2 - self.data['jitter'] ** 2)
        self.data['eRV'] = np.sqrt(self.data['eRV'] ** 2 + newval ** 2)
        self.data['jitter'] = newval
        self.on_draw()

    def exportFit(self):
        if self.cur_fit_no >= 0:
            file_choices = "Fit (*.pkl)|*.pkl"

            fname = "fit_%s_%i.pkl" % \
                (self.star_TYC, self.star_rvs)
            path = QtGui.QFileDialog.getSaveFileName(
                self, 'Save file', fname, file_choices)
            self.fits[self.cur_fit_no].export(path[0])

    def massChanged(self, newval):
        self.star_mass = newval
        if self.cur_fit_no >= 0:
            self.textedit.setText(
                self.fits[self.cur_fit_no].get_orbital_pars(self.star_mass))

    def point_removed(self, new_data):
        self.data.update(new_data)
        self.point = None
        self.on_draw()

    def point_selected(self, lista):
        ind = np.where(abs(self.data['T'] - lista[0]) < 0.1)[0]
        if len(ind) == 1:
            self.point = [self.data['T'][ind[0]], self.data['RV'][ind[0]] - self.data['off'][ind[0]],
                          self.data['eRV'][ind[0]]]
            self.on_draw()

    def initUI(self):
        self.setGeometry(100, 100, 1200, 1000)
        self.create_menu()
        self.create_status_bar()
        self.create_main_frame()

    def create_menu(self):
        load_file_action = self.create_action("&Save plot",
                                              shortcut="Ctrl+S", slot=self.save_plot,
                                              tip="Save the plot")
        save_nice_plot = self.create_action("&Nice plot",
                                            shortcut="Ctrl+N", slot=self.on_niceplot,
                                            tip="Save paper-grade level plot")
        openfile = self.create_action("&Open file",
                                      shortcut="Ctrl+O", slot=self.open_file)
        exportfit = self.create_action("&Export fit", tip="Save fit to file",
                                       shortcut="Ctrl+E", slot=self.exportFit)
        quit_action = self.create_action("&Quit", slot=self.on_close,
                                         shortcut="Ctrl+Q", tip="Close the application")

        add_actions(self.file_menu,
                    (load_file_action, openfile, save_nice_plot, exportfit, None, quit_action))

    def on_close(self):
        self.tabelka.close()
        self.close()

    def create_status_bar(self):
        self.statusBar().addWidget(QtGui.QLabel("Welcome to RV plotter"), 1)

    def create_action(self, text, slot=None, shortcut=None,
                      icon=None, tip=None, checkable=False):
        action = QtGui.QAction(text, self)
        if icon is not None:
            action.setIcon(QtGui.QIcon(":/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            action.triggered.connect(slot)
        if checkable:
            action.setCheckable(True)
        return action

    def save_plot(self):
        file_choices = "PNG (*.png)|*.png"

        path = str(QtGui.QFileDialog.getSaveFileName(self,
                                                         'Save file', '',
                                                         file_choices))
        if path:
            self.canvas.print_figure(path.encode('utf-8'), dpi=self.dpi)
            self.statusBar().showMessage('Saved to %s' % path, 2000)

    def create_main_frame(self):

        # Other GUI controls
        #
        self.draw_button.clicked.connect(self.on_draw)
        self.hist_button.clicked.connect(self.on_hist)
        self.fit_button.clicked.connect(self.on_fit)
        self.pik_button.clicked.connect(self.on_pikaia)
        #
        # Layout with box sizers
        #
        hbox = QtGui.QHBoxLayout()

        for w in [self.draw_button, self.hist_button,
                  self.pik_button, self.fit_button]:
            hbox.addWidget(w)
            hbox.setAlignment(w, QtCore.Qt.AlignVCenter)

        hbox2 = QtGui.QHBoxLayout()
        for w in [self.jitter_label, self.jitter_field,
                  self.mass_label, self.mass_field]:
            hbox2.addWidget(w)
            # hbox2.setAlignment(w, QtCore.Qt.AlignVCenter)

        hbox3 = QtGui.QHBoxLayout()
        for w in [self.star_name_label, self.star_name_field]:
            hbox3.addWidget(w)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.canvas)
        vbox.addWidget(self.mpl_toolbar)
        vbox.addLayout(hbox)
        vbox.addLayout(hbox2)
        vbox.addLayout(hbox3)

        main_hbox = QtGui.QHBoxLayout()
        main_hbox.addLayout(vbox)
        main_hbox.addWidget(self.textedit)
        # vbox.addWidget(self.textedit)

        self.main_frame.setLayout(main_hbox)
        self.setCentralWidget(self.main_frame)

    def on_pikaia(self):

        fname = "%s_%i.pikaia" % (self.star_TYC, self.star_rvs)
        if os.path.isfile(fname):
            reuse = QtGui.QMessageBox.question(self, 'Message',
                                               "I've found old result! Do you want to reuse it?",
                                               QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                               QtGui.QMessageBox.No)
        else:
            reuse = QtGui.QMessageBox.No
        if reuse == QtGui.QMessageBox.Yes:
            self.pik_full = np.genfromtxt(fname, unpack=True).T
            self.nplanets = self.pik_full[:, :-1].shape[-1] // 5
        else:
            self.nplanets, ok = QtGui.QInputDialog.getInt(self, 'No planets',
                                                          'Number of planets for keplerian fit:',
                                                          value=1)
            if not ok:
                self.nplanets = 1

            iters, ok = QtGui.QInputDialog.getInt(self, 'Input Dialog',
                                                  'Enter nuber of iterations:',
                                                  value=1)

            if not ok:
                iters = 10

            # if there's a current fit take initial values
            if self.pik_initvals is None:
                self.pik_initvals = {}

            for i in range(self.nplanets):
                if 'Planet %i' % (i + 1) not in self.pik_initvals:
                    self.pik_initvals['Planet %i' % (i + 1)] = \
                        {'e': [0.0, 0.9], 'w': [0.0, 360.0],
                         'K': [5.0, self.data['RV'].max() - self.data['RV'].min()],
                         'P': [10.0, 1.5 * (self.data['T'].max() - self.data['T'].min())],
                         'T0': [self.data['T'].min() * 0.97, self.data['T'].max() * 1.03]}

            form = pik_dial(nplanets=self.nplanets, initvals=self.pik_initvals)
            form.setAttribute(QtCore.Qt.WA_DeleteOnClose)
            form.exec_()
            pikaia_form = form.result()
            self.pik_initvals.update(pikaia_form)

            mina, maxa = parse_pik_dial(pikaia_form)
            if any(mina > maxa):
                print("Minimum cannot be grater than maximum, try again!")
                return

            self.statusBar().showMessage('Running pikaia!')
            ctrl = -np.ones((12))

            seed = random.randint(1, 999999)
            method = 0
            if any(self.data['tel'] > 0):
                method = 2

            nplanets = len(mina) // 5
            if method == 2:
                tmina = []
                tmaxa = []
                for j in range(nplanets):
                    tmina += [mina[1 + j * 5],
                              mina[0 + j * 5], mina[3 + j * 5]]
                    tmaxa += [maxa[1 + j * 5],
                              maxa[0 + j * 5], maxa[3 + j * 5]]
                mina = tmina + [0.0]
                maxa = tmaxa + [50.0]

            if debug:
                import pickle as pickle
                with open('data.pkl', 'w') as output:
                    pickle.dump(self.data['T'], output)
                    pickle.dump(self.data['RV'], output)
                    pickle.dump(self.data['eRV'], output)
                    pickle.dump(self.data['tel'], output)
                    pickle.dump(mina, output)
                    pickle.dump(maxa, output)
                    pickle.dump(method, output)
                exit()

            print(method)
            x, f, stat = pypikaia(
                self.data['T'], self.data['RV'], self.data['eRV'],
                self.data['tel'], seed, ctrl, mina, maxa, iters, method
            )
            self.statusBar().showMessage(
                'Pikaia gathered result and is happy!')

            if method == 2:
                xnew = np.zeros(
                    (x.shape[0], nplanets * 5 + (x.shape[1] % 3) + 1))
                xnew[:, -1] = x[:, -1]
                xnew[:, -2] = x[:, -2]
                xnew[:, -3] = x[:, -3]

                # cannot copy sequence with size 15 to array axis with dimension 16
                for j in range(x.shape[0]):
                    xnew[j, 0:nplanets * 5 + 1] = get_long_pars(
                        self.data, x[j, 0:nplanets * 3], shuffle=True)[0:nplanets * 5 + 1]
                x = xnew
            # ToDo fix the wild amount of tranpositions
            v = np.vstack((f, stat)).T
            self.pik_full = np.vstack((x.T, v.T)).T
            np.savetxt(fname, self.pik_full, fmt="%12.6G")

        best = self.pik_full[np.argmin(self.pik_full[:, -2]), :]
        # new_jit = best[-3]
        # self.jitterChanged(new_jit)
        # self.jitter_field.setValue(self.data['jitter'])
        self.pik = best[0:6]
        return

    def on_draw(self):
        """ Redraws the figure
        """
        # clear the axes and redraw the plot anew
        #
        self.axes.clear()
        if len(self.fits) < 1:
            self.data['off'] = np.zeros_like(self.data['RV'])
        else:
            self.data['off'] = self.fits[self.cur_fit_no].get_offset()
        for ntel in range(self.data['tel'].max() + 1):
            ind = np.where(self.data['tel'] == ntel)
            self.axes.errorbar(
                self.data['T'][ind], self.data['RV'][
                    ind] - self.data['off'][ind],
                yerr=self.data['eRV'][ind], fmt=MRK_FMT[ntel],
                picker=5
            )
        if self.point is not None:
            self.axes.errorbar(
                self.point[0], self.point[1], yerr=self.point[2],
                fmt='go'
            )
        xmin, xmax = np.min(self.data['T']), np.max(self.data['T'])
        xmin = np.floor(xmin / 10.) * 10. - 100.0
        xmax = np.ceil(xmax / 10.) * 10. + 100.0
        for ax in self.grid:
            ax.set_xlim(left=xmin, right=xmax)
        for f_no, cur_fit in enumerate(self.fits):
            if cur_fit.fit['Planet 1']['P'] > 0:
                plot_name = "Fit %i" % (f_no)
                # % (f_no, cur_fit.rms, cur_fit.chi2)
                self.axes.plot(
                    self.x, keplerian_fit(
                        self.x, cur_fit.fit_to_list()),  # FIXME
                    label=plot_name, picker=5)
                cur_fit.update_no(f_no)
        if len(self.fits) > 0:
            self.draw_residuals()
        self.canvas.draw()

    def draw_residuals(self):
        if len(self.fits) < 1:
            self.data['off'] = np.zeros_like(self.data['RV'])
        else:
            self.data['off'] = self.fits[self.cur_fit_no].get_offset()
        cf = self.fits[self.cur_fit_no]
        self.data['res'] = self.data['RV'] - self.data['off'] - \
            cf.func(self.data['T'], cf.fit_to_list())
        self.grid[1].clear()
        for ntel in range(self.data['tel'].max() + 1):
            ind = np.where(self.data['tel'] == ntel)
            self.grid[1].errorbar(self.data['T'][ind], self.data['res'][ind],
                                  yerr=self.data['eRV'][ind], picker=5,
                                  fmt=MRK_FMT[ntel]
                                  )
        self.canvas.draw()

    def on_niceplot(self):
        import nice_plot as nicep
        nicep.nice_plot(self.data, self.fits[self.cur_fit_no]
                        .fit_to_list(), self.star_name_field.text(), 0)

    def on_hist(self):
        self.statusBar().showMessage(
            'Doing histogram magic this may take a while'
        )
        np_remove, ok = QtGui.QInputDialog.getInt(self, 'Np remove',
                                                  'How many points to remove from data:',
                                                  value=2)
        if ok:
            if len(self.data['res']) > 0:
                reuse = QtGui.QMessageBox.question(self, 'Message',
                                                   "Do you want to work on residuals?",
                                                   QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                                   QtGui.QMessageBox.No)
            else:
                reuse = QtGui.QMessageBox.No

            from hists import hist_magic
            if reuse == QtGui.QMessageBox.No:
                hist_magic(self.data['T'], self.data['RV'] - self.data['off'], self.data['eRV'],
                           np_remove, self.star_name_field.text())
            else:
                hist_magic(self.data['T'], self.data['res'], self.data['eRV'],
                           np_remove, self.star_name_field.text())

    def old_mpfit(self, p0, parinfo):
        from my_fit import myfunc
        from fit_dialog import DataLabels
        m = mpfit(myfunc, p0, parinfo=parinfo, quiet=True,
                  functkw={'x': self.data['T'], 'y': self.data['RV'],
                           'err': self.data['eRV']}
                  )

        nplanets = len(m.params) // 5
        noffsets = len(m.params) % 5
        fit = {}
        err = {}
        ind = 0
        for planet in range(nplanets):
            planetID = "Planet %i" % (planet + 1)
            fit[planetID] = \
                {param: 0.0 for param in DataLabels['Planet']}.copy()
            err[planetID] = \
                {param: 0.0 for param in DataLabels['Planet']}.copy()
            for param in DataLabels['Planet']:
                fit[planetID][param] = m.params[ind]
                err[planetID][param] = m.perror[ind]
                ind += 1
        fit['Offset'] = \
            {param: 0.0 for param in DataLabels['Offset']}.copy()
        err['Offset'] = \
            {param: 0.0 for param in DataLabels['Offset']}.copy()
        for offset in range(noffsets):
            fit['Offset'][DataLabels['Offset'][offset]] = m.params[ind]
            err['Offset'][DataLabels['Offset'][offset]] = m.perror[ind]
            ind += 1
        return fit, err, nplanets, np.zeros(nplanets).tolist()

    def new_mpfit(self, p0, parinfo, trend=False):
        from rvlin import myfunc, rvlin
        from fit_dialog import DataLabels
        nplt = len(p0) // 5
        my_p0 = []
        my_info = []
        for iplt in range(nplt):
            my_p0 += [p0[1 + iplt * 5], p0[0 + iplt * 5], p0[3 + iplt * 5]]
            my_info += [parinfo[1 + iplt * 5],
                        parinfo[0 + iplt * 5], parinfo[3 + iplt * 5]]

        my_p0.append(10.0)
        my_info.append(
            {'value': 10., 'fixed': 0, 'limited': [1, 1], 'limits': [0., 200.]})
        m = mpfit(myfunc, my_p0, parinfo=my_info, quiet=True, autoderivative=1,
                  functkw={'x': self.data['T'], 'y': self.data['RV'],
                           'err': self.data['eRV'], 'tel': self.data['tel']}
                  )
        my_p0.pop()
        my_info.pop()
        self.jitterChanged(m.params[-1])
        fp = rvlin(self.data, m.params[:-1], nompfit=True, trend=False)
        a = fp['pars']
        b = np.reshape(a, (7, nplt))
        # npars = (len(a) % 5) // nplt
        new_params = []
        for iplt in range(nplt):
            new_params += [b[1][iplt], b[0][iplt], b[4][iplt], b[2][iplt],
                           b[3][iplt]]
        new_params.append(b[-2][0])
        if trend:
            new_params.append(b[-1][0])
        nplanets = len(new_params) // 5
        noffsets = len(new_params) % 5
        fit = {}
        err = {}
        ind = 0
        for planet in range(nplanets):
            planetID = "Planet %i" % (planet + 1)
            fit[planetID] = \
                {param: 0.0 for param in DataLabels['Planet']}.copy()
            err[planetID] = \
                {param: 0.0 for param in DataLabels['Planet']}.copy()
            for param in DataLabels['Planet']:
                fit[planetID][param] = new_params[ind]
                err[planetID][param] = 0.01  # m.perror[ind]
                ind += 1
        fit['Offset'] = \
            {param: 0.0 for param in DataLabels['Offset']}.copy()
        err['Offset'] = \
            {param: 0.0 for param in DataLabels['Offset']}.copy()
        for offset in range(noffsets):
            fit['Offset'][DataLabels['Offset'][offset]] = new_params[ind]
            err['Offset'][DataLabels['Offset'][offset]] = 0.01  # m.perror[ind]
            ind += 1

        tel_off = [0.0] + list(fp['offset'])

        self.jitter_field.setValue(self.data['jitter'])
        return fit, err, nplanets, tel_off

    def on_fit(self):
        import fit_dialog as ui
        if self.pik_full is not None:
            pik_full = ui.decode_pikaia(self.pik_full[:, :-1])
            pik_best = ui.decode_pikaia(np.array([
                self.pik_full[np.argmin(self.pik_full[:, -2]), :-1]
            ]))
        else:
            pik_full = {'chi2': np.random.random_sample((10,)), 'Offset': {}}
            pik_best = {'chi2': np.array([6.79555]), 'Offset': {}}
            for nplanet in range(self.nplanets):
                lbl = 'Planet %i' % (nplanet + 1)
                pik_full[lbl] = {
                    'P': 10.0 + 1000.0 * np.random.random_sample((10,)),
                    'K': 50 * np.random.random_sample((10,)) + 5.0,
                    'e': 0.9 * np.random.random_sample((10,)),
                    'w': 360.0 * np.random.random_sample((10,)),
                    'T0': 500.0 * np.random.random_sample((10,)) + 50000.0
                }
                pik_best[lbl] = {
                    'P': np.array([500.0]),
                    'K': np.array([30.0]),
                    'e': np.array([0.1]),
                    'w': np.array([180.0]),
                    'T0': np.array([55000.0])
                }

        form = ui.fit_dialog(nplanets=self.nplanets, data=pik_full,
                             best=pik_best)
        form.exec_()
        form.update()
        del pik_full
        del pik_best
        parinfo = form.result()
        if parinfo is None:
            return
        p0 = []
        for i in range(len(parinfo)):
            p0.append(parinfo[i]['value'])

        if any(self.data['tel'] > 0):
            fit, err, nplanets, tel_off = self.new_mpfit(p0, parinfo)
        else:
            fit, err, nplanets, tel_off = self.old_mpfit(p0, parinfo)

        for planet in range(nplanets):
            planetID = "Planet %i" % (planet + 1)
            fit[planetID]['w'] = fit[planetID]['w'] % (360.0)
            # empirycznie :)
            fit[planetID]['T0'] = (fit[planetID]['T0'] % fit[planetID]['P']) \
                + fit[planetID]['P'] * \
                int(self.data['T'][0] / fit[planetID]['P'] + 1)

        self.fits.append(
            Fit(fit, err, self.data, keplerian_fit, offset=tel_off)  # FIXME
        )
        self.cur_fit_no = len(self.fits) - 1
        self.textedit.setText(
            self.fits[-1].get_orbital_pars(self.star_mass))

        self.statusBar().showMessage('Fit completed')
        self.on_draw()

    def on_pick(self, event):
        # The event received here is of the type
        # matplotlib.backend_bases.PickEvent
        #
        # It carries lots of information, of which we're using
        # only a small amount here.
        #
        label = event.artist.properties()['label']
        if re.match("Fit", label):
            fit_no = int(label.split()[-1])
            if event.mouseevent.button == 3:
                for ind_fit, cur_fit in enumerate(self.fits):
                    if (cur_fit.no == fit_no):
                        del self.fits[ind_fit]
                        self.cur_fit_no = -1
            else:
                self.textedit.setText(
                    self.fits[fit_no].get_orbital_pars(self.star_mass))
                self.cur_fit_no = fit_no
            self.on_draw()
            return

        point = event.artist
        ind = np.where(self.data['T'] == point.get_xdata()[event.ind])
        for key in list(self.data.keys()):
            self.data[key] = np.delete(self.data[key], ind)

        x = point.get_xdata()[event.ind]
        y = point.get_ydata()[event.ind]

        msg = "You've removed point with coords:\n %f,%f \n Click Draw!" % \
            (x, y)

        QtGui.QMessageBox.information(self, "Click!", msg)
        self.on_draw()

    def open_file(self):

        # reset all vars upon loading new file
        self.cur_fit_no = -1
        self.fits = []
        self.pik = None
        self.pik_initvals = None
        self.pik_full = None
        self.nplanets = 1

        glob_file = 'Data File (*.dat);;Grzegorz Files (rvc-bis*);;Fits (*.pkl);;All Files (*)'
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '.',
                                                  glob_file)[0]

        match = STARNAME_RE.search(fname)
        if match:
            self.star_TYC = match.group()
        self.star_name_field.setEnabled(True)
        self.star_name_field.setText(self.star_TYC)
        self.setWindowTitle("RV Plotter --- %s" % self.star_name_field.text())

        # Reset old values
        self.data = {'T': [], 'RV': [], 'eRV': [], 'res': [], 'tel': [],
                     'off': [], 'BS': [], 'eBS': [], 'jitter': 0.0}
        self.x = []

        self.star_mass = 1.0
        self.mass_field.setEnabled(True)

        if os.path.isfile(fname):
            fileName, fileExtension = os.path.splitext(str(fname))
            if fileExtension == '.dat':
                rvlin_dt = np.dtype({'names': ['t', 'v', 'e', 'tel'],
                                     'formats': [np.float, np.float, np.float, 'S1']})
                data = np.loadtxt(str(fname), dtype=rvlin_dt)
                telarr = np.zeros(data['tel'].shape, dtype=np.int)
                for itel, stel in enumerate(np.unique(data['tel'])):
                    telarr[np.where(data['tel'] == stel)] = itel

                self.data['T'] = data['t']
                self.data['RV'] = data['v']
                self.data['off'] = np.zeros_like(data['v'])
                self.data['BS'] = np.zeros_like(data['v'])
                self.data['eBS'] = np.zeros_like(data['v'])
                self.data['eRV'] = data['e']
                self.data['tel'] = telarr
                f = np.zeros((telarr.shape[0], 5))
                f[:, 0] = data['t']
                f[:, 1] = data['v']
                f[:, 2] = data['e']
            elif fileExtension == '.pkl':
                pkl_file = open(fname, 'rb')
                fit = pickle.load(pkl_file)
                pkl_file.close()

                self.fits.append(fit)
                self.cur_fit_no = len(self.fits) - 1
                self.textedit.setText(
                    self.fits[-1].get_orbital_pars(self.star_mass))
                self.data = fit.data.copy()
                # need for on draw
                self.x = np.linspace(self.data['T'].min(),
                                     self.data['T'].max(), 1000)
                self.jitter_field.setValue(self.data['jitter'])
                f = np.zeros((len(self.data['T']), 5))
                f[:, 0] = self.data['T']
                f[:, 1] = self.data['RV']
                f[:, 2] = self.data['eRV']
                for key in ['BS', 'eBS']:
                    if key not in self.data:
                        self.data[key] = np.zeros_like(self.data['RV'])
                f[:, 3] = self.data['BS']
                f[:, 4] = self.data['eBS']
            else:
                f = np.loadtxt(str(fname), usecols=(0, 1, 2, 3, 4))
                self.data['T'] = f[:, 0]
                self.data['RV'] = f[:, 1]
                self.data['eRV'] = f[:, 2]
                self.data['BS'] = f[:, 3]
                self.data['eBS'] = f[:, 4]
                self.data['tel'] = np.zeros(f.shape[0], dtype=np.int)
                self.data['off'] = np.zeros(f.shape[0], dtype=np.int)

            if fileExtension != '.pkl':
                self.data['jitter'] = 0.0

            self.jitter_field.setEnabled(True)

            self.star_rvs = len(self.data['T'])
            # useful for later fit plots
            self.x = np.linspace(self.data['T'].min(),
                                 self.data['T'].max(), 1000)
            self.statusBar().showMessage('File %s loaded' % fname)
            self.grid[1].clear()
            self.on_draw()
            self.tabelka.get_table_data(f)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
