#!/usr/bin/python

import os
import numpy as np
import random
import tempfile
import argparse
import multiprocessing as mp
from mpi4py import MPI
from pypikaia import pypikaia
from mpfit import mpfit
from fit import Fit
import h5py as h5
import shutil
import glob
from rvlin import rvlin, rv_drive
from rvlin_jit import blah, get_long_pars


def decode_pikaia(pikaia_data):
    result = {'Offset': {}}

    offs = [('v%i' % i) for i in range(0, 5)]
    nplanets = pikaia_data[:, :-1].shape[-1] // 5
    noffsets = pikaia_data[:, :-1].shape[-1] % 5

    for planet in range(0, nplanets):
        key = "Planet %i" % (planet + 1)
        result[key] = {}
        for indx, item in enumerate(['T0', 'P', 'K', 'e', 'w']):
            i = indx + 5 * planet
            result[key][item] = pikaia_data[:, i]
    for i in range(0, noffsets):
        result['Offset'][offs[i]] = pikaia_data[:, nplanets * 5 + i]

    result['chi2'] = pikaia_data[:, -1]
    return result


def random_permutations(l, n):
    """http://stackoverflow.com/questions/10232338/"""
    while n:
        random.shuffle(l)
        yield list(l)
        n -= 1


def flatten(lst):
    for x in lst:
        if isinstance(x, list):
            for x in flatten(x):
                yield x
        else:
            yield x


parser = argparse.ArgumentParser()
# parser.add_argument("--jitter", type=float, default=0.0)
parser.add_argument("--nperms", type=int, default=100)
parser.add_argument("--nplanets", type=int, default=1)
parser.add_argument("--npercore", type=int, default=1)
parser.add_argument('fname', metavar='fname', type=str, nargs=1,
                    help='Plik z danymi')
args = parser.parse_args()

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    if os.path.isfile(args.fname[0]):
        starname = args.fname[0].split('.')[1]
        fileName, fileExtension = os.path.splitext(str(args.fname[0]))
        data = {'T': [], 'RV': [], 'eRV': [], 'tel': []}
        if fileExtension == '.dat':
            rvlin_dt = np.dtype(
                {'names': ['T', 'RV', 'eRV', 'tel'],
                 'formats': [np.float, np.float, np.float, 'S1']}
            )
            foo = np.loadtxt(str(args.fname[0]), dtype=rvlin_dt)
            for t, rv, erv, tel in foo:
                data['T'].append(t)
                data['RV'].append(rv)
                data['eRV'].append(erv)
                data['tel'].append(tel)
        else:
            rvlin_dt = np.dtype({'names': ['T', 'RV', 'eRV'],
                                 'formats': [np.float, np.float, np.float]})
            foo = np.loadtxt(str(args.fname[0]), usecols=(0, 1, 2),
                             dtype=rvlin_dt)
            for t, rv, erv in foo:
                data['T'].append(t)
                data['RV'].append(rv)
                data['eRV'].append(erv)
                data['tel'].append(0)

        for key in list(data.keys()):
            data[key] = np.array(data[key])

        telarr = np.zeros_like(data['tel'], dtype=np.int)
        for itel, stel in enumerate(np.unique(data['tel'])):
            telarr[np.where(data['tel'] == stel)] = itel
        data['tel'] = telarr

        Npoints = len(data['T'])
        # data['eRV'] = np.sqrt(data['eRV'] ** 2 + args.jitter ** 2)

    if args.nperms > 1:
        perms = [item for item in random_permutations(list(range(Npoints)), args.nperms)]
    else:
        perms = [list(range(Npoints))]
    # dividing data into chunks
    chunks = [[] for _ in range(size)]
    for i, chunk in enumerate(perms):
        chunks[i % size].append(chunk)
else:
    permid = None
    perms = None
    data = None
    chunks = None

perms = comm.scatter(chunks, root=0)
data = comm.bcast(data, root=0)

# pikaia
npln = args.nplanets
ctrl = -np.ones((12))
mina = np.array(npln * [10.0, data['T'].min(), 0.001] + [0.0])
maxa = np.array(npln * [2000.0, data['T'].max(), 0.800] + [100.0])

results = np.zeros((len(perms), 10))

for iperm, perm in enumerate(perms):
    seed = random.randint(1, 999999)
    x, f, stat = pypikaia(data['T'], data['RV'][perm], data['eRV'][perm],
                          data['tel'][perm],
                          seed, ctrl, mina, maxa,
                          args.npercore * mp.cpu_count(), 2)
    v = np.vstack((f, stat)).T
    pik_full = np.vstack((x.T, v.T)).T
    #rint pik_full
    best = pik_full[np.argmin(pik_full[:, -2]), :-1]
    print(best)

    p0 = best[:npln*3 + 1]
    parinfo = [{'value': val, 'fixed': 0, 'limited': [0, 0],
                'limits': [0.0, 0.0]} for val in p0]
    for i in range(len(p0) // 3):
        parinfo[2 + i * 3]['limited'] = [1, 1]
        parinfo[2 + i * 3]['limits'] = [0.01, 0.7]  # e

    nplt = len(p0) // 3
    m = mpfit(blah, p0, parinfo=parinfo, quiet=True, autoderivative=1,
              functkw={'x': data['T'][perm], 'y': data['RV'][perm],
                       'err': data['eRV'][perm], 'tel': data['tel'][perm]})
    foo = blah(m.params, x=data['T'][perm], y=data['RV'][perm],
               err=data['eRV'][perm], tel=data['tel'][perm])

    nn = len(foo[-1])
    foo = np.sum(foo[-1]**2 / nn)

    fp = rvlin(data, m.params[:-1], nompfit=True)
    p1 = get_long_pars(data, m.params[:-1])
    p1.append(m.params[-1])
    p1 = np.array(p1)

    offset = [0, fp['offset']]
    data['off'] = np.zeros_like(data['T'])
    for itel in range(int(data['tel'].max()) + 1):
        ind = np.where(data['tel'] == itel)
        data['off'][ind] = offset[itel]


    eRV_1 = np.sqrt(data['eRV'][:] ** 2 + p1[-1] ** 2)
    chi2_p1 = np.sum(
        (data['RV'][:] - data['off'] - rv_drive(data['T'][:], p1[:-1])) ** 2 /
         eRV_1 ** 2) / (data['T'].shape[0] - 4)
    p1[-2] = fp['offset']

    print("foo", foo)
    #results[iperm, :] = np.concatenate((p1, [np.sqrt(chi2_p1)]))
    results[iperm, :] = np.concatenate((p1, [foo, np.sqrt(chi2_p1)]))

results = comm.gather(results, root=0)

if rank == 0:
    nperms = args.nperms
    with h5.File('%s_fap_1e%1i.h5' % (starname, np.log10(nperms)), 'w') as h5f:
        #h5f['/gold'] = np.concatenate((p00, [np.sqrt(chi2_p0)]))
        h5f['/data'] = np.concatenate(results)
