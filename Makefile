F2PY   = f2py
FC     = gfortran
F2PYFLAGS = --fcompiler=$(FC) --f90flags="-fopenmp"
FFLAGS = -Ipikaia
FFLAGS += -fPIC -ffree-line-length-none -O2 -g -ffree-form -fopenmp
#FFLAGS += -fcheck=all -fbacktrace -ffpe-trap=zero,overflow,invalid
#

OBJS = pikaia/func.o pikaia/pikaia.o pikaia/rvlin.o
OBJS_GPU = pikaia_gpu/pikaia_master.o pikaia_gpu/commons.o pikaia_gpu/func.o pikaia_gpu/host_func.o

all: pypikaia.so

pikaia_gpu/pikaia_master.o: pikaia_gpu/pikaia_master.f pikaia_gpu/func.o pikaia_gpu/host_func.o
pikaia_gpu/func.o: pikaia_gpu/func.f90 pikaia_gpu/commons.o
pikaia/func.o: pikaia/func.f90
pikaia/rvlin.o: pikaia/rvlin.f90
pikaia/pikaia.o: pikaia/pikaia.f90

pikaia_gpu/host_func.o: pikaia_gpu/host_func.f90
	$(FC) $(FFLAGS) `pkg-config --cflags fortrancl` -c $< -o $@

%.o: %.f90
	$(FC) $(FFLAGS) -c $< -o $@

pypikaia.so: $(OBJS)
	$(F2PY) $(F2PYFLAGS) -c $(OBJS) pikaia/pypikaia.f90 -m pypikaia -lgomp `pkg-config --libs blas lapack`

pypikaia_gpu.so: $(OBJS_GPU)
	$(F2PY) $(F2PYFLAGS) -c $(OBJS_GPU) pikaia_gpu/pypikaia.f90 -m pypikaia_gpu `pkg-config --libs fortrancl`

clean:
	rm -f pikaia_gpu/*.o pikaia_gpu/*.mod *.mod pypikaia*.so pikaia/*.o *.pyc
